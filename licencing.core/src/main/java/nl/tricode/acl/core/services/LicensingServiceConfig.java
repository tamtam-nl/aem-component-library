package nl.tricode.acl.core.services;


/**
 * Service for dealing with inherited properties.
 */
public interface LicensingServiceConfig {

    /**
     * Gets the licensingName specified in the OSGi configuration
     */
    String getLicensingName();
    /**
     * Gets the licensingId specified in the OSGi configuration
     */
    String getLicensingId();

}
