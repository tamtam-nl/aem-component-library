package nl.tricode.acl.core.services;

import org.apache.felix.scr.annotations.*;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Dictionary;

@Service(value = LicensingServiceConfig.class)
@Component(immediate = true, metatype = true, name = "nl.tricode.acl.core.services.LicensingServiceConfig",
        description = "Configuration for licensing the AEM Component Library bundle",
        label = "ACL licensing configuration")
public class LicensingServiceConfigImpl implements LicensingServiceConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(LicensingServiceConfigImpl.class);

    @Property(name = "acl.licensingName", label = "Client name", description = "Licensing name that is provided for the client", value = "")
    public static final String PROPERTY_LICENSING_NAME = "acl.licensingName";

    @Property(name = "acl.licensingId", label = "License key", description = "Licensing unique ID provided for the client", value = "")
    private static final String PROPERTY_LICENSING_ID = "acl.licensingId";

    private String licensingName;
    private String licensingId;

    @Override
    public String getLicensingName() {
        return licensingName;
    }

    @Override
    public String getLicensingId() {
        return licensingId;
    }

    @Activate
     public void activate(final ComponentContext componentContext)  {
        final Dictionary properties = componentContext.getProperties();
        if(properties != null) {
            licensingName = properties.get(PROPERTY_LICENSING_NAME).toString();
            licensingId = properties.get(PROPERTY_LICENSING_ID).toString();
        }
    }

}
