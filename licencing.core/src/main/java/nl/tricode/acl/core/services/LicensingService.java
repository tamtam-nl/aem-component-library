package nl.tricode.acl.core.services;


/**
 * Service for checking ACL licences.
 */
@FunctionalInterface
public interface LicensingService {

    public boolean isValidLicense();
}
