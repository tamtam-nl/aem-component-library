package nl.tricode.acl.core.services;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.osgi.framework.BundleException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Dictionary;

@Component(immediate = true, description = "Service for checking validity of ACL license", label = "ACL License check")
@Service(value = LicensingService.class)
public class LicensingServiceImpl implements LicensingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LicensingServiceImpl.class);
    private static final String ENDPOINT = "http://10.0.4.76:8081/api/licensing";

    @Reference
    private LicensingServiceConfig licensingServiceConfig;

    @Override
    public boolean isValidLicense() {
        LOGGER.info("IsValidLicense() method called");
        String licensingName = licensingServiceConfig.getLicensingName();
        String licensingId = licensingServiceConfig.getLicensingId();

        String urlParameters = "licensingName="+licensingName+"&"+"licensingId="+licensingId;
        String completeUrl = ENDPOINT + "?" + urlParameters;

        try {
            return sendGet(completeUrl);
        } catch (Exception e) {
            LOGGER.error("Error while connecting to the licensing service.", e);
        }

        return false;
    }

    private boolean sendGet(String url) throws Exception {
        LOGGER.info("sendGet() method called");

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        con.setRequestProperty("X-FORWARDED-FOR","127.0.0.1");
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();
        return Boolean.parseBoolean(response.toString());
    }
}
