package nl.tricode.aem.core.services;

import io.wcm.testing.mock.aem.junit.AemContext;
import nl.tricode.aem.core.services.implementations.AlternateLanguagePagesService;
import org.apache.sling.testing.resourceresolver.MockResourceResolverFactory;
import org.apache.sling.testing.resourceresolver.MockResourceResolverFactoryOptions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AlternateLanguagePagesServiceTest {

    public static final String ROOT_PAGE = "/content/acl";
    public static final String EN_LANGUAGE_PATH = "/en";
    public static final String IT_LANGUAGE_PATH = "/it";
    public static final String FR_LANGUAGE_PATH = "/fr";
    private AlternateLanguagePagesService service;

    @Rule
    public final AemContext context = new AemContext();
    private final static MockResourceResolverFactoryOptions resolverFactoryOptions = new MockResourceResolverFactoryOptions();

    @Before
    public void initInheritedPropertiesService() {
        this.context.registerService(new MockResourceResolverFactory(resolverFactoryOptions));
        service = this.context.registerInjectActivateService(new AlternateLanguagePagesService());
        createRoorPage();
    }

    @Test
    public void testGetAlternatePagesForSiteLevelPage_shouldGetEmptyList() {
        assertTrue(service.getAlternatePages(ROOT_PAGE).isEmpty());

    }

    @Test
    public void testGetAlternatePagesForLanguageLevelPage_shouldGetEmptyList() {
        createLanguagePagesPage();
        assertTrue(service.getAlternatePages(ROOT_PAGE + EN_LANGUAGE_PATH).isEmpty());

    }

    @Test
    public void testGetAlternatePagesNoLanguagePagesUnderRootPage_shouldGetEmptyLis() {
        createPageStructure();
        assertTrue(service.getAlternatePages(ROOT_PAGE + EN_LANGUAGE_PATH + "/test").isEmpty());

    }

    @Test
    public void testGetAlternatePagesForNonExistingPage_shouldGetEmptyList() {
        createLanguagePagesPage();
        assertTrue(service.getAlternatePages(ROOT_PAGE + FR_LANGUAGE_PATH + "/nonexistingpage").isEmpty());

    }

    @Test
    public void testGetAlternatePagesMoreAlternatePages_shouldGetAlternatePagesList() {
        createLanguagePagesPage();
        createPageStructure();
        assertEquals(2, service.getAlternatePages(ROOT_PAGE + EN_LANGUAGE_PATH + "/test").size());
    }

    @Test
    public void testGetAlternatePagesExistingPageButNoAlternatePages_shouldGetEmptyList() {
        createLanguagePagesPage();
        createPageStructure();
        assertTrue(service.getAlternatePages(ROOT_PAGE + FR_LANGUAGE_PATH + "/onlyfrpage").isEmpty());
    }

    private void createRoorPage() {
        this.context.create().page("/content/acl", "nonexisting");
    }

    private void createLanguagePagesPage() {
        this.context.create().page(ROOT_PAGE + EN_LANGUAGE_PATH, "nonexisting");
        this.context.create().page(ROOT_PAGE + IT_LANGUAGE_PATH, "nonexisting");
        this.context.create().page(ROOT_PAGE + FR_LANGUAGE_PATH, "nonexisting");
    }

    private void createPageStructure() {
        this.context.create().page(ROOT_PAGE + EN_LANGUAGE_PATH + "/test", "nonexisting");
        this.context.create().page(ROOT_PAGE + IT_LANGUAGE_PATH + "/test", "nonexisting");
        this.context.create().page(ROOT_PAGE + FR_LANGUAGE_PATH + "/test", "nonexisting");
        this.context.create().page(ROOT_PAGE + FR_LANGUAGE_PATH + "/onlyfrpage", "nonexisting");

    }


}
