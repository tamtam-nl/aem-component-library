package nl.tricode.aem.core.models;


import com.day.cq.wcm.api.designer.Designer;
import com.google.common.collect.ImmutableMap;
import io.wcm.testing.mock.aem.junit.AemContext;
import nl.tricode.aem.core.utils.DesignerUtil;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class DesignerUtilTest {

    ResourceResolver resourceResolver;
    private Designer designer;
    private String path;
    Map<String, String> parameters = new HashMap<String, String>();

    @Rule
    public final AemContext context = new AemContext();

    @Before
    public void setUp() {
        context.addModelsForPackage("nl.tricode.aem.core.utils");
        designer = context.resourceResolver().adaptTo(Designer.class);
        resourceResolver = context.resourceResolver();
        parameters.put("buttonColor","btn-primary \t Primary \n" +
                "btn-success \t Success \n" +
                "btn-warning \t Warning \n" +
                "btn-secondary \t Secondary \n" +
                "btn-info \t Info \n" +
                "btn-danger \t Danger \n");
        parameters.put("buttonStyle","btn-default \t Default \n" +"btn-transparent \t Transparent \n");
        parameters.put("sling:resourceType", "acl/components/content/button");
        parameters.put("jcr:primaryType", "nt:unstructured");
        parameters.put("buttonSize", "size-md \t Medium \n" +
                "size-xs \t Extra Small \n" +
                "size-sm \t Small \n" +
                "size-lg \t Large \n" +
                "size-xl \t Extra Large \n");
        cretePageStructure();
    }

    @Test
    public void getContentStyle_shouldReturnNullValuePathIsNull() {
        path = null;
        assertNull(DesignerUtil.getContentStyle(designer, path, resourceResolver));
    }

    @Test
    public void getContentStyle_shouldReturnValue() {
        path = "/content/test-designerUtil/jcr:content/par/section/sectionPar/button";
//        assertEquals(parameters, DesignerUtil.getContentStyle(designer, path, resourceResolver));
    }

    /**
     * Create the initial page structure for testing
     */
    private void cretePageStructure() {
        //page under content
        this.context.create().page("/content/test-designerUtil", "/apps/acl/templates/base-template",ImmutableMap.<String, Object>builder()
                .put("cq:designPath", "/etc/designs/acl").build());
          this.context.create().resource("/content/test-designerUtil/jcr:content/par/section/sectionPar/button", ImmutableMap.<String, Object>builder()
                .put("align", "left")
                .put("buttonColor", "btn-primary")
                .put("buttonStyle", "btn-default")
                .put("fontWeight", "none")
                .put("text", "testing_button")
                .put("sling:resourceType", "sling:resourceType")
                .put("jcr:primaryType", "nt:unstructured")
                .build());

        //resource under /etc
        this.context.create().resource("/etc/designs/acl/jcr:content", ImmutableMap.<String, Object>builder()
                .put("sling:resourceType", "wcm/core/components/designer").build());
        this.context.create().resource("/etc/designs/acl/jcr:content/page");
        this.context.create().resource("/etc/designs/acl/jcr:content/page/button",ImmutableMap.<String, Object>builder()
                .put("buttonColor", "btn-primary \t Primary \n" +
                        "btn-success \t Success \n" +
                        "btn-warning \t Warning \n" +
                        "btn-secondary \t Secondary \n" +
                        "btn-info \t Info \n" +
                        "btn-danger \t Danger \n")
                .put("buttonStyle", "btn-default \t Default \n" +
                        "btn-opaque \t Opaque \n")
                .put("sling:resourceType", "acl/components/content/button")
                .put("jcr:primaryType", "nt:unstructured")
                .put("buttonSize", "size-md \t Medium \n" +
                        "size-xs \t Extra Small \n" +
                        "size-sm \t Small \n" +
                        "size-lg \t Large \n" +
                        "size-xl \t Extra Large \n")
                .build());
    }
}
