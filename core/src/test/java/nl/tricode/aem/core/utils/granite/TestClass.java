package nl.tricode.aem.core.utils.granite;

public class TestClass {
    private String field1;
    private String field2;
    private String field3;

    TestClass() {
    }

    TestClass(String field1, String field2, String field3) {
        this.field1 = field1;
        this.field2 = field2;
        this.field3 = field3;
    }

    public String getField1() {
        return field1;
    }

    public void setField1(String field1) {
        this.field1 = field1;
    }

    public String getField2() {
        return field2;
    }

    public void setField2(String field2) {
        this.field2 = field2;
    }

    public String getField3() {
        return field3;
    }

    public void setField3(String field3) {
        this.field3 = field3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TestClass)) {
            return false;
        }

        TestClass testClass = (TestClass) o;

        if (!field1.equals(testClass.field1)) {
            return false;
        }
        if (!field2.equals(testClass.field2)) {
            return false;
        }
        if (!field3.equals(testClass.field3)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = field1.hashCode();
        result = 31 * result + field2.hashCode();
        result = 31 * result + field3.hashCode();
        return result;
    }
}