package nl.tricode.aem.core.models.socialicons;

import com.google.common.collect.ImmutableMap;
import io.wcm.testing.mock.aem.junit.AemContext;
import nl.tricode.aem.core.services.InheritedPropertiesService;
import nl.tricode.aem.core.services.implementations.InheritedPropertiesServiceImpl;
import org.apache.sling.api.resource.Resource;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for {@link nl.tricode.aem.core.models.socialicons.SocialIcons}.
 */
public class SocialIconsTest {

    @Rule
    public final AemContext context = new AemContext();


    private InheritedPropertiesService service;

    private List<SocialIconsOption> listOfExpectedSocialIcons = new LinkedList<>();
    private SocialIconsOption facebook = new SocialIconsOption("test1", "test1", "facebook");
    private SocialIconsOption linkedin = new SocialIconsOption("test2", "test2", "linkedin");

    @Before
    public void setUp() {
        service = this.context.registerService(new InheritedPropertiesServiceImpl());
        context.addModelsForPackage("nl.tricode.aem.core.models.socialicons");
        cretePageStructure();

        listOfExpectedSocialIcons.add(facebook);
    }

    @Test
    public void testGetSocialIcons_shouldReturnOwnProperties() {
        Resource resource = createSocialIconsResource("/content/acl/en/level1page/jcr:content/component", facebook, true);

        assertEquals(listOfExpectedSocialIcons, resource.adaptTo(SocialIcons.class).getSocialIcons());
    }

    @Test
    public void testGetSocialIcons_shouldReturnOwnPropertiesDisabledInheritance() {
        createSocialIconsResource("/content/acl/en/level1page/jcr:content/component", linkedin, false);

        Resource resource = createSocialIconsResource("/content/acl/en/level1page/level2page/jcr:content/component", facebook, true);

        assertEquals(listOfExpectedSocialIcons, resource.adaptTo(SocialIcons.class).getSocialIcons());
    }

    @Test
    public void testGetSocialIcons_shouldReturnParentProperties() {
        createSocialIconsResource("/content/acl/en/level1page/jcr:content/component", facebook, false);

        Resource resource = createEmptyResource("/content/acl/en/level1page/level2page/jcr:content/component");

        assertEquals(listOfExpectedSocialIcons, resource.adaptTo(SocialIcons.class).getSocialIcons());
    }

    @Test
    public void testGetSocialIcons_shouldReturnParentPropertiesEnabledInheritance() {
        createSocialIconsResource("/content/acl/en/level1page/jcr:content/component", facebook, false);

        Resource resource = createSocialIconsResource("/content/acl/en/level1page/level2page/jcr:content/component", linkedin, false);

        assertEquals(listOfExpectedSocialIcons, resource.adaptTo(SocialIcons.class).getSocialIcons());
    }

    @Test
    public void testGetSocialIcons_shouldReturnParentPropertiesWithSingleIcon() {
        createSocialIconResource("/content/acl/en/level1page/jcr:content/component", facebook, false);

        Resource resource = createEmptyResource("/content/acl/en/level1page/level2page/jcr:content/component");

        assertEquals(listOfExpectedSocialIcons, resource.adaptTo(SocialIcons.class).getSocialIcons());
    }

    @Test
    public void testGetSocialIcons_shouldReturnParentPropertiesEnabledInheritanceWithSingleIcon() {
        createSocialIconResource("/content/acl/en/level1page/jcr:content/component", facebook, false);

        Resource resource = createSocialIconResource("/content/acl/en/level1page/level2page/jcr:content/component", linkedin, false);

        assertEquals(listOfExpectedSocialIcons, resource.adaptTo(SocialIcons.class).getSocialIcons());
    }

    @Test
    public void testIsConfigured_shouldReturnFalseNoComponentConfiguredNoInheritance() {
        Resource resource = createEmptyResource("/content/acl/en/level1page/level2page/jcr:content/component");

        assertFalse(resource.adaptTo(SocialIcons.class).isConfigured());
    }

    @Test
    public void testIsConfigured_shouldReturnTrueNoComponentConfiguredButInheritedSuccessfully() {
        createSocialIconsResource("/content/acl/en/level1page/jcr:content/component", facebook, false);

        Resource resource = createEmptyResource("/content/acl/en/level1page/level2page/jcr:content/component");

        assertTrue(resource.adaptTo(SocialIcons.class).isConfigured());
    }

    @Test
    public void testIsConfigured_shouldReturnTrueComponentConfiguredNoInheritance() {
        createSocialIconsResource("/content/acl/en/level1page/jcr:content/component", linkedin, false);

        Resource resource = createSocialIconsResource("/content/acl/en/level1page/level2page/jcr:content/component", facebook, true);

        assertTrue(resource.adaptTo(SocialIcons.class).isConfigured());
    }

    @Test
    public void testIsConfigured_shouldReturnTrueComponentConfiguredAndInherited() {
        createSocialIconsResource("/content/acl/en/level1page/jcr:content/component", linkedin, false);

        Resource resource = createSocialIconsResource("/content/acl/en/level1page/level2page/jcr:content/component", facebook, false);

        assertTrue(resource.adaptTo(SocialIcons.class).isConfigured());
    }

    /**
     * Create the initial page structure for testing
     */
    private void cretePageStructure() {
        this.context.create().page("/content/acl/en/level1page", "nonexisting");
        this.context.create().page("/content/acl/en/level1page/level2page", "nonexisting");
    }

    /**
     * Creates social icons resource out of SocialIconOption on a specific path with disabledInheritance
     * enabled/disabled
     *
     * @param path
     * @param option
     * @param disableInheritance
     * @return the created resource
     */
    private Resource createSocialIconsResource(String path, SocialIconsOption option, Boolean disableInheritance) {
        return this.context.create().resource(path,
                ImmutableMap.<String, Object>builder()
                        .put("disableInheritance", disableInheritance)
                        .put("socialIcons", new String[]{"{\"title\":\"" + option.getTitle()
                                + "\",\"link\":\"" + option.getLink()
                                + "\",\"logoType\":\"" + option.getLogoType() + "\"}"})
                        .build());
    }

    /**
     * Creates social icons resource out of Single SocialIconOption on a specific path with disabledInheritance
     * enabled/disabled
     *
     * @param path
     * @param option
     * @param disableInheritance
     * @return the created resource
     */
    private Resource createSocialIconResource(String path, SocialIconsOption option, Boolean disableInheritance) {
        return this.context.create().resource(path,
                ImmutableMap.<String, Object>builder()
                        .put("disableInheritance", disableInheritance)
                        .put("socialIcons", "{\"title\":\"" + option.getTitle()
                                + "\",\"link\":\"" + option.getLink()
                                + "\",\"logoType\":\"" + option.getLogoType() + "\"}")
                        .build());
    }

    /**
     * Creates empty resource on specific path. No parameters included.
     *
     * @param path
     * @return te created resource
     */
    private Resource createEmptyResource(String path) {
        return this.context.create().resource(path,
                ImmutableMap.<String, Object>builder()
                        .build());
    }
}
