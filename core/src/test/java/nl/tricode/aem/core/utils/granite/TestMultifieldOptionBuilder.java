package nl.tricode.aem.core.utils.granite;

import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.LinkedList;

/**
 * Unit tests for {@link nl.tricode.aem.core.utils.granite.MultifieldOptionBuilder}.
 */
public class TestMultifieldOptionBuilder {

    private final String [] itemsThatCanBeDeserialized = {"{\"field1\":\"field10\",\"field2\":\"field11\",\"field3\":\"field12\"}",
            "{\"field1\":\"field20\",\"field2\":\"field21\",\"field3\":\"field22\"}",
            "{\"field1\":\"field30\",\"field2\":\"field31\",\"field3\":\"field32\"}"};

    private final String [] itemsThatMissClassProperty = {"{\"field1\":\"field10\",\"field2\":\"field11\"}",
            "{\"field1\":\"field20\",\"field2\":\"field21\"}",
            "{\"field1\":\"field30\",\"field2\":\"field31\""};

    private final String itemThatCanBeDeserialized = "{\"field1\":\"field10\",\"field2\":\"field11\",\"field3\":\"field12\"}";

    private static LinkedList<TestClass> list = new LinkedList<>();
    private static LinkedList<TestClass> singleItemlist = new LinkedList<>();
    private MultifieldOptionBuilder<TestClass> multifieldOptionBuilder = new MultifieldOptionBuilder<>();

    @BeforeClass
    public static void initTest(){
        list.add(new TestClass("field10", "field11", "field12"));
        list.add(new TestClass("field20", "field21", "field22"));
        list.add(new TestClass("field30", "field31", "field32"));

        singleItemlist.add(new TestClass("field10", "field11", "field12"));
    }

    @Test
    public void testBuildOptions_shouldReturnListOfItems() throws IOException {
        assertTrue(multifieldOptionBuilder.buildOptions(itemsThatCanBeDeserialized, TestClass.class).equals(list));
    }

    @Test(expected = IOException.class)
    public void testBuildOptions_shouldThrowIOException() throws IOException {
        multifieldOptionBuilder.buildOptions(itemsThatMissClassProperty, TestClass.class);
    }

    @Test
    public void testBuildOptions_shouldReturnEmptyListForNoItems() throws IOException {
        assertTrue(multifieldOptionBuilder.buildOptions(new String[]{}, TestClass.class).isEmpty());
    }

    @Test
    public void testBuildOption_shouldReturnItem() throws IOException {
        assertTrue(multifieldOptionBuilder.buildOption(itemThatCanBeDeserialized, TestClass.class).equals(singleItemlist));
    }
}
