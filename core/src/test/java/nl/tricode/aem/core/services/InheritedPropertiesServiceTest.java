package nl.tricode.aem.core.services;

import com.day.cq.wcm.api.WCMException;
import com.google.common.collect.ImmutableMap;
import io.wcm.testing.mock.aem.junit.AemContext;
import nl.tricode.aem.core.services.implementations.InheritedPropertiesServiceImpl;
import org.apache.sling.api.resource.Resource;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Unit tests for {@link nl.tricode.aem.core.services.implementations.InheritedPropertiesServiceImpl}.
 */
public class InheritedPropertiesServiceTest {

    private static final String EXPECTED_STRING_PROPERTY_VALUE = "someProperty";
    private static final String [] EXPECTED_PROPERTY_STRING_ARRAY_VALUE = new String [] {"property1", "property1"};
    private static final String PROPERTY_NAME = "property";
    private InheritedPropertiesService service;

    @Rule
    public final AemContext context = new AemContext();

    @Before
    public void initInheritedPropertiesService() {
        service = this.context.registerService(new InheritedPropertiesServiceImpl());
        cretePageStructure();
    }

    @Test
    public void testGetInheritedPropertyStringValue_shouldGetSingleParentProperty() throws WCMException {
        this.context.create().resource("/content/acl/en/level1page/jcr:content/component", ImmutableMap.<String, Object>builder()
                .put(PROPERTY_NAME, EXPECTED_STRING_PROPERTY_VALUE)
                .build());
        Resource level2Resource = this.context.create().resource("/content/acl/en/level1page/level2page/jcr:content/component");

        assertEquals(EXPECTED_STRING_PROPERTY_VALUE, service.getInheritedPropertyValue(PROPERTY_NAME, level2Resource));
    }

    @Test
    public void testGetInheritedPropertyStringValue_shouldGetParentArrayProperty() throws WCMException {
        this.context.create().resource("/content/acl/en/level1page/jcr:content/component", ImmutableMap.<String, Object>builder()
                .put(PROPERTY_NAME, EXPECTED_PROPERTY_STRING_ARRAY_VALUE)
                .build());
        Resource level2Resource = this.context.create().resource("/content/acl/en/level1page/level2page/jcr:content/component");

        assertArrayEquals(EXPECTED_PROPERTY_STRING_ARRAY_VALUE, service.getInheritedPropertyValue(PROPERTY_NAME, level2Resource));
    }

    @Test
    public void testGetInheritedPropertyStringValue_shouldNotFindProperty() throws WCMException {
        this.context.create().resource("/content/acl/en/level1page/jcr:content/component");
        Resource level2Resource = this.context.create().resource("/content/acl/en/level1page/level2page/jcr:content/component");

        assertNull(service.getInheritedPropertyValue(PROPERTY_NAME, level2Resource));
    }

    /**
     * Create the initial page structure for testing
     */
    private void cretePageStructure() {
        this.context.create().page("/content/acl/en/level1page", "nonexisting");
        this.context.create().page("/content/acl/en/level1page/level2page", "nonexisting");
    }
}
