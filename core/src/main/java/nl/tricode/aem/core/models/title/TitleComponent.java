package nl.tricode.aem.core.models.title;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.designer.Style;
import nl.tricode.aem.core.services.TitleValidatorService;
import nl.tricode.aem.core.utils.PageUtil;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Model for the title component
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class TitleComponent {

    @Inject
    private transient TitleValidatorService titleValidatorService;

    @Inject
    private Page currentPage;

    @Inject
    private Style currentStyle;

    @Inject
    @Via("resource")
    @Default(values = "")
    private String type;

    @Inject
    @Via("resource")
    @Default(values = "inherit")
    private String textAlign;

    @Inject
    @Via("resource")
    @Default(values = "inherit")
    private String textColor;

    @SlingObject
    private Resource resource;

    @Inject
    @Via("resource")
    @Named("jcr:title")
    @Default(values = "")
    private String title;

    public String getText() {
        if (!title.isEmpty()) {
            return title;
        }
        return PageUtil.getNavigationTitleWithFallback(currentPage);
    }

    public String getType() {
        return type;
    }

    public String getElement() {
        String defaultStyle = null;
        if (!type.isEmpty()) {
            return type;
        } else {
            if (currentStyle != null) {
                defaultStyle = (String) currentStyle.get("defaultType");
                if (defaultStyle != null && defaultStyle.isEmpty()) {
                    return defaultStyle;
                }
            }
        }
        return defaultStyle;
    }

    public String getTextAlign() { return textAlign; }

    public String getTextColor() { return textColor; }


    public boolean getStatus() {
        boolean tag = this.type.equals("h1");
        int isValid = titleValidatorService.isValid(currentPage, resource);
        if (isValid == 0 && tag) {
            return false;
        } else if (isValid == 1 && !tag) {
            return false;
        } else if (!tag) {
            return false;
        }
        return true;
    }
}
