package nl.tricode.aem.core.models.button;

import nl.tricode.aem.core.models.genericdatasource.GenericDataSource;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;

@Model(adaptables = SlingHttpServletRequest.class)
public class ButtonColorDataSource extends GenericDataSource {

    @PostConstruct
    public void activate() {
        super.loadCustomDataSource("buttonColor");
    }
}
