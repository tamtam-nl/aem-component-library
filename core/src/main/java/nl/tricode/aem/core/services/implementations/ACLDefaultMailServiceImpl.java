package nl.tricode.aem.core.services.implementations;

import com.day.cq.mailer.MailService;
import com.day.cq.mailer.MailingException;
import com.day.cq.mailer.MessageGateway;
import nl.tricode.aem.core.services.ACLDefaultMailService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.oak.commons.PropertiesUtil;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Dictionary;

/**
 * ACL mail service that has a getter for the 'fromAddress' property.
 */
@Service(value = ACLDefaultMailServiceImpl.class)
@Component(metatype = true, label = "ACL - Mail Service")
public class ACLDefaultMailServiceImpl implements MessageGateway<Email>, MailService, ACLDefaultMailService {

    private static final Logger LOG = LoggerFactory.getLogger(ACLDefaultMailServiceImpl.class);

    private static final String DEFAULT_SMTP_HOST = "";
    private static final int DEFAULT_SMTP_PORT = 25;
    private static final boolean DEFAULT_DEBUG_EMAIL = false;
    private static final boolean DEFAULT_USE_SSL_CONNECTION = false;


    private static final String SMTP_HOST = "smtp.host";
    private static final String SMTP_PORT = "smtp.port";
    private static final String SMTP_USER = "smtp.user";
    private static final String SMTP_PASS = "smtp.password";
    private static final String FROM_ADDRESS = "from.address";
    private static final String SMTP_SSL = "smtp.ssl";
    private static final String DEBUG_EMAIL = "debug.email";


    @Property(name = SMTP_HOST, label = "SMTP server host name", description = "The mailer uses this SMTP server to send messages")
    private String smtpHost;

    @Property(name = SMTP_PORT, label = "SMTP server port", description = "Port number to use to connect to the SMTP server")
    private int smtpPort;

    @Property(name = SMTP_USER, label = "SMTP user", description = "The user for authentication through SMTP")
    private String smtpUser;

    @Property(name = SMTP_PASS, label = "SMTP password", description = "The password for authentication through SMTP. The password can either be provided plain text, or crypted via the Crypto Support feature (Main -> Crypto Support menu)")
    private String smtpPassword;

    @Property(name = FROM_ADDRESS, label = "\"From\" address", description = "The email address to use in the \"From:\" field of messages sent by the mailer")
    private String fromAddress;

    @Property(name = SMTP_SSL, label = "SMTP use SSL", description = "If enabled, an SSL connection is set up.", boolValue = DEFAULT_USE_SSL_CONNECTION)
    private boolean smtpSsl;

    @Property(name = DEBUG_EMAIL, label = "Debug email", description = "If enabled, interactions with the SMTP server are dumped to the operating system terminal that runs Sling", boolValue = DEFAULT_DEBUG_EMAIL)
    private boolean debugEmail;


    private ServiceRegistration serviceRegistration;

    @Override
    public String getFromAddress() {
        return fromAddress;
    }

    protected void activate(ComponentContext componentContext) {
        Dictionary properties = componentContext.getProperties();

        this.smtpHost = PropertiesUtil.toString(properties.get(SMTP_HOST), DEFAULT_SMTP_HOST);
        this.smtpPort = PropertiesUtil.toInteger(properties.get(SMTP_PORT), DEFAULT_SMTP_PORT);
        this.smtpUser = PropertiesUtil.toString(properties.get(SMTP_USER), null);
        this.smtpPassword = PropertiesUtil.toString(properties.get(SMTP_PASS), null);
        this.fromAddress = (String) properties.get(FROM_ADDRESS);
        this.smtpSsl = PropertiesUtil.toBoolean(properties.get(SMTP_SSL), DEFAULT_USE_SSL_CONNECTION);
        this.debugEmail = PropertiesUtil.toBoolean(properties.get(DEBUG_EMAIL), DEFAULT_DEBUG_EMAIL);

        if (isValidConfig()) {
            if (null != this.serviceRegistration) {
                this.serviceRegistration.setProperties(properties);
            } else {
                this.serviceRegistration = componentContext.getBundleContext().registerService(new String[]{MailService.class.getName(), MessageGateway.class.getName()}, (Object) this, properties);
            }
            if (LOG.isDebugEnabled()) {
                Object[] arrObject = new Object[5];
                arrObject[0] = this.fromAddress;
                arrObject[1] = this.smtpHost;
                arrObject[2] = this.smtpPort;
                arrObject[3] = this.debugEmail;
                arrObject[4] = this.smtpUser == null ? "" : this.smtpUser;
                LOG.debug("MailService activated, fromAddress={}, smtpHost={}, smtpPort={}, debugEmail={}, smtpUser={}.", arrObject);
            }
        } else {
            LOG.error("Invalid mail service configuration.");
            this.unregister();
        }
    }

    private void unregister() {
        if (null != this.serviceRegistration) {
            this.serviceRegistration.unregister();
            this.serviceRegistration = null;
        }
    }


    @Override
    public void sendEmail(org.apache.commons.mail.Email email) {
        this.send(email);
    }

    @Override
    public boolean handles(Class type) {
        return Email.class.isAssignableFrom(type);
    }


    @Override
    public void send(Email email) {
        ClassLoader oldCL = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
        try {
            boolean hasHostName;
            if (!isEmptyConfiguration()) {
                throw new MailingException("Invalid mail service configuration.");
            }
            email.setDebug(debugEmail);
            setFromAddressToEmail(email);

            hasHostName = null != email.getHostName();
            if (!hasHostName) {
                email.setHostName(smtpHost);
                if (smtpSsl) {
                    email.setSSL(true);
                    email.setSslSmtpPort(String.valueOf(smtpPort));
                } else {
                    email.setSmtpPort(smtpPort);
                }
                if (smtpUser != null && smtpUser.trim().length() > 0) {
                    email.setAuthentication(smtpUser, smtpPassword);
                }
            }
            email.send();
        } catch (EmailException e) {
            throw new MailingException(e);
        } finally {
            Thread.currentThread().setContextClassLoader(oldCL);
        }
    }

    private void setFromAddressToEmail(final Email email) throws EmailException {
        if (email.getFromAddress() == null) {
            if (fromAddress == null || fromAddress.trim().length() == 0) {
                throw new EmailException("no from-address was given, cannot send message");
            }
            email.setFrom(fromAddress);
        }
    }

    private boolean isEmptyConfiguration() {
        if (null == smtpHost) {
            return false;
        }
        return true;
    }

    private boolean isValidConfig() {
        if (this.smtpPort < DEFAULT_SMTP_PORT) {
            LOG.error("invalid configuration: SMTP port must be 25 or greater.");
            return false;
        }
        if (StringUtils.isBlank(this.smtpHost)) {
            LOG.error("invalid configuration: SMTP host must not be blank.");
            return false;
        }
        return true;
    }
}
