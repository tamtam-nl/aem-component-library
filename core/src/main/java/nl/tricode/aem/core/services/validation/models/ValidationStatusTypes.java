package nl.tricode.aem.core.services.validation.models;

/**
 * Enum for validation statuses.
 */
public enum ValidationStatusTypes {

    VALIDATION_OK(200, "Validation OK"),

    REQUIRED_FIELD_EMPTY(100, "Required field is empty."),
    FIELD_LONGER_THAN_MAX_LENGTH(101, "Input cannot exceed %s characters."),
    FIELD_SHORTER_THAN_MIN_LENGTH(102,  "Input must be at least %s characters."),
    DATE_LATER_THAN_MAX_DATE(103, "The chosen date is later than max date."),
    DATE_BEFORE_MIN_DATE(104, "The chosen date is before min date."),
    SUBMITTED_VALUE_UNKNOWN(105, "The submitted value is unknown."),
    FILE_SIZE_BIGGER_THAN_MAX_SIZE(106, "The file is bigger than max size allowed."),
    FILE_TYPE_NOT_SUPPORTED(107, "The file type is not supported."),
    FIELD_VALUE_HIGHER_THAN_MAX_VALUE(108, "Value must be less then or equal to %s."),
    FIELD_VALUE_LOWER_THAN_MIN_VALUE(109, "Value must be greater then or equal to %s."),
    FIELD_NOT_ALPHA(110, "Accepts letters only."),
    FIELD_NOT_ALPHANUMERIC(111, "Accepts letters and numbers only."),
    FIELD_NOT_PHONE(112, "Please provide a valid phone number."),
    FIELD_NOT_URL(113, "Please provide a valid URL."),
    FIELD_NOT_EMAIL(114, "Please provide a valid email address."),
    FIELD_NOT_NUMBER(115, "Please provide a valid number value."),
    TEXT_FIELD_NOT_VALID(116, "The text field is not valid."),
    MIN_NUMBER_OF_SELECTIONS_MISSED(117, "Please select minimum %s values."),
    NO_WHITESPACE(118, "No whitespace allowed."),
    DATE_FORMAT_DOESNT_MATCH(109, "The date format is incorrect.");

    private final int statusCode;
    private final String description;

    ValidationStatusTypes(int statusCode, String description) {
        this.statusCode = statusCode;
        this.description = description;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getDescription() {
        return description;
    }
}
