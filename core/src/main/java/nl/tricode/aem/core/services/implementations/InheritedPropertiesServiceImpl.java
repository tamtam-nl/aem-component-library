package nl.tricode.aem.core.services.implementations;

import com.day.cq.wcm.api.Page;
import nl.tricode.aem.core.services.InheritedPropertiesService;
import nl.tricode.aem.core.utils.ResourceUtil;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of {@link nl.tricode.aem.core.services.InheritedPropertiesService }
 */
@Component(immediate = true, metatype = false, label = "ACL - Inherited Properties Service")
@Service(InheritedPropertiesService.class)
public class InheritedPropertiesServiceImpl implements InheritedPropertiesService {
    private static final Logger LOG = LoggerFactory.getLogger(InheritedPropertiesServiceImpl.class);

    @Override
    public final <T> T getInheritedPropertyValue(final String propertyName, final Resource resource) {
        Object inheritedPropertyValue = findInheritedPropertyValue(propertyName, resource);
        if (inheritedPropertyValue != null) {
            return (T) inheritedPropertyValue;
        }
        LOG.debug("The property value for: {} on node: {} is not of type String", propertyName, resource);
        return null;
    }

    private static Object findInheritedPropertyValue(final String propertyName, final Resource resource) {
        final Page resourcePage = ResourceUtil.getResourcePage(resource);
        final String componentPath = resource.getPath().substring(resourcePage.getContentResource().getPath().length() + 1);

        Page page = resourcePage.getParent();
        Object propertyValue = null;

        while (page != null) {
            final Resource componentResource = page.getContentResource(componentPath);

            if(componentResource != null) {
                propertyValue = componentResource.getValueMap().get(propertyName);

                if (propertyValue != null) {
                    return propertyValue;
                }
            }

            page = page.getParent();
        }

        LOG.debug("Cannot find property value for: {} on node: {}", propertyName, resource);
        return propertyValue;
    }
}
