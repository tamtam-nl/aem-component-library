package nl.tricode.aem.core.models.form.password;

import nl.tricode.aem.core.models.form.FormFieldModel;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.inject.Inject;

@Model(adaptables = Resource.class)
public class PasswordModel extends FormFieldModel {


    @Inject
    @Optional
    private String elementName;

    @Inject
    @Optional
    private String title;

    @Inject
    @Optional
    private String hideTitle;

    @Inject
    @Optional
    private String required;

    @Inject
    @Optional
    private String requiredMessage;

    @Inject
    @Optional
    private boolean autocomplete;

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHideTitle() {
        return hideTitle;
    }

    public void setHideTitle(String hideTitle) {
        this.hideTitle = hideTitle;
    }

    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }

    public String getRequiredMessage() {
        return requiredMessage;
    }

    public void setRequiredMessage(String requiredMessage) {
        this.requiredMessage = requiredMessage;
    }

    public boolean getAutocomplete() {
        return autocomplete;
    }
}
