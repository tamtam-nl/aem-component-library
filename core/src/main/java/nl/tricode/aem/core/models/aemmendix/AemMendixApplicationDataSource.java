package nl.tricode.aem.core.models.aemmendix;

import com.adobe.granite.ui.components.ds.DataSource;
import com.adobe.granite.ui.components.ds.SimpleDataSource;
import com.adobe.granite.ui.components.ds.ValueMapResource;
import nl.tricode.aem.core.utils.ConfigurationUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceMetadata;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.wrappers.ValueMapDecorator;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Model(adaptables = SlingHttpServletRequest.class)
public class AemMendixApplicationDataSource {

    public static final String CLOUDSERVICE_MENDIX = "/etc/cloudservices/mendix";
    public static final String NO_CONFIGURATIONS_TEXT = "There are no applications configured";
    public static final String NO_CONFIGURATIONS_VALUE = "noConfigurations";
    public static final String VALUE_KEY = "value";
    public static final String TEXT_KEY = "text";
    public static final String PROXY_PATH = "proxyPath";
    public static final String APPLICATION_NAME = "name";
    private Resource resource;

    @Self
    private SlingHttpServletRequest request;


    @PostConstruct
    public void activate() {
        final ResourceResolver resourceResolver = this.request.getResourceResolver();

        final ArrayList<Resource> resourceList = new ArrayList<>();

        ConfigurationUtil.getConfigurations(resourceResolver, CLOUDSERVICE_MENDIX).forEachRemaining(configuration -> {
            ValueMap properties = configuration.getProperties();
            String proxyPath = properties.get(PROXY_PATH, String.class);
            String name = properties.get(APPLICATION_NAME, String.class);

            final Map<String, Object> map = new HashMap<>();
            if (StringUtils.isNotEmpty(proxyPath) && StringUtils.isNotEmpty(name)) {
                map.put(VALUE_KEY, proxyPath);
                map.put(TEXT_KEY, name);
                resourceList.add(generateValueMapResource(resourceResolver, map));
            }
        });

        if(resourceList.size() == 0) {
            final Map<String, Object> map = new HashMap<>();
            map.put(VALUE_KEY, NO_CONFIGURATIONS_VALUE);
            map.put(TEXT_KEY, NO_CONFIGURATIONS_TEXT);
            resourceList.add(generateValueMapResource(resourceResolver, map));
        }

        request.setAttribute(DataSource.class.getName(), new SimpleDataSource(resourceList.iterator()));
    }

    private ValueMapResource generateValueMapResource(ResourceResolver resourceResolver, Map<String, Object> valueMapDecorator) {
        return new ValueMapResource(
                resourceResolver,
                new ResourceMetadata(),
                "",
                new ValueMapDecorator(valueMapDecorator));
    }
}
