package nl.tricode.aem.core.services;

import com.day.cq.wcm.api.Page;
import nl.tricode.aem.core.models.megamenu.MenuItem;
import org.apache.sling.api.SlingHttpServletRequest;

import java.util.List;

/**
 * Service that provides the navigation items one or 2 level deep.
 */
@FunctionalInterface
public interface MenuItemsService {
    List<MenuItem> getMenuItems(final Page rootPage, final SlingHttpServletRequest request, final int pageLevel);
}
