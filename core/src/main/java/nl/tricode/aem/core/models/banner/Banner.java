package nl.tricode.aem.core.models.banner;

import nl.tricode.aem.core.utils.LinkUtil;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;

import javax.inject.Inject;

@Model(adaptables = SlingHttpServletRequest.class)
public class Banner {

    /**
     * Image of the Banner component
     */
    @Inject
    @Optional
    @Via("resource")
    private String fileReference;

    /**
     * Link of the Banner component
     */
    @Inject
    @Optional
    @Via("resource")
    private String link;

    /**
     * Background repeat
     */
    @Inject
    @Optional
    @Via("resource")
    private String backgroundRepeat;

    /**
     * Banner height
     */
    @Inject
    @Optional
    @Via("resource")
    private String height;

    @Inject
    private SlingHttpServletRequest request;

    public String getFileReference() {
        return fileReference;
    }

    public String getLink() {
        return LinkUtil.pathBrowserPropertyToLink(request, link);
    }

    public String getBackgroundRepeat() {
        return backgroundRepeat;
    }

    public String getHeight() {
        return height;
    }
}
