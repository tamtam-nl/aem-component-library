package nl.tricode.aem.core.models.form;

import nl.tricode.aem.core.ACLConstants;
import nl.tricode.aem.core.models.form.selectable.SelectableBuilder;
import nl.tricode.aem.core.models.form.selectable.SelectableItem;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Model(adaptables = Resource.class)
public class SelectModel extends FormFieldModel {

    @Inject
    @Optional
    @Named(ACLConstants.SELECTABLE_ITEMS_PROPERTY)
    protected String[] items;

    @Inject
    @Optional
    @Named(ACLConstants.SHOW_EMPTY_OPTION_PROPERTY)
    protected String showEmptyOption;

    @Inject
    @Optional
    protected String emptyPlaceholder;

    public List<SelectableItem> getSelectItems() {
        List<SelectableItem> selectableItems = SelectableBuilder.buildSelectableItems(items);
        if (Boolean.parseBoolean(this.showEmptyOption)) {
            SelectableItem newItem = new SelectableItem(emptyPlaceholder, "");
            selectableItems.add(0, newItem);
        }
        return selectableItems;
    }

    public String getShowEmptyOption() {
        return showEmptyOption;
    }

    public String getEmptyPlaceholder() {
        return emptyPlaceholder;
    }
}
