package nl.tricode.aem.core.services.validation.commands.textfield.textvalidators.factory;


import nl.tricode.aem.core.ACLConstants;
import nl.tricode.aem.core.services.validation.commands.textfield.textvalidators.*;
import nl.tricode.aem.core.services.validation.models.ValidatorStatus;
import org.apache.sling.api.resource.Resource;

import java.util.HashMap;

/**
 *
 */
public class RegexValidationFactory {

    private final HashMap<String, RegexValidator> regexValidations = new HashMap<>();

    public RegexValidationFactory() {
        this.regexValidations.put(ACLConstants.TEXT_PROPERTY, new TextRegexValidator());
        this.regexValidations.put(ACLConstants.EMAIL_PROPERTY, new EmailRegexValidator());
        this.regexValidations.put(ACLConstants.TEL_PROPERTY, new TelRegexValidator());
        this.regexValidations.put(ACLConstants.URL_PROPERTY, new UrlRegexValidator());
        this.regexValidations.put(ACLConstants.NUMBER_PROPERTY, new NumberRegexValidator());
    }

    public String getRegex(final Resource resource) {
        if (resource != null) {
            Object type = resource.getValueMap().get(ACLConstants.TYPE_PROPERTY);
            if (type != null) {
                RegexValidator regexValidator = regexValidations.get(type);
                return regexValidator.getRegex(resource);
            }
        }
        return null;
    }

    public ValidatorStatus getFalseValidatorStatus(final Resource resource) {
        if (resource != null) {
            Object type = resource.getValueMap().get(ACLConstants.TYPE_PROPERTY);
            if (type != null) {
                RegexValidator regexValidator = regexValidations.get(type);
                return regexValidator.getFalseValidatorStatus(resource);
            }
        }
        return null;
    }
}
