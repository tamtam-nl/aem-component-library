package nl.tricode.aem.core.services.validation.commands.textfield.textvalidators;

import nl.tricode.aem.core.services.validation.models.ValidatorStatus;
import org.apache.sling.api.resource.Resource;

/**
 *
 */
public interface RegexValidator {

    public String getRegex(final Resource resource);

    public ValidatorStatus getFalseValidatorStatus(final Resource resource);
}
