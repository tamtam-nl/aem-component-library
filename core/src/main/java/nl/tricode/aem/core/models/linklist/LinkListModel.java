package nl.tricode.aem.core.models.linklist;

import nl.tricode.aem.core.utils.granite.MultifieldOptionBuilder;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * Model for the link list component
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class LinkListModel {

    private static final Logger LOGGER = LoggerFactory.getLogger(LinkListModel.class);

    /**
     * Size of the title of the link which is set in the dialog
     */
    @Inject
    @Optional
    @Via("resource")
    private String size;

    /**
     * Open link in a new page?
     */
    @Inject
    @Optional
    @Via("resource")
    private boolean newPage;

    /**
     * Alignment of the link which is set in the dialog
     */
    @Inject
    @Optional
    @Via("resource")
    private String align;

    /**
     * Bullet style of the link which is set in the dialog
     */
    @Inject
    @Optional
    @Via("resource")
    private String bulletStyle;

    /**
     * Text color of the link which is set in the dialog
     */
    @Inject
    @Optional
    @Via("resource")
    private String textColor;

    /**
     * Text font weight of the link which is set in the dialog
     */
    @Inject
    @Optional
    @Via("resource")
    private String fontWeight;

    /**
     * Hovereffect of the link which is set in the dialog
     */
    @Inject
    @Optional
    @Via("resource")
    private String hoverEffect;

    /**
     * The social icons options.
     */
    @Inject
    @Default(values = {})
    @Via("resource")
    private String[] listItems;

    public String getSize() {
        return size;
    }

    public boolean isNewPage() {
        return newPage;
    }

    public String getAlign() {
        return align;
    }

    public String getTextColor() {
        return textColor;
    }

    public String getHoverEffect() {
        return hoverEffect;
    }

    public String getBulletStyle() {
        return bulletStyle;
    }

    public String getFontWeight() {
        return fontWeight;
    }

    public List<LinkListOption> getListItems() {
        final MultifieldOptionBuilder<LinkListOption> multifieldOptionBuilder = new MultifieldOptionBuilder<>();

        try {
            return multifieldOptionBuilder.buildOptions(listItems, LinkListOption.class);
        } catch (final IOException e) {
            LOGGER.error("Unable to populate list of social icons {}", listItems, e);
        }

        return Collections.EMPTY_LIST;
    }

    public boolean isConfigured() {
        return !getListItems().isEmpty();
    }
}
