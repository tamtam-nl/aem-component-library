package nl.tricode.aem.core.models.sitemap;

/**
 * Model for sitemap element
 */
public class SitemapElement {

    private String location;
    private String lastModified; //format YYYY-MM-DD
    private String changeFrequency;
    private String priority;

    public SitemapElement(String location, String lastModified, String changeFrequency, String priority) {
        this.location = location;
        this.lastModified = lastModified;
        this.changeFrequency = changeFrequency;
        this.priority = priority;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public String getChangeFrequency() {
        return changeFrequency;
    }

    public void setChangeFrequency(String changeFrequency) {
        this.changeFrequency = changeFrequency;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }
}
