package nl.tricode.aem.core.models.megamenu;

import com.day.cq.wcm.api.Page;
import nl.tricode.aem.core.services.MenuItemsService;
import nl.tricode.aem.core.utils.PageUtil;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.inject.Inject;
import java.util.List;

/**
 * Navigation menu component. Does not have any configuration.
 */
@Model(adaptables = {SlingHttpServletRequest.class})
public class MenuItemModel {

    private static final int NAVIGATION_PAGE_LEVEL = 3;

    @Inject
    private MenuItemsService menuItemsService;

    @Inject @Optional
    private Boolean hierarchy;

    @Self
    private SlingHttpServletRequest request;

    @Inject @Optional
    private Page currentPage;

    public List<MenuItem> getItems() {
        //find homepage
        final Page homePage = PageUtil.getSiteLanguageRoot(currentPage);

        List<MenuItem> navigationItems = menuItemsService.getMenuItems(homePage, request, NAVIGATION_PAGE_LEVEL);

        return navigationItems;
    }
}
