package nl.tricode.aem.core.models.form.selectable;

import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Builder class that builds selectable items out of json objects that contain text/value
 */
public class SelectableBuilder {

    private static final Logger LOGGER = LoggerFactory.getLogger(SelectableBuilder.class);

    public static List<SelectableItem> buildSelectableItems(String [] items) {
        List<SelectableItem> selectableItemList = new ArrayList<>();

        for (String item : items) {
            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(item);
                SelectableItem selectableItem = new SelectableItem(jsonObject.getString("text"), jsonObject.getString("value"));

                selectableItemList.add(selectableItem);
            } catch (JSONException e) {
                LOGGER.warn("Unable to map items out of: %s", items, e);
            }
        }
        return selectableItemList;
    }
}
