package nl.tricode.aem.core.models.footer;


import nl.tricode.aem.core.services.InheritedPropertiesService;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Model for the footer component used to retrieve the values from the component's dialog
 */
@Model(adaptables = Resource.class)
public class FooterModel {
    public static final String BACKGROUND_COLOR = "backgroundColor";
    public static final String BACKGROUND_COLOR_LIGHT = "bg-color-light";

    /**
     * Service for retrieving the properties from component in the parent page
     */
    @Inject
    private InheritedPropertiesService inheritedPropertiesService;

    /**
     * Resource of the footer component
     */
    @Self
    Resource resource;


    /**
     * The background color which is chosen in the dialog of the component
     */
    @Inject @Optional
    private String backgroundColor;

    /**
     * Init method for initializing the backgroundColor if it is not set in the dialog.
     */
    @PostConstruct
    protected void init(){
        initializeBackgroundColor();
    }

    private void initializeBackgroundColor() {
        if (backgroundColor == null) {
            backgroundColor = inheritedPropertiesService.getInheritedPropertyValue(BACKGROUND_COLOR, resource);

            if (backgroundColor == null) {
                backgroundColor = BACKGROUND_COLOR_LIGHT;
            }
        }
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }
}
