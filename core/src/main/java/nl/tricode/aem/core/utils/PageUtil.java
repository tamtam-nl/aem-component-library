package nl.tricode.aem.core.utils;

import com.day.cq.wcm.api.Page;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PageUtil {

    private static final Logger LOG = LoggerFactory.getLogger(PageUtil.class);
    
    private PageUtil() {}

    /**
     * The root page is the lowest level page of type homepage. 
     * We can't use the level site sites may be grouped into folders.
     * 
     * @param page the current page
     * @return the root page or null if it cant be found
     */
    public static Page getSiteRoot(final Page page) {
        Page currentPage = page;
        while (true) {
            if (currentPage == null) {
                return null;
            }
            if (isDeepestHomepage(currentPage)) {
                return currentPage;
            }
            currentPage = currentPage.getParent();
        }
    }

    /**
     * The top content page is the top level page under /content.
     *
     * @param page the current page
     * @return the root page or null if it cant be found
     */
    public static Page getTopContentPage(final Page page) {
        Page currentPage = page;
        while (true) {
            if (currentPage == null) {
                return null;
            }
            if (currentPage.getParent() == null) {
                return currentPage;
            }
            currentPage = currentPage.getParent();
        }
    }

    /**
     * The root language page is the language page.
     * We can't use the level site sites may be grouped into folders.
     *
     * @param page the current page
     * @return the root language page or null if it cant be found
     */
    public static Page getSiteLanguageRoot(final Page page) {
        Page currentPage = page;
        while (true) {
            if (currentPage == null) {
                LOG.error("Could not find language page for: {}", page);
                return null;
            }
            if (isLanguagePage(currentPage)) {
                return currentPage;
            }
            currentPage = currentPage.getParent();
        }
    }

    /**
     * Gets the navigational title of a page. Fall back to the regular tile or node title if not set. 
     * @param page the page
     * @return title to show in navigation
     */
    public static String getNavigationTitleWithFallback(final Page page) {
        final String navTitle = page.getNavigationTitle();
        if (StringUtils.isNotEmpty(navTitle)) {
            return navTitle;
        }
        final String pageTitle = page.getPageTitle();
        if (StringUtils.isNotEmpty(pageTitle)) {
            return pageTitle;
        } 
        final String title = page.getTitle();
        if (StringUtils.isNotEmpty(title)) {
            return title;
        }
        return page.getName();
    }
    
    ////// private helper methods //////

    /**
     * This method won't work until we get the home page template done
     *
     * @param page
     * @return
     */
    private static boolean isHomePage(final Page page) {
        final String templatePath = page.getTemplate().getPath();
        return templatePath.endsWith("acl/templates/page-home");
    }
    
    private static boolean isDeepestHomepage(final Page page) {
        return isHomePage(page) && (page.getParent() == null || !isHomePage(page.getParent()));
    }

    private static boolean isLanguagePage(final Page page) {
        if(page.getParent()==null){
            return false;
        }
        return page.getParent().getParent()==null;
    }
}
