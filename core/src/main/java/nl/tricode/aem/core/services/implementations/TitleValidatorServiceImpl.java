package nl.tricode.aem.core.services.implementations;

import com.day.cq.wcm.api.Page;
import nl.tricode.aem.core.services.TitleValidatorService;
import nl.tricode.aem.core.utils.QueryUtil;
import nl.tricode.aem.core.utils.ResourceUtil;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

/**
 * Implementation of {@link nl.tricode.aem.core.services.TitleValidatorService}
 */
@Component(immediate = true, metatype = false, label = "Service for checking existing H1 tags on page.")
@Service(TitleValidatorService.class)
public class TitleValidatorServiceImpl implements TitleValidatorService {

    private static final String H1_TAG = "h1";
    @Reference
    private ResourceResolverFactory resourceFactory;

    private static final String QUERY_FIND_TITLE_COMPONENT_NODES = "SELECT * FROM [nt:unstructured] AS node WHERE ISDESCENDANTNODE(node, '%s') AND contains(node.*, 'title')";
    private static final Logger LOG = LoggerFactory.getLogger(TitleValidatorServiceImpl.class);

    @Override
    public int isValid(Page currentPage, Resource currentResource) {
        String currentPagePath = currentPage.getPath();
        final String queryString = String.format(QUERY_FIND_TITLE_COMPONENT_NODES, currentPagePath + "/jcr:content");

        if (resourceFactory != null) {
            final ResourceResolver resourceResolver = ResourceUtil.getResourceResolver(resourceFactory);
            final Session session = resourceResolver.adaptTo(Session.class);

            final NodeIterator nodeIterator = QueryUtil.getResultNodeIteratorForQuery(queryString, session);
            final int[] numberOfH1Titles = {0};

            nodeIterator.forEachRemaining(titleComponent -> {
                try {
                    if (!((Node) titleComponent).getPath().equals(currentResource.getPath()) && H1_TAG.equals(((Node) titleComponent).getProperty("type").getString())) {
                        numberOfH1Titles[0]++;
                    }
                } catch (RepositoryException e) {
                    LOG.info("Failed to get node path", e);
                }
            });
            resourceResolver.close();
            return numberOfH1Titles[0];
        }
        return 0;
    }
}