package nl.tricode.aem.core.models.form.datepicker;

import nl.tricode.aem.core.models.genericdatasource.GenericDataSource;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;

@Model(adaptables = SlingHttpServletRequest.class)
public class DatepickerDateFormatDataSource extends GenericDataSource {

    @PostConstruct
    public void activate() {
        loadCustomDataSource("dateFormat");
    }
}
