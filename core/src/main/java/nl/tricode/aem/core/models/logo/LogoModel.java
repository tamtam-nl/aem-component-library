package nl.tricode.aem.core.models.logo;

import nl.tricode.aem.core.services.InheritedPropertiesService;
import nl.tricode.aem.core.utils.LinkUtil;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Sling model for logo component. Uses inherited properties functionality.
 */

@Model(adaptables = SlingHttpServletRequest.class)
public class LogoModel{

    @Inject
    private InheritedPropertiesService inheritedPropertiesService;

    @Inject
    private SlingHttpServletRequest request;

    @SlingObject
    Resource resource;

    @Inject
    @Optional
    @Via("resource")
    private String linkURL;

    @Inject
    @Optional @Via("resource")
    private String fileReference;

    @Inject
    @Optional @Via("resource")
    private String alt;

    @Inject
    @Optional @Via("resource")
    private String inlineSVG;

    @Inject
    @Optional @Via("resource")
    private String disableInheritance;

    private static final Logger LOG = LoggerFactory.getLogger(LogoModel.class);

    /**
     * Postconstructor method that checks if a property is available and inherits it if not.
     */
    @PostConstruct
    protected void init() {

        if(disableInheritance == null) {
            fileReference = inheritedPropertiesService.getInheritedPropertyValue(LogoProperties.FILE_REFERENCE.getPropertyName(), resource);
            linkURL = inheritedPropertiesService.getInheritedPropertyValue(LogoProperties.LINK_URL.getPropertyName(), resource);
            alt = inheritedPropertiesService.getInheritedPropertyValue(LogoProperties.ALT.getPropertyName(), resource);
            inlineSVG = inheritedPropertiesService.getInheritedPropertyValue(LogoProperties.INLINE_SVG.getPropertyName(), resource);
        }
    }

    public String getFileReference() {
        return fileReference;
    }

    public String getAlt() {
        return alt;
    }

    public String getInlineSVG() {
        return inlineSVG;
    }

    public String getLinkURL() {
        return LinkUtil.pathBrowserPropertyToLink(request, linkURL);
    }

    /**
     * Private enum for the Logo component property names.
     */
    private enum LogoProperties {
        FILE_REFERENCE("fileReference"),
        LINK_URL("linkURL"),
        INLINE_SVG("inlineSVG"),
        ALT("alt");

        private final String propertyName;

        LogoProperties(String propertyName) {
            this.propertyName = propertyName;
        }

        public String getPropertyName() {
            return propertyName;
        }
    }
}
