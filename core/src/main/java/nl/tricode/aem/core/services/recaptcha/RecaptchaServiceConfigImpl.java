package nl.tricode.aem.core.services.recaptcha;

import org.apache.felix.scr.annotations.*;
import org.osgi.service.component.ComponentContext;

import java.util.Dictionary;

@Service(value = RecaptchaServiceConfig.class)
@Component(immediate = true, metatype = true, name = "nl.tricode.aem.core.services.recaptcha.RecaptchaServiceConfig",
        description = "reCAPTCHA (I'm not a robot) Configuration set for a single website. By default all keys work on \"localhost\". Sign up for API keys at https://www.google.com/recaptcha/admin",
        label = "ACL Google reCAPTCHA service config")

public class RecaptchaServiceConfigImpl implements RecaptchaServiceConfig {

    @Property(name = "recaptcha.site", label = "Site root name", description = "Root node name of the site the configuration is applicable for", value = "")
    public static final String PROPERTY_SITE = "recaptcha.site";

    @Property(name = "recaptcha.siteverify.url", label = "Siteverify URL", description = "The URL to the Google reCAPTCHA API to verify a user's response to a reCAPTCHA challenge", value = "https://www.google.com/recaptcha/api/siteverify")
    private static final String PROPERTY_URL = "recaptcha.siteverify.url";

    @Property(name = "recaptcha.secret", label = "Secret", description = "The secret authorizes communication between your application backend and the reCAPTCHA server to verify the user's response", value = "")
    private static final String PROPERTY_SECRET = "recaptcha.secret";

    @Property(name = "recaptcha.sitekey", label = "Site Key", description = "The site key is used to display the widget on your site.", value = "")
    private static final String PROPERTY_SITE_KEY = "recaptcha.sitekey";

    @Property(name = "recaptcha.enabled", label = "Enabled", description = "When disabled the component will not be rendered, which is useful for automated testing.", boolValue = true)
    private static final String PROPERTY_ENABLED = "recaptcha.enabled";

    private String site;
    private String url;
    private String secret;
    private String siteKey;
    private boolean enabled;

    @Override
    public String getSite() {
        return site;
    }

    @Override
    public String getSiteKey() {
        return siteKey;
    }

    @Override
    public String getSecret() {
        return secret;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Activate
     public void activate(final ComponentContext componentContext)  {

        final Dictionary properties = componentContext.getProperties();
        if(properties != null) {
            site = properties.get(PROPERTY_SITE).toString();
            url = properties.get(PROPERTY_URL).toString();
            secret = properties.get(PROPERTY_SECRET).toString();
            siteKey = properties.get(PROPERTY_SITE_KEY).toString();
            enabled = Boolean.valueOf(properties.get(PROPERTY_ENABLED).toString());
        }
    }

}
