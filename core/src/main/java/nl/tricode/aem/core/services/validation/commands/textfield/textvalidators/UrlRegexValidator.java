package nl.tricode.aem.core.services.validation.commands.textfield.textvalidators;

import nl.tricode.aem.core.services.validation.ValidationConstants;
import nl.tricode.aem.core.services.validation.models.ValidationStatusTypes;
import nl.tricode.aem.core.services.validation.models.ValidatorStatus;
import org.apache.sling.api.resource.Resource;

/**
 *
 */
public class UrlRegexValidator implements RegexValidator {

    @Override
    public String getRegex(final Resource resource) {
        return ValidationConstants.URL_REGEX;
    }

    @Override
    public ValidatorStatus getFalseValidatorStatus(final Resource resource) {
        return new ValidatorStatus(ValidationStatusTypes.FIELD_NOT_URL.getStatusCode(), ValidationStatusTypes.FIELD_NOT_URL.getDescription(), resource);
    }
}
