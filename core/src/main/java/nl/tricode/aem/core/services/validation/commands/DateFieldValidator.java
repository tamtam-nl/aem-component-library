package nl.tricode.aem.core.services.validation.commands;

import nl.tricode.aem.core.ACLConstants;
import nl.tricode.aem.core.services.validation.models.ValidationStatusTypes;
import nl.tricode.aem.core.services.validation.models.ValidatorStatus;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Validator for the date field component
 */
public class DateFieldValidator extends FieldValidator {

    private static final Logger LOG = LoggerFactory.getLogger(DateFieldValidator.class);
    private static Date chosenDate = null;

    @Override
    public List<ValidatorStatus> validate(final String[] values, final Resource resource, final Map<String, RequestParameter[]> requestParameters) {
        List<ValidatorStatus> validatorStatusList = new ArrayList<>();

        validatorStatusList.add(validateDateFormat(values, resource));
        validatorStatusList.add(validateRequired(values, resource));
        validatorStatusList.add(validateMaxDate(values, resource));
        validatorStatusList.add(validateMinDate(values, resource));

        return validatorStatusList;
    }

    ValidatorStatus validateMaxDate(final String[] values, final Resource resource) {
        return validateDate(values, resource, ACLConstants.MAX_DATE_PROPERTY, ACLConstants.CHECK_MAX_DATE_PROPERTY);
    }

    ValidatorStatus validateMinDate(final String[] values, final Resource resource) {
        return validateDate(values, resource, ACLConstants.MIN_DATE_PROPERTY, ACLConstants.CHECK_MIN_DATE_PROPERTY);
    }


    ValidatorStatus validateDate(final String[] values, final Resource resource, final String dateLimitParameter, final String checkDateParameter) {
        Object checkDateProperty = resource.getValueMap().get(checkDateParameter);
        if (checkDateProperty != null) {
            Object dateLimitProperty = resource.getValueMap().get(dateLimitParameter);
            if (chosenDate == null) {
                chosenDate = getDate(values, resource);
            }
            if (dateLimitProperty != null && chosenDate != null) {
                final Date limitDate = ((GregorianCalendar) dateLimitProperty).getTime();

                if (dateLimitParameter.equals(ACLConstants.MAX_DATE_PROPERTY) && chosenDate.after(limitDate)) {
                    return new ValidatorStatus(ValidationStatusTypes.DATE_LATER_THAN_MAX_DATE.getStatusCode(), ValidationStatusTypes.DATE_LATER_THAN_MAX_DATE.getDescription(), resource);
                } else {
                    if (dateLimitParameter.equals(ACLConstants.MIN_DATE_PROPERTY) && chosenDate.before(limitDate)) {
                        return new ValidatorStatus(ValidationStatusTypes.DATE_BEFORE_MIN_DATE.getStatusCode(), ValidationStatusTypes.DATE_BEFORE_MIN_DATE.getDescription(), resource);
                    }
                }
            }
        }
        return new ValidatorStatus(ValidationStatusTypes.VALIDATION_OK.getStatusCode(), ValidationStatusTypes.VALIDATION_OK.getDescription(), resource);
    }

    private static ValidatorStatus validateDateFormat(final String[] values, final Resource resource) {
        Object dateFormatProperty = resource.getValueMap().get(ACLConstants.DATE_FORMAT_PROPERTY);
        if (dateFormatProperty != null && getDate(values, resource) == null) {
            return new ValidatorStatus(ValidationStatusTypes.DATE_FORMAT_DOESNT_MATCH.getStatusCode(), ValidationStatusTypes.DATE_FORMAT_DOESNT_MATCH.getDescription(), resource);
        }
        return new ValidatorStatus(ValidationStatusTypes.VALIDATION_OK.getStatusCode(), ValidationStatusTypes.VALIDATION_OK.getDescription(), resource);
    }

    private static Date getDate(final String[] values, final Resource resource) {
        Object dateFormatProperty = resource.getValueMap().get(ACLConstants.DATE_FORMAT_PROPERTY);
        if (dateFormatProperty != null && values != null && !ArrayUtils.isEmpty(values)) {
            try {
                final DateFormat dateFormat = new SimpleDateFormat(dateFormatProperty.toString());
                chosenDate = dateFormat.parse(values[0]);
            } catch (ParseException e) {
                LOG.error("Cannot parse the limitDate: ", e.getMessage());
            }
        }
        return chosenDate;
    }

}
