package nl.tricode.aem.core.services.recaptcha;


/**
 * Service for dealing with inherited properties.
 */
public interface RecaptchaServiceConfig {



    String getSite();
    /**
     * Gets the recaptcha site key specified in the OSGi configuration
     */
    String getSiteKey();
    /**
     * Gets the recaptcha secret specified in the OSGi configuration
     */
    String getSecret();
    /**
     * Gets the recaptcha check URL specified in the OSGi configuration
     */
    String getUrl();

    boolean isEnabled();
}
