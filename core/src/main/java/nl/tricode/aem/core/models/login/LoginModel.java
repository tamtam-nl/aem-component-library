package nl.tricode.aem.core.models.login;


import nl.tricode.aem.core.utils.LinkUtil;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;

import javax.inject.Inject;

/**
 * Model for the Login component
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class LoginModel {

    @Inject
    @Optional
    @Via("resource")
    private String endpoint;

    @Inject
    private SlingHttpServletRequest request;

    public String getEndpoint() {
        return endpoint;
    }
}