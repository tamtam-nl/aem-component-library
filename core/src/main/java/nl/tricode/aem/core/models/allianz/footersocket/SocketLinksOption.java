package nl.tricode.aem.core.models.allianz.footersocket;


public class SocketLinksOption {

    private String linkText;
    private String title;
    private String link;

    public SocketLinksOption() {
    }


    public String getLinkText() {
        return linkText;
    }

    public void setLinkText(String linkText) {
        this.linkText = linkText;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SocketLinksOption that = (SocketLinksOption) o;

        if (link != null ? !link.equals(that.link) : that.link != null) return false;
        if (linkText != null ? !linkText.equals(that.linkText) : that.linkText != null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = linkText != null ? linkText.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (link != null ? link.hashCode() : 0);
        return result;
    }
}
