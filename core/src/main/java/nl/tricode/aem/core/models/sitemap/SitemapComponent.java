package nl.tricode.aem.core.models.sitemap;

import com.day.cq.wcm.api.Page;

import nl.tricode.aem.core.services.SitemapService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.inject.Inject;
import java.util.List;

/**
 * Sitemap component. Does not have any configuration.
 */
@Model(adaptables = {SlingHttpServletRequest.class})
public class SitemapComponent {

    @Inject
    private SitemapService sitemapService;

    @Self
    private SlingHttpServletRequest request;

    @Inject
    private Page currentPage;

    public List<SitemapElement> getItems(){

        return sitemapService.getListOfSitemapElements(request, currentPage);

    }
}
