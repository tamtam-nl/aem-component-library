package nl.tricode.aem.core.services.search;

import nl.tricode.aem.core.services.search.models.SearchResultSummary;
import org.apache.sling.api.SlingHttpServletRequest;

/**
 * Search service, simplifies full text searches.
 */
@FunctionalInterface
public interface SearchService {

    /**
     * Perform a search on the given keywords
     * @param request the sling http request
     * @param keywords keywords to search for
     * @param startPage start page (first page = 1)
     * @param resultsPerPage result per page to return
     * @param searchInPath path to search from
     * @return CQ SearchResults object
     */
    SearchResultSummary search(final SlingHttpServletRequest request,
                               final String keywords,
                               final long startPage,
                               final long resultsPerPage,
                               final String searchInPath);
}
