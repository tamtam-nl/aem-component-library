package nl.tricode.aem.core.services.implementations;

import com.day.cq.commons.Language;
import com.day.cq.wcm.api.Page;
import nl.tricode.aem.core.ACLConstants;
import nl.tricode.aem.core.models.page.AlternatePage;
import nl.tricode.aem.core.services.AlternatePagesService;
import nl.tricode.aem.core.utils.PathUtil;
import nl.tricode.aem.core.utils.ResourceUtil;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of {@link nl.tricode.aem.core.services.AlternatePagesService}
 */
@Component(immediate = true, metatype = false, label = "Alternate Language Pages Service")
@Service(AlternatePagesService.class)
public class AlternateLanguagePagesService implements AlternatePagesService {

    @Reference
    private ResourceResolverFactory resourceFactory;


    @Override
    public List<AlternatePage> getAlternatePages(final String pagePath) {

        final ResourceResolver resourceResolver = ResourceUtil.getResourceResolver(resourceFactory);
        final String rootPath = PathUtil.getRootPath(pagePath);
        final String relativePathToLanguage = PathUtil.getRelativeToLanguagePath(pagePath);

        final List<AlternatePage> listOfAlternatePages = new ArrayList<>();
        if (relativePathToLanguage != null) {
            getListOfLanguagesForRootPage(rootPath, resourceResolver).forEach(item -> {
                String languagePath = getPathForLanguage(rootPath, relativePathToLanguage, item);
                if (resourceResolver.getResource(languagePath) != null && !pagePath.equals(languagePath)) {
                    //adding '.html' extension to the paths
                    languagePath += ACLConstants.EXTENSION_PAGE;
                    listOfAlternatePages.add(new AlternatePage(item, languagePath));
                }
            });
        }
        if (resourceResolver != null) {
            resourceResolver.close();
        }

        return listOfAlternatePages;
    }


    /**
     * Builds the path for specific language
     *
     * @param rootPath
     * @param relativePathToLanguage
     * @param language
     * @return string path to the page for specific language
     */
    private String getPathForLanguage(final String rootPath, final String relativePathToLanguage, final Language language) {
        final StringBuilder sb = new StringBuilder();
        sb.append(rootPath);
        sb.append("/");
        sb.append(language.getLocale().toString());
        sb.append(relativePathToLanguage);
        return sb.toString();
    }

    /**
     * Finds list of languages for specific site (rootPage)
     *
     * @param rootPath
     * @param resourceResolver
     * @return list of languages that are available for specific site
     */
    private List<Language> getListOfLanguagesForRootPage(final String rootPath, final ResourceResolver resourceResolver) {
        final List<Language> listOfLanguagesUnderRootPath = new ArrayList<>();

        final Resource rootResource = resourceResolver.getResource(rootPath);
        if (rootResource != null) {
            final Page page = rootResource.adaptTo(Page.class);
            if (page != null) {
                page.listChildren().forEachRemaining(item -> listOfLanguagesUnderRootPath.add(new Language(item.getName())));
            }
        }

        return listOfLanguagesUnderRootPath;
    }
}
