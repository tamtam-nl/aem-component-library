package nl.tricode.aem.core.models.megamenu;

import nl.tricode.aem.core.services.MenuItemsService;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Model for the Mega Menu component used to retrieve the values from the component's dialog
 */
@Model(adaptables = Resource.class)
public class MegaMenuModel {
    public static final Integer COLUMN_NUMBER_DEFAULT  = 4;
    /**
     * Column number set in the dialog
     */
    @Inject @Optional
    private Integer columnNumber;

    public List<String> getLayout() {
        List<String> columnList = new ArrayList<String>();

        if(columnNumber != null) {
            String[] parsys = new String [columnNumber==0?COLUMN_NUMBER_DEFAULT:columnNumber];
            columnList = Arrays.asList(parsys);
        } else {
            String[] parsys = new String [COLUMN_NUMBER_DEFAULT];
            columnList = Arrays.asList(parsys);
        }

        return columnList;
    }
}
