package nl.tricode.aem.core.services;

import nl.tricode.aem.core.models.page.AlternatePage;

import java.util.List;

/**
 * Service for checking alternate pages existence
 */
public interface AlternatePagesService {

    /**
     * Finds and returns the alternate pages for a page on a page path
     *
     * @param pagePath
     * @return list of alternate pages
     */
    List<AlternatePage> getAlternatePages(String pagePath);
}
