package nl.tricode.aem.core.models.socialicons;

import nl.tricode.aem.core.services.InheritedPropertiesService;
import nl.tricode.aem.core.utils.granite.MultifieldOptionBuilder;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * Social icons component. Contains configuration for several social medias and their icons.
 */
@Model(adaptables = Resource.class)
public class SocialIcons {

    private static final Logger LOGGER = LoggerFactory.getLogger(SocialIcons.class);

    @Inject
    @Default(booleanValues = false)
    private boolean disableInheritance;

    @Inject
    @Default(values = {})
    private String[] socialIcons;

    @Inject
    private InheritedPropertiesService inheritedPropertiesService;

    @Self
    private Resource resource;

    public boolean isDisableInheritance() {
        return disableInheritance;
    }

    public List<SocialIconsOption> getSocialIcons() {
        if (disableInheritance) {
            return getSocialIconsOptions(socialIcons);
        } else {
            Object socialIcons = inheritedPropertiesService.getInheritedPropertyValue("socialIcons", resource);
            return getSocialIconsOptions(socialIcons);
        }
    }

    private List<SocialIconsOption> getSocialIconsOptions(final Object socialIcons) {
        final MultifieldOptionBuilder<SocialIconsOption> multifieldOptionBuilder = new MultifieldOptionBuilder<>();

        try {
            if (socialIcons != null) {
                if (socialIcons instanceof String) {
                    return multifieldOptionBuilder.buildOption((String)socialIcons, SocialIconsOption.class);
                } else if (socialIcons instanceof String[]) {
                    return multifieldOptionBuilder.buildOptions((String [])socialIcons, SocialIconsOption.class);
                }
            }
        } catch (final IOException e) {
            LOGGER.error("Unable to populate list of social icons {}", socialIcons, e);
        }
        return Collections.EMPTY_LIST;
    }

    public boolean isConfigured() {
        return !getSocialIcons().isEmpty();
    }
}
