package nl.tricode.aem.core.services.recaptcha;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service(value = ACLReCaptchaService.class)
@Component(metatype = true, name = "nl.tricode.aem.core.services.recaptcha.ACLReCaptchaService",
        label = "ACL Google reCAPTCHA service", description = "ACL Google reCAPTCHA service")
public class ACLReCaptchaServiceImpl implements ACLReCaptchaService {
    private static final Logger LOG = LoggerFactory.getLogger(ACLReCaptchaService.class);

    private static final String SESSION_ATTRIBUTE_POSTFIX = ".submitcount";
    public static final String ROOT_PAGE = "rootPage";

    @Reference(referenceInterface = RecaptchaServiceConfig.class,
            cardinality = ReferenceCardinality.MANDATORY_MULTIPLE,
            bind = "bind",
            unbind = "unbind",
            policy = ReferencePolicy.DYNAMIC)
    private final Map<String, RecaptchaServiceConfig> configs = new HashMap<>();

    public String getSecret(final SlingHttpServletRequest slingRequest) {
        final RecaptchaServiceConfig config = getConfigForSite(getSiteName(slingRequest));
        if (config == null) {
            LOG.error("Unable verify captcha, getSecret config="+config);
            return null;
        }
        return config.getSecret();
    }

    public String buildUrl(final String siteName) {
        final RecaptchaServiceConfig config = getConfigForSite(siteName);
        if (config == null) {
            LOG.error("Unable verify captcha, buildUrl config="+config);
            return null;
        }
        return config.getUrl();
    }


    @Override
    public ReCaptchaApiResponse check(final SlingHttpServletRequest slingRequest, final String resourcePath,
                                      final String challenge, final String remoteIp) throws IOException {
        final String url = buildUrl(slingRequest, challenge, remoteIp).toString();
        LOG.error("Url ="+url);
        try {
            final ReCaptchaApiResponse apiResponse = getApiResponse(url);
            if (apiResponse.isSuccess()) {
                resetAttempts(slingRequest, resourcePath);
            }
            return apiResponse;
        } catch (final IOException e) {
            throw new IOException("Could not verify captcha", e);
        } catch (final IllegalArgumentException e) {
            LOG.error("Unable verify captcha at [{}]: {}", url, e.getMessage());
            throw new IllegalArgumentException("Could not verify captcha", e);
        }
    }

    private RecaptchaServiceConfig getConfigForSite(final String siteName) {
        LOG.error("sitename="+siteName);
        return configs.get(siteName);
    }

    private static String getSiteName(final SlingHttpServletRequest request) {
        final Resource resource = request.getResource();
            final String[] pathParts = resource.getPath().split("/");
            if (pathParts.length > 2) {
                return pathParts[2];
            }
        return null;
    }

    private StringBuilder buildUrl(final SlingHttpServletRequest slingRequest, final String challenge,
                                   final String remoteIp) {
        final StringBuilder url = new StringBuilder(buildUrl(getSiteName(slingRequest)));
        url.append("?secret=").append(getSecret(slingRequest));
        url.append("&response=").append(challenge);
        if (remoteIp.isEmpty()) {
            url.append("&remoteip=").append(remoteIp);
        }
        return url;
    }

    private static ReCaptchaApiResponse getApiResponse(final String url) throws IOException {
        final HttpClient client = getHttpClient();
        final GetMethod method = new GetMethod(url);
        final int statusCode = client.executeMethod(method);
        if (statusCode != 200) {
            LOG.error("Could not verify captcha at [{}]: HTTP Status {}", url, statusCode + ": " + method.getStatusText());
            throw new IOException("Could not verify captcha, got http status code " + statusCode + " saying " + method.getStatusText());
        }
        final String response = new String(method.getResponseBody());
        return new ObjectMapper().readValue(response, ReCaptchaApiResponse.class);
    }

    private static HttpClient getHttpClient() {
        final HttpClient client = new HttpClient();
        final HttpClientParams params = new HttpClientParams();
        params.setContentCharset("UTF-8");
        params.setParameter("http.connection.timeout", 5000);
        params.setSoTimeout(5000);
        client.setParams(params);
        return client;
    }

    protected final void bind(final RecaptchaServiceConfig config, final Map properties) {
        final String rootPage = properties.get(ROOT_PAGE).toString();
        configs.put(rootPage, config);
    }

    protected final void unbind(final RecaptchaServiceConfig config) {
        configs.values().remove(config);
    }

    /**
     * Reset attempts
     *
     * Reset the attempts counter if needed.
     * This session attribute is set when the captcha is used after an x amount of attempts
     *
     * @param slingRequest The request object
     * @param resourcePath The resource path
     */
    private static void resetAttempts(final SlingHttpServletRequest slingRequest, final String resourcePath) {
        slingRequest.getSession().removeAttribute(resourcePath + SESSION_ATTRIBUTE_POSTFIX);
    }

}
