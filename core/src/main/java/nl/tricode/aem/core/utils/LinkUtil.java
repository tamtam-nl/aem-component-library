package nl.tricode.aem.core.utils;

import nl.tricode.aem.core.ACLConstants;
import org.apache.sling.api.SlingHttpServletRequest;

/**
 * Utility class for working with / modifying links
 */
public class LinkUtil {
    
    private LinkUtil() {}

    /**
     * Converts the input from a pathbrowser dialog component to a link.
     * The input can be a path in the repository, or a link. In the latter case the link is not modified.
     * 
     * @param request the slign request (needed for externalizing)
     * @param propertyValue the pathbrowser property value
     * @return An externalized link or the original url in case its external
     */
    public static String pathBrowserPropertyToLink(final SlingHttpServletRequest request, final String propertyValue) {
        if (propertyValue == null) {
            return null;
        }
        // external link, or siteroot
        if (!shouldExternalize(propertyValue)) {
            return propertyValue;
        }
        // its a path; map it
        String pagePath = propertyValue;
        // add html extension if needed
        if (!hasFileExtension(pagePath) && !isAnchor(pagePath)) {
            pagePath = pagePath.concat(ACLConstants.EXTENSION_PAGE);
        }
        if (InstanceUtil.isPublishMode()) {
            return request.getResourceResolver().map(request, pagePath);
        }
        return pagePath;
    }

    /**
     * Check if the given url has a file extension.
     * This is assumed to be 3 or 4 characters long.
     * NOTE: this method will not correctly handle Sling suffixes, because its not recommended to use them.
     * 
     * @param url the url
     * @return true or false
     */
    public static boolean hasFileExtension(final String url) {
        return url.matches("^[\\w\\d\\:\\/\\.]+\\.\\w{3,4}(\\?[\\w\\W]*)?$");
    }

    /**
     * Check if this is a link to an anchor
     * @param link the link
     * @return true or false
     */
    public static boolean isAnchor(final String link) {
        return link.startsWith("#");
    }

    /**
     * Creates and returns the external link for a link.
     *
     * @param pagePath
     * @param request
     * @return external link
     */
    public static String getExternalPath(final String pagePath, final SlingHttpServletRequest request) {
        return request.getScheme()+"://"+ request.getServerName()+pagePath;
    }

    /**
     * Converts the input from a pathbrowser dialog component to a absolute link (including protocol and domain).
     * The input can be a path in the repository, or a link. In the latter case the link is not modified.
     *
     * @param request the slign request (needed for externalizing)
     * @param propertyValue the pathbrowser property value
     * @return An externalized link or the original url in case its external
     */
    public static String pathBrowserPropertyToAbsoluteLink(final SlingHttpServletRequest request, final String propertyValue) {
        if (propertyValue == null) {
            return null;
        }
        // external link, or siteroot
        if (!shouldExternalize(propertyValue) && !isAnchor(propertyValue) ) {
            return propertyValue;
        }
        // its a path; map it
        String pagePath = propertyValue;
        // add html extension if needed
        if (!hasFileExtension(pagePath) && !isAnchor(pagePath)) {
            pagePath = pagePath.concat(ACLConstants.EXTENSION_PAGE);
        }
        if (InstanceUtil.isPublishMode()) {
            return getExternalPath(request.getResourceResolver().map(request, pagePath), request);
        }
        return pagePath;
    }
    private static boolean shouldExternalize(final String propertyValue) {
        if (propertyValue.toLowerCase().startsWith("mailto:")
                || "/".equalsIgnoreCase(propertyValue)
                || propertyValue.startsWith("//")
                || propertyValue.contains("://")) {
            return false;
        }
        return true;
    }

}
