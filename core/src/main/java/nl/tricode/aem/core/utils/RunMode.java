package nl.tricode.aem.core.utils;

/**
 * Enum to hold the possible runmodes for the ACL project
 */
public enum RunMode {
    AUTHOR("author"),
    PUBLISH("publish"),
    LOCAL("local"),
    DEVELOPMENT("development"),
    TEST("test"),
    ACCEPTANCE("acceptance"),
    PRODUCTION("production");

    private final String runModeName;

    private RunMode(final String runModeName) {
        this.runModeName = runModeName;
    }

    /**
     * Creates an instance of this enumeration using the given runModeString.
     * When the given run-mode is a NULL reference or does not exists (yet) in this
     * enumeration then a NULL reference will be returned.
     *
     * @param runModeString A String representing the runMode for which the instance
     *                      should be created. 
     * @return An instance of this enumeration or NULL when the given runModeString is not valid.
     */
    public static RunMode createRunModeInstance(final String runModeString) {
        if (runModeString == null) {
            return null;
        }

        for (final RunMode runMode : RunMode.values()) {
            if (runMode.getRunModeAsString().equalsIgnoreCase(runModeString)) {
                return runMode;
            }
        }

        return null;
    }

    /**
     * Get runmode name as string
     * @return the runmode name
     */
    public String getRunModeAsString() {
        return this.runModeName;
    }
}