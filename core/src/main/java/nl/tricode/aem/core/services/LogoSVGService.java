package nl.tricode.aem.core.services;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.dam.api.Asset;
import org.apache.commons.io.IOUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import java.io.IOException;
import java.io.StringWriter;

public class LogoSVGService extends WCMUsePojo {

    private static final Logger LOG = LoggerFactory.getLogger(LogoSVGService.class);
    private String svgMarkup = "";
    private String svgPath = "";

    @Override
    public void activate() throws Exception {
        //Overriding from WCMUsePojo
        LOG.info("Inside Activate");
    }

    public String getSvgMarkup() {

        try {
            Node currentNode = getResource().adaptTo(Node.class);
            if (currentNode != null) {
                svgPath = currentNode.getProperty("fileReference").getString();
                // Make sure image file ends with svg extension
                if (currentNode.hasProperty("inlineSVG") && svgPath.endsWith("svg")) {
                        svgMarkup = prepareSVGMarkup();
                }
            }

        } catch (Exception e) {
            LOG.error("Exception in rendering SVG=" + e);
        }
        LOG.info("svgMarkup=" + svgMarkup);
        return svgMarkup;
    }

    public Boolean checkImageType() {

        Node currentNode = getResource().adaptTo(Node.class);
        String filePath = "";
        Boolean isSVG = false;
        try {
            if (currentNode != null) {
                filePath = currentNode.getProperty("fileReference").getString();
                if(filePath.endsWith("svg")){
                    isSVG = true;
                }
            }
        } catch (Exception e) {
            LOG.error("Exception in checking image type ==" + e);
        }
        LOG.error("isSVG=" + isSVG);
        return isSVG;
    }

    private String prepareSVGMarkup() throws IOException{

        SlingHttpServletRequest request = getRequest();

        ResourceResolver resourceResolver = request.getResourceResolver();
        Resource resource = resourceResolver.getResource(svgPath);
        if (resource != null) {
            Asset svgAsset = resource.adaptTo(Asset.class);
            if (svgAsset != null) {
                StringWriter writer = new StringWriter();
                IOUtils.copy(svgAsset.getOriginal().getStream(), writer);
                svgMarkup = writer.toString();
                svgMarkup.trim();
            }
        }
        return svgMarkup;
    }
}