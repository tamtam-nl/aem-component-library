package nl.tricode.aem.core.models.socialshare;


import nl.tricode.aem.core.utils.PathUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.inject.Inject;

/**
 * Model for the SocialShare component
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class SocialShare {
    @Inject
    @Optional
    @Via("resource")
    private String facebook;

    @Inject
    @Optional
    @Via("resource")
    private String googleplus;

    @Inject
    @Optional
    @Via("resource")
    private String twitter;

    @Self
    private SlingHttpServletRequest request;

    public String getPath() {
        return PathUtil.getUrl(request);
    }

    public boolean isConfigured(){
        return StringUtils.isNotEmpty(facebook) || StringUtils.isNotEmpty(googleplus)|| StringUtils.isNotEmpty(twitter);
    }

    public String getFacebook() {
        return facebook;
    }

    public String getGoogleplus() {
        return googleplus;
    }

    public String getTwitter() {
        return twitter;
    }
}

