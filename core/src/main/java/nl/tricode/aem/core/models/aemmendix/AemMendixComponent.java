package nl.tricode.aem.core.models.aemmendix;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;

import javax.inject.Inject;

/**
 * Sling models AEM Mendix Component.
 */
@Model(adaptables = {SlingHttpServletRequest.class})
public class AemMendixComponent {
    @Inject
    @Optional
    @Via("resource")
    private String app;

    public String getApp() {
        return app;
    }
}
