package nl.tricode.aem.core.services.validation.commands;

import nl.tricode.aem.core.ACLConstants;
import nl.tricode.aem.core.services.validation.commands.textfield.textvalidators.factory.RegexValidationFactory;
import nl.tricode.aem.core.services.validation.commands.utils.ValidationUtils;
import nl.tricode.aem.core.services.validation.models.ValidatorStatus;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.Resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Validator for the text field
 */
public class TextFieldValidator extends FieldValidator {

    private final RegexValidationFactory regexValidationFactory = new RegexValidationFactory();

    @Override
    public List<ValidatorStatus> validate(final String[] values, final Resource resource, final Map<String, RequestParameter[]> requestParameters) {
        List<ValidatorStatus> validatorStatusList = new ArrayList<>();

        Object type = resource.getValueMap().get(ACLConstants.TYPE_PROPERTY);

        if (type != null) {
            validatorStatusList.add(validateRequired(values, resource));

            validatorStatusList.add(ValidationUtils.validateRegex(values, resource, regexValidationFactory.getRegex(resource), regexValidationFactory.getFalseValidatorStatus(resource)));

            if (type.toString().equals(ACLConstants.TEXT_PROPERTY)) {
                validatorStatusList.add(ValidationUtils.validateMaxLength(values, resource));
                validatorStatusList.add(ValidationUtils.validateMinLength(values, resource));
            }
            if (type.toString().equals(ACLConstants.NUMBER_PROPERTY)) {
                validatorStatusList.add(ValidationUtils.validateMaxNumberValue(values, resource));
                validatorStatusList.add(ValidationUtils.validateMinNumberValue(values, resource));
            }
        }
        return validatorStatusList;
    }

}
