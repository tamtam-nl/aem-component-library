package nl.tricode.aem.core;

import nl.tricode.acl.core.services.LicensingService;
import nl.tricode.aem.core.services.search.SearchService;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.osgi.framework.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * ACL activator that checks for valid licence
 */
@Component(immediate = true, label = "ACL activator", description = "ACL activator")
public final class ACLActivator implements BundleActivator, BundleListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(ACLActivator.class);

    LicensingService licensingService;

    @Override
    public final void start(BundleContext context) {
        LOGGER.info("ACL Activator start() method called");
        context.addBundleListener(this);
    }

    @Override
    public final void stop(BundleContext context) throws Exception {
        LOGGER.error("ACL Activator stop() method called");
        context.removeBundleListener(this);
    }

    @Override
    public void bundleChanged(BundleEvent bundleEvent) {
        Bundle bundle = bundleEvent.getBundle();
        BundleContext bundleContext = bundle.getBundleContext();

        ServiceReference reference = bundleContext.getServiceReference(LicensingService.class.getName());
        if(reference != null) {
            licensingService = (LicensingService) bundleContext.getService(reference);
            boolean isLicenseValid = licensingService.isValidLicense();

            if(!isLicenseValid && bundle.getState() == Bundle.ACTIVE) {
                //stopBundle(bundle);
            }
        } else {
            LOGGER.error("Reference to the licensing service is non existing");
            //stopBundle(bundle);
        }
    }

    private void stopBundle(Bundle bundle) {
        try {
            LOGGER.info("License not valid, stopping the bundle..");
            bundle.stop();
        } catch (Exception e) {
            LOGGER.error("Error while stopping the bundle.");
        }
    }
}
