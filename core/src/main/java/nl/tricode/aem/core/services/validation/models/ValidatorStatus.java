package nl.tricode.aem.core.services.validation.models;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.sling.api.resource.Resource;

import java.io.Serializable;

/**
 * Object that is returned when validating the form fields
 */
@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ValidatorStatus implements Serializable{

    private int statusCode;
    private String description;

    @JsonIgnore
    private Resource resource;

    public ValidatorStatus(final int statusCode, final String description, final Resource resource) {
        this.statusCode = statusCode;
        this.description = description;
        this.resource = resource;
    }

    public long getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    @Override
    public String toString() {
        return "ValidatorStatus{" +
                "statusCode=" + statusCode +
                ", description='" + description + '\'' +
                '}';
    }
}
