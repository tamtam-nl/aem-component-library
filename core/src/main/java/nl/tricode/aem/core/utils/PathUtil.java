package nl.tricode.aem.core.utils;



import com.day.cq.commons.Externalizer;
import nl.tricode.aem.core.ACLConstants;
import org.apache.sling.api.SlingHttpServletRequest;

import java.util.Arrays;

/**
 * Utility class for working with / modifying paths
 */
public class PathUtil {

    private static final int ROOT_PATH_DEPTH = 3;
    private static final int START_OF_RELATIVE_LANGUAGE_PATH = 4;

    private PathUtil() {
    }

    /**
     * Finds the root path for a path. eg. root path for /content/something/en/something-else
     * would be /content/something
     *
     * @param currentPagePath
     * @return the root path string
     */
    public static String getRootPath(final String currentPagePath) {
        String[] tokenizedPagePath = currentPagePath.split("/");
        return String.join("/", Arrays.copyOfRange(tokenizedPagePath, 0, ROOT_PATH_DEPTH));
    }

    /**
     * Returns the path without extension.
     *
     * @param pagePath
     * @return string path without extension.
     */
    public static String getPathWithoutPageExtension(final String pagePath) {
        return pagePath.replace(ACLConstants.EXTENSION_PAGE, "");
    }

    /**
     * Returns the relative language path for a page. eg. for /content/something/en/page/sub-page
     * the relative to language path would be /page/sub-page
     *
     * @param rootPath
     * @return
     */
    public static String getRelativeToLanguagePath(String rootPath) {
        String[] tokenizedPagePath = rootPath.split("/");
        if(START_OF_RELATIVE_LANGUAGE_PATH<tokenizedPagePath.length) {
            return "/" + String.join("/", Arrays.copyOfRange(tokenizedPagePath, START_OF_RELATIVE_LANGUAGE_PATH, tokenizedPagePath.length));
        }
        return null;
    }

    /**
     * Returns the full path of the page
     *
     * @param request
     * @return string path without extension.
     */
    public static String getUrl(final SlingHttpServletRequest request) {
        final String nodePath = ResourceUtil.getResourcePage(request.getResource()).getPath();
        final String nodeUrl = nodePath.concat(".html");
        final Externalizer externalizer = request.getResourceResolver().adaptTo(Externalizer.class);
        if (externalizer != null) {
            if (InstanceUtil.isAuthorMode()) {
                return externalizer.authorLink(request.getResourceResolver(), nodeUrl);
            } else {
                return externalizer.publishLink(request.getResourceResolver(), nodeUrl);
            }
        }
        return null;
    }
}
