package nl.tricode.aem.core.models.form;

import nl.tricode.aem.core.ACLConstants;
import nl.tricode.aem.core.services.validation.models.ValidationStatusTypes;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Model for generic form component
 */
@Model(adaptables = {Resource.class})
public class FormFieldModel {

    @Inject
    @Optional
    private String name;

    @Inject
    @Optional
    private String title;

    @Inject
    @Optional
    private String hideTitle;

    @Inject
    @Optional
    @Named(ACLConstants.REQUIRED_PROPERTY)
    private String required;

    @Inject
    @Optional
    private String requiredMessage;

    public String getName() {
        return name;
    }

    public String getHideTitle() {
        return hideTitle;
    }

    public String getTitle() {
        return title;
    }

    public String getRequired() {
        return required;
    }

    public String getRequiredMessage() {
        return requiredMessage != null ? requiredMessage : ValidationStatusTypes.REQUIRED_FIELD_EMPTY.getDescription();
    }
}
