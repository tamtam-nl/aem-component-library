package nl.tricode.aem.core.models.form;

import nl.tricode.aem.core.ACLConstants;
import nl.tricode.aem.core.models.form.selectable.SelectableBuilder;
import nl.tricode.aem.core.models.form.selectable.SelectableItem;
import nl.tricode.aem.core.services.validation.models.ValidationStatusTypes;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Model(adaptables = Resource.class)
public class CheckboxModel extends FormFieldModel {


    @Inject
    @Optional
    @Named(ACLConstants.SELECTABLE_ITEMS_PROPERTY)
    protected String[] items;

    @Inject
    @Optional
    private String alignment;

    @Inject
    @Optional
    private String minLength;

    @Inject
    @Optional
    private String minLengthMsg;

    @Inject
    @Optional
    private String checkMinLength;

    public String getAlignment() {
        return this.alignment;
    }

    public String getCheckMinLength() {
        return this.checkMinLength;
    }

    public String getMinLength() {
        return Boolean.parseBoolean(this.checkMinLength) ? this.minLength : null;
    }

    public String getMinLengthMsg() {
        return Boolean.parseBoolean(this.checkMinLength) ? (StringUtils.isNotEmpty(this.minLengthMsg) ? this.minLengthMsg : String.format(ValidationStatusTypes.MIN_NUMBER_OF_SELECTIONS_MISSED.getDescription(), this.minLength)) : null;
    }

    public List<SelectableItem> getCheckboxItems() {
        return SelectableBuilder.buildSelectableItems(items);
    }
}