package nl.tricode.aem.core.models.breadcrumb;

import com.day.cq.wcm.api.Page;
import nl.tricode.aem.core.utils.LinkUtil;
import nl.tricode.aem.core.utils.PageUtil;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.inject.Inject;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Breadcrumb component. Does not have any configuration.
 */
@Model(adaptables = {SlingHttpServletRequest.class})
public class BreadcrumbComponent {
    
    @Self
    private SlingHttpServletRequest request;

    @Inject
    private Page currentPage;
    
    public List<Item> getItems() {
        // create a list of all pages up to the site root
        final List<Item> items = new LinkedList<>();
        /*TODO this needs to be changed to get siteroot as soon as we have more stable content
          i.e we have homepage that we can relate to to get the site root
          should be:
          final Page homePage = PageUtil.getSiteRoot(currentPage);
         */
        final Page homePage = PageUtil.getTopContentPage(currentPage);
        while (!currentPage.equals(homePage) && currentPage != null) {
            items.add(createItem(currentPage));
            currentPage = currentPage.getParent();
        }
        // reverse the list (so its from homepage to detailpage) and add numbering
        Collections.reverse(items);
        addNumbering(items);
        return items;
    }
    
    private void addNumbering(final List<Item> items) {
        int position = 1;
        for (int i = 0; i < items.size(); i++) {
            items.get(i).setPosition(position);
            position++;
        } 
    }
    
    private Item createItem(final Page sourcePage) {
        final String externalizedlink = LinkUtil.pathBrowserPropertyToLink(request, sourcePage.getPath());
        // create item
        Item item = new Item();
        item.setName(PageUtil.getNavigationTitleWithFallback(sourcePage));
        item.setLink(externalizedlink);
        
        return item;
    }
}
