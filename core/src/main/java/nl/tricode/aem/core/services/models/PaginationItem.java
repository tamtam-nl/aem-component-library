package nl.tricode.aem.core.services.models;

/**
 * Pagination item; container that holds info about a link in the pagination list
 */
public class PaginationItem {
    private String title;
    private String link;
    private boolean active = false;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
