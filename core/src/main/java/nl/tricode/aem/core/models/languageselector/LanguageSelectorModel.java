package nl.tricode.aem.core.models.languageselector;

import com.day.cq.wcm.api.Page;
import nl.tricode.aem.core.models.page.AlternatePage;
import nl.tricode.aem.core.services.AlternatePagesService;
import nl.tricode.aem.core.utils.PageUtil;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

@Model(adaptables = {SlingHttpServletRequest.class})
public class LanguageSelectorModel {

    @Inject
    private AlternatePagesService alternatePagesService;

    @Inject
    private Page currentPage;

    private List<AlternatePage> alternatePagesList;

    @PostConstruct
    protected void init() {
        alternatePagesList = alternatePagesService.getAlternatePages(currentPage.getPath());
    }

    public List<AlternatePage> getAlternatePagesList() {
        return alternatePagesList;
    }

    public String getCurrentPageLanguage() {
        return PageUtil.getSiteLanguageRoot(currentPage).getName();
    }
}
