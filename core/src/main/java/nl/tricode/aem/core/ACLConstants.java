package nl.tricode.aem.core;

/**
 * Constant values for the ACL project
 */
public final class ACLConstants {

    public static final String EXTENSION_PAGE = ".html";

    public static final String RESOURCE_TYPE_FIELDSET = "acl/components/content/forms/fieldset";
    public static final String RESOURCE_TYPE_FORM_TEXT = "acl/components/content/forms/text";
    public static final String RESOURCE_TYPE_FORM_DATEPICKER = "acl/components/content/forms/datepicker";
    public static final String RESOURCE_TYPE_FORM_PASS = "acl/components/content/forms/password";
    public static final String RESOURCE_TYPE_FORM_TEXTAREA = "acl/components/content/forms/textarea";
    public static final String RESOURCE_TYPE_FORM_RADIOBUTTON = "acl/components/content/forms/radiobutton";
    public static final String RESOURCE_TYPE_FORM_CHECKBOX = "acl/components/content/forms/checkbox";
    public static final String RESOURCE_TYPE_FORM_UPLOAD = "acl/components/content/forms/upload";
    public static final String RESOURCE_TYPE_FORM_SELECT = "acl/components/content/forms/select";

    public static final String HTML_EXTENSION = ".html";
    public static final String JCR_CONTENT_URL_PROPERTY = "_jcr_content";
    public static final String MULTIPART_FORM_DATA_PARAMETER = "multipart/form-data";

    public static final int BYTES_IN_KILOBYTE = 1024;
    public static final String UTF8 = "UTF-8";

    //form
    public static final String TARGET = "target";
    public static final String ERROR_PAGE = "errorPage";
    public static final String SUCCESS_PAGE = "successPage";

    //Parameters
    public static final String SELECTABLE_ITEMS_PROPERTY = "items";
    public static final String MAX_DATE_PROPERTY = "maxDate";
    public static final String MIN_DATE_PROPERTY = "minDate";
    public static final String CHECK_MAX_DATE_PROPERTY = "checkMaxDate";
    public static final String CHECK_MIN_DATE_PROPERTY = "checkMinDate";
    public static final String MAX_DATE_ERROR_MSG_PROPERTY = "maxDateErrorMsg";
    public static final String MIN_DATE_ERROR_MSG_PROPERTY = "minDateErrorMsg";
    public static final String DATE_FORMAT_PROPERTY = "dateFormat";
    public static final String REQUIRED_PROPERTY = "required";
    public static final String MAX_FILE_SIZE_PARAMETER = "maxSize";
    public static final String FILE_TYPES_PARAMETER = "fileTypes";
    public static final String MAX_LENGTH_PROPERTY = "maxLength";
    public static final String MIN_LENGTH_PROPERTY = "minLength";
    public static final String TYPE_PROPERTY = "type";
    public static final String TEXT_PROPERTY = "text";
    public static final String NUMBER_PROPERTY = "number";
    public static final String EMAIL_PROPERTY = "email";
    public static final String TEL_PROPERTY = "tel";
    public static final String URL_PROPERTY = "url";
    public static final String ALPHANUMERIC_PROPERTY = "alphanumeric";
    public static final String MIN_VALUE_PROPERTY = "minValue";
    public static final String MAX_VALUE_PROPERTY = "maxValue";
    public static final String CHECK_MAX_VALUE_PROPERTY = "checkMaxValue";
    public static final String CHECK_MIN_VALUE_PROPERTY = "checkMinValue";
    public static final String CHECK_MAX_LENGTH_PROPERTY = "checkMaxLength";
    public static final String CHECK_MIN_LENGTH_PROPERTY = "checkMinLength";
    public static final String MIN_DATE_FORMAT = "yyyy-MM-dd";
    public static final String SHOW_EMPTY_OPTION_PROPERTY = "showEmptyOption" ;

    private ACLConstants() {}
}
