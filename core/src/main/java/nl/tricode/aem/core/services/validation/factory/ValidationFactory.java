package nl.tricode.aem.core.services.validation.factory;

import nl.tricode.aem.core.ACLConstants;
import nl.tricode.aem.core.services.validation.commands.*;
import nl.tricode.aem.core.services.validation.models.ValidationStatusTypes;
import nl.tricode.aem.core.services.validation.models.ValidatorStatus;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.Resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *Factory class that based on the resource type of the submitted input,  decides which validation class will be called.
 */
public class ValidationFactory {

    private final HashMap<String, FieldValidator> validations = new HashMap<>();

    public ValidationFactory() {
        this.validations.put(ACLConstants.RESOURCE_TYPE_FORM_TEXT, new TextFieldValidator());
        this.validations.put(ACLConstants.RESOURCE_TYPE_FORM_DATEPICKER, new DateFieldValidator());
        this.validations.put(ACLConstants.RESOURCE_TYPE_FORM_TEXTAREA, new TextAreaValidator());
        this.validations.put(ACLConstants.RESOURCE_TYPE_FORM_RADIOBUTTON, new RadioButtonValidator());
        this.validations.put(ACLConstants.RESOURCE_TYPE_FORM_CHECKBOX, new CheckBoxValidator());
        this.validations.put(ACLConstants.RESOURCE_TYPE_FORM_UPLOAD, new UploadValidator());
        this.validations.put(ACLConstants.RESOURCE_TYPE_FORM_SELECT, new SelectValidator());
    }

    public List<ValidatorStatus> validate(final String[] values, Resource resource, final Map<String, RequestParameter[]> requestParams) {
        if (resource != null) {
            String resourceType = resource.getResourceType();
            FieldValidator fieldValidator = validations.get(resourceType);
            if (fieldValidator != null)
                return fieldValidator.validate(values, resource, requestParams);
        }

        return getValidatorOkStatus(resource);
    }

    private static List<ValidatorStatus> getValidatorOkStatus(final Resource resource) {
        List<ValidatorStatus> validatorStatuses = new ArrayList<>();
        validatorStatuses.add(new ValidatorStatus(ValidationStatusTypes.VALIDATION_OK.getStatusCode(), ValidationStatusTypes.VALIDATION_OK.getDescription(), resource));
        return validatorStatuses;
    }

}
