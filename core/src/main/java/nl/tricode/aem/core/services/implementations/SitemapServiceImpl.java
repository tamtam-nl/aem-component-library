package nl.tricode.aem.core.services.implementations;

import com.day.cq.wcm.api.Page;
import nl.tricode.aem.core.models.sitemap.SitemapElement;
import nl.tricode.aem.core.services.SitemapService;
import nl.tricode.aem.core.utils.LinkUtil;
import nl.tricode.aem.core.utils.QueryUtil;
import nl.tricode.aem.core.utils.ResourceUtil;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

/**
 * Implementation of {@link nl.tricode.aem.core.services.SitemapService}
 */
@Component(immediate = true, metatype = false, label = "ACL - Sitemap service")
@Service(SitemapService.class)
public class SitemapServiceImpl implements SitemapService {

    @Reference
    private ResourceResolverFactory resourceFactory;

    private static final Logger LOG = LoggerFactory.getLogger(SitemapServiceImpl.class);
    public static final String QUERY_FIND_AVAILABLE_FOR_INDEXING_PAGES = "SELECT * FROM [cq:PageContent] AS s WHERE CONTAINS(s.availableForIndexing, 'on') AND ISDESCENDANTNODE([%s])";

    @Override
    public List<SitemapElement> getListOfSitemapElements(final SlingHttpServletRequest request, final Page currentPage) {
        final String queryString = String.format(QUERY_FIND_AVAILABLE_FOR_INDEXING_PAGES, currentPage.getParent().getPath());
        final ResourceResolver resourceResolver = ResourceUtil.getResourceResolver(resourceFactory);
        final Session session = resourceResolver.adaptTo(Session.class);
        final NodeIterator nodeIterator = QueryUtil.getResultNodeIteratorForQuery(queryString, session);

        final List<SitemapElement> sitemapElements = new LinkedList<>();

        nodeIterator.forEachRemaining(node -> {
            try {
                sitemapElements.add(buildSiteMapElement(request, (Node) node));
            } catch (final RepositoryException e) {
                LOG.error("Cannot read sitemap properties for node: {}", node, e);
            }
        });

        resourceResolver.close();

        return sitemapElements;
    }

    private static SitemapElement buildSiteMapElement(final SlingHttpServletRequest request, final Node node) throws RepositoryException {
        final String priority = node.getProperty("priority").getString();
        final String frequency = node.getProperty("frequency").getString();
        final Calendar lastModified = node.getProperty("cq:lastModified").getDate();
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final String lastModifiedDateString = dateFormat.format(lastModified.getTime());
        final String pagePath = LinkUtil.pathBrowserPropertyToLink(request, node.getParent().getPath());

        String externalLink = LinkUtil.getExternalPath(pagePath, request);

        return new SitemapElement(externalLink, lastModifiedDateString, frequency, priority);
    }
}
