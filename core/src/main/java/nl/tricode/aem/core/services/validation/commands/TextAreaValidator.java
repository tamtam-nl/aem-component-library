package nl.tricode.aem.core.services.validation.commands;

import nl.tricode.aem.core.services.validation.models.ValidatorStatus;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.Resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Validator for Text Area
 */
public class TextAreaValidator  extends FieldValidator {

    @Override
    public List<ValidatorStatus> validate(final String[] values, final Resource resource, final Map<String, RequestParameter[]> requestParameters) {
        List<ValidatorStatus> validatorStatusList = new ArrayList<>();

        validatorStatusList.addAll(validateRequiredMaxAndMinLength(values, resource));

        return validatorStatusList;
    }
}
