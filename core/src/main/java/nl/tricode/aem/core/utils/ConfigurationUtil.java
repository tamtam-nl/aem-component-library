package nl.tricode.aem.core.utils;

import com.day.cq.wcm.webservicesupport.Configuration;
import com.day.cq.wcm.webservicesupport.ConfigurationManager;
import com.day.cq.wcm.webservicesupport.ConfigurationManagerFactory;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Utility class containing methods for AEM service related actions such as retrieving an instance of the SlingSettingsService.
 * This class is not final due to a test case, according to NFa.
 */
public class ConfigurationUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationUtil.class);

    // Private constructor to prevent instantiating
    private ConfigurationUtil() {
    }

    /**
     *
     * @param resourceResolver
     * @param configurationName
     * @return
     */
    public static Iterator<Configuration> getConfigurations(final ResourceResolver resourceResolver, final String configurationName) {
        final ConfigurationManagerFactory configurationManagerFactory = OSGIServiceUtil.getOSGIService(ConfigurationManagerFactory.class);
        final ConfigurationManager configurationManager = configurationManagerFactory.getConfigurationManager(resourceResolver);

        final ArrayList<Resource> resourceList = new ArrayList<>();


        return configurationManager.getConfigurations(configurationName);
    }


}
