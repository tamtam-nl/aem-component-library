package nl.tricode.aem.core.models.form;

import nl.tricode.aem.core.ACLConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Model for Text Area component
 */

@Model(adaptables = Resource.class)
public class TextAreaModel extends FormFieldModel {

    private static final String DEFAULT_LENGTH = "5120";

    @Inject
    @Optional
    private String placeholder;

    @Inject
    @Optional
    @Default(values = DEFAULT_LENGTH)
    @Named(ACLConstants.MAX_LENGTH_PROPERTY)
    private String maxLength;

    public String getPlaceholder() {
        return placeholder;
    }

    public String getMaxLength() {
        return maxLength;
    }

}
