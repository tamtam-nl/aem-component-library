package nl.tricode.aem.core.models.download;

import com.day.cq.dam.api.Asset;
import com.day.cq.wcm.foundation.Download;
import com.day.text.Text;
import nl.tricode.aem.core.ACLConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.xss.XSSFilter;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Sling models download component.
 */
@Model(adaptables = {SlingHttpServletRequest.class})
public class DownloadComponent {

    @Inject
    private XSSFilter xssFilter;

    private Download download;

    @SlingObject
    private Resource resource;

    @Inject @Optional @Via("resource") @Named("jcr:description")
    private String description;

    @Inject @Optional @Via("resource")
    private String fileReference;

    @PostConstruct
    protected void init() {
        download = new Download(resource);
    }

    public String getDescription() {
        return description;
    }

    public String getFileReference() {
        return fileReference;
    }

    public String getHref() {
        if (download.hasContent()) {
            return Text.escape(download.getHref(), '%', true);
        }
        return "";
    }

    public String getTitle() {
        if (download.hasContent()) {
            return download.getTitle(true);
        }
        return "";
    }

    public String getDisplayText() {
        if (download.hasContent()) {
            final String displayText = download.getInnerHtml() == null ? download.getFileName() : download.getInnerHtml().toString();
            return xssFilter.filter(displayText);
        }
        return "";
    }

    public String getIconType() {
        return download.getIconType();
    }

    public long getSize() {
        final ResourceResolver resourceResolver = resource.getResourceResolver();
        final Resource damResource = resourceResolver.getResource(download.getHref());

        if (damResource != null) {
            return damResource.adaptTo(Asset.class).getOriginal().getSize() / ACLConstants.BYTES_IN_KILOBYTE;
        }

        return 0;
    }
}
