package nl.tricode.aem.core.services.validation;

/**
 *
 */
public class ValidationConstants {

    public static final String EMAIL_REGEX = "^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
    public static final String TEL_REGEX = "^([+?0-9 -/(/)])+$";
    public static final String URL_REGEX = "^((https?:)?\\/\\/)?(www\\.)?([-a-zA-Z0-9@%._\\+~#=]{2,256})\\.[a-z]{2,6}(:[0-9]{2,4})?\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)$";
    public static final String NUMBER_REGEX = "^[0-9]+$";

    private ValidationConstants() {
    }
}
