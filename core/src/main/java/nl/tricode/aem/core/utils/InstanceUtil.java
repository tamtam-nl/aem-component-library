package nl.tricode.aem.core.utils;

import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

/**
 * Utility class containing methods for AEM Instance related actions such as retrieving the current run-modes.
 *
 * @author N. Faber
 * @version 1.0
 */
public final class InstanceUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(InstanceUtil.class);

    // Private constructor to prevent instantiating
    private InstanceUtil() {
    }

    /**
     * Determines if the current AEM instance has the 'publish' runmode enabled.
     *
     * @return TRUE when the 'publish' runmode is enabled.
     */
    public static boolean isPublishMode() {
        final Set<RunMode> runModes = getCurrentRunModes();
        if (runModes.contains(RunMode.PUBLISH)) {
            return true;
        }

        return false;
    }

    /**
     * Determines if the current AEM instance has the 'author' runmode enabled.
     *
     * @return TRUE when the 'author' runmode is enabled.
     */
    public static boolean isAuthorMode() {
        final Set<RunMode> runModes = getCurrentRunModes();
        if (runModes.contains(RunMode.AUTHOR)) {
            return true;
        }

        return false;
    }

    /**
     * Determines if the current AEM instance has the 'local' runmode enabled.
     *
     * @return TRUE when the 'local' runmode is enabled.
     */
    public static boolean isLocalMode() {
        final Set<RunMode> runModes = getCurrentRunModes();
        if (runModes.contains(RunMode.LOCAL)) {
            return true;
        }

        return false;
    }

    /**
     * Retrieves all the run-modes currently active on the AEM Instance. An empty
     * <code>HashSet</code> is returned if no run-modes could be found or determined.
     *
     * @return All the run-modes that are currently active.
     */
    public static Set<RunMode> getCurrentRunModes() {
        final SlingSettingsService slingSettingsService = OSGIServiceUtil.getOSGIService(SlingSettingsService.class);
        if (slingSettingsService == null) {
            LOGGER.warn("Unable to determine the run-modes. Cause: unable to get an instance of the SlingSettingsService.");
            // Return empty HashSet
            return new HashSet<>();
        }

        final Set<String> runModes = slingSettingsService.getRunModes();
        if (runModes == null) {
            return new HashSet<>();
        }

        final Set<RunMode> result = new HashSet<>();
        for (final String runMode : runModes) {
            final RunMode runModeInstance = RunMode.createRunModeInstance(runMode);
            if (runModeInstance != null) {
                result.add(runModeInstance);
            }
        }

        return result;
    }
}

