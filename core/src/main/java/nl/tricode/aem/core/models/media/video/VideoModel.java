package nl.tricode.aem.core.models.media.video;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Model for the Video component
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class VideoModel {

    public static final String DEFAULT_WIDTH = "420";
    public static final String DEFAULT_HEIGHT = "315";

    /**
     * Video URL set in the dialog
     */
    @Inject
    @Optional
    @Via("resource")
    private String videoUrl;

    /**
     * YouTube video ID set in the dialog
     */
    @Inject
    @Optional
    @Via("resource")
    private String youTubeVideoId;

    @Inject
    @Optional
    @Via("resource")
    private String asset;

    @Inject
    @Optional
    @Via("resource")
    private String autoPlay;

    @Inject
    @Optional
    @Via("resource")
    private String mute;

    @Inject
    @Optional
    @Via("resource")
    private String width;

    @Inject
    @Optional
    @Via("resource")
    private String height;

    @Inject
    @Optional
    @Via("resource")
    private String videoSource;

    @Inject
    @Optional
    @Via("resource")
    private String videoOpacity;

    public String getVideoUrl() {
        return videoUrl;
    }

    public String getYouTubeVideoId() {
        return youTubeVideoId;
    }

    public String getAsset() {
        return asset;
    }

    public String getAutoPlay() {
        return autoPlay;
    }

    public String getMute() {
        return mute;
    }

    public String getVideoOpacity() {
        return videoOpacity;
    }

    public String getWidth() {
        if ("0".equals(width)) {
            return DEFAULT_WIDTH;
        }
        return width;
    }

    public String getHeight() {
        if ("0".equals(height)) {
            return DEFAULT_HEIGHT;
        }
        return height;
    }

    public String getVideoSource() {
        return videoSource;
    }
}
