package nl.tricode.aem.core.models.form;

import nl.tricode.aem.core.ACLConstants;
import nl.tricode.aem.core.services.validation.models.ValidationStatusTypes;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Model for Upload component
 */
@Model(adaptables = Resource.class)
public class UploadModel extends FormFieldModel {

    private static final String DEFAULT_SIZE = "2048";

    @Inject
    @Optional
    private String multiple;

    @Inject
    @Optional
    @Named(ACLConstants.FILE_TYPES_PARAMETER)
    private String[] fileTypes;

    @Inject
    @Optional
    @Default(values = DEFAULT_SIZE)
    @Named(ACLConstants.MAX_FILE_SIZE_PARAMETER)
    private String maxSize;

    @Inject
    @Optional
    private String maxSizeMessage;

    public String getMultiple() {
        return multiple;
    }

    public String getMaxSizeMessage() {
        return ValidationStatusTypes.FILE_SIZE_BIGGER_THAN_MAX_SIZE.getDescription();
    }

    /**
     * @return maximum allowed size of the uploaded file in Kilobytes.
     */
    public String getMaxSize() {
        return StringUtils.isNotEmpty(maxSize) ? maxSize : DEFAULT_SIZE;
    }

    public String[] getFileTypes() {
        return fileTypes;
    }
}
