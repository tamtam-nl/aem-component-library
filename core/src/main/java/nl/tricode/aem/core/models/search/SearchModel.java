package nl.tricode.aem.core.models.search;

import com.day.cq.wcm.api.Page;
import nl.tricode.aem.core.services.InheritedPropertiesService;
import nl.tricode.aem.core.utils.PageUtil;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Model(adaptables = SlingHttpServletRequest.class)
public class SearchModel {

    private static final String SEARCH_IN = "searchIn";
    private static final String SEARCH_RESULTS_PAGE = "searchResultsPagePath";

    @Self
    SlingHttpServletRequest request;

    @Inject
    private InheritedPropertiesService inheritedPropertiesService;

    private Resource resource;

    @Inject
    private Page currentPage;

    @Inject
    @Optional
    @Via("resource")
    private String searchIn;

    @Inject
    @Optional
    @Via("resource")
    private String searchResultsPagePath;

    @Inject
    @Optional
    @Via("resource")
    private String disableInheritance;

    @PostConstruct
    public void init() throws Exception {
        if (disableInheritance == null) {
            resource = request.getResource();
            searchIn = inheritedPropertiesService.getInheritedPropertyValue(SEARCH_IN, resource);
            searchResultsPagePath = inheritedPropertiesService.getInheritedPropertyValue(SEARCH_RESULTS_PAGE, resource);

        }

        //check if page is on first or second level do not search for language page if it is
        //todo this is short term fix should be fixed in the future when we have templates for different page level
        if(searchIn == null) {
            if (currentPage.getParent() != null && currentPage.getParent().getParent() != null) {
                searchIn = PageUtil.getSiteLanguageRoot(currentPage).getPath();
            } else {
                searchIn = currentPage.getPath();
            }

        }

        request.getSession().setAttribute(SEARCH_IN, searchIn);
    }

    public String getSearchIn() {
        return searchIn;
    }

    public String getSearchResultsPagePath() {
        return searchResultsPagePath;
    }

}

