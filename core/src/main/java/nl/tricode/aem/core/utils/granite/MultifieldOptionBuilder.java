package nl.tricode.aem.core.utils.granite;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Multifield Option Builder class that can be used for generic classes to deserialize the multifield json objects.
 *
 * @param <T>
 */
public class MultifieldOptionBuilder<T> {

    /**
     * Deserializes the items in a list of options from a class clazz.
     *
     * @param items
     * @param clazz
     * @return list of deserialized items
     */
    public List<T> buildOptions(final String[] items, final Class<T> clazz) throws IOException {

        final List<T> optionList = new LinkedList<>();
        final ObjectMapper mapper = new ObjectMapper();

        for (final String item : items) {
            optionList.add(mapper.readValue(item, clazz));
        }

        return optionList;
    }

    /**
     * Deserializes single item from a class clazz.
     *
     * @param item
     * @param clazz
     * @return list of deserialized items
     */
    public List<T> buildOption(final String item, final Class<T> clazz) throws IOException {

        final List<T> optionList = new LinkedList<>();
        final ObjectMapper mapper = new ObjectMapper();

        optionList.add(mapper.readValue(item, clazz));

        return optionList;
    }
}
