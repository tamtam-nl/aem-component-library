package nl.tricode.aem.core.services.models;

import java.util.LinkedList;
import java.util.List;

/**
 * The summary of a result (statistics, the results and pagination info)
 * @param <T> the type of the results
 */
public class ResultSummary <T> {
    private List<T> hits = new LinkedList<>();
    private List<PaginationItem> paginationItems = new LinkedList<>();
    private long hitsPerPage = 0;
    private long startPage = 0;
    private long resultCount = 0;
    private long numberOfPages = 0;

    public long getResultCount() {
        return resultCount;
    }

    public void setResultCount(final long resultCount) {
        this.resultCount = resultCount;
    }

    public long getStartPage() {
        return startPage;
    }

    public void setStartPage(final long startPage) {
        this.startPage = startPage;
    }

    public long getHitsPerPage() {
        return hitsPerPage;
    }

    public void setHitsPerPage(final long hitsPerPage) {
        this.hitsPerPage = hitsPerPage;
    }

    public long getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(final long numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public List<PaginationItem> getPaginationItems() {
        return paginationItems;
    }

    public void setPaginationItems(List<PaginationItem> paginationItems) {
        this.paginationItems = paginationItems;
    }

    public List<T> getHits() {
        return hits;
    }

    public void setHits(final List<T> hits) {
        this.hits = hits;
    }
}
