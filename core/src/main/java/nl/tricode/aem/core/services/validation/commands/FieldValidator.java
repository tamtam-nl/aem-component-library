package nl.tricode.aem.core.services.validation.commands;


import nl.tricode.aem.core.ACLConstants;
import nl.tricode.aem.core.services.validation.commands.utils.ValidationUtils;
import nl.tricode.aem.core.services.validation.models.ValidationStatusTypes;
import nl.tricode.aem.core.services.validation.models.ValidatorStatus;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.Resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * General validator for all form fields
 */
public abstract class FieldValidator {

    public abstract List<ValidatorStatus> validate(final String[] values, final Resource resource, final Map<String, RequestParameter[]> requestParameters);

    public ValidatorStatus validateRequired(final String[] values, final Resource resource) {
        Object requiredProperty = resource.getValueMap().get(ACLConstants.REQUIRED_PROPERTY);
        if (requiredProperty != null && StringUtils.isNotEmpty(requiredProperty.toString()) && isEmptyValues(values)) {
            return new ValidatorStatus(ValidationStatusTypes.REQUIRED_FIELD_EMPTY.getStatusCode(), ValidationStatusTypes.REQUIRED_FIELD_EMPTY.getDescription(), resource);
        }
        return new ValidatorStatus(ValidationStatusTypes.VALIDATION_OK.getStatusCode(), ValidationStatusTypes.VALIDATION_OK.getDescription(), resource);
    }


    List<ValidatorStatus> validateRequiredMaxAndMinLength(final String[] values, final Resource resource) {
        List<ValidatorStatus> validatorStatusList = new ArrayList<>();

        validatorStatusList.add(validateRequired(values, resource));
        validatorStatusList.add(ValidationUtils.validateMaxLength(values, resource));
        validatorStatusList.add(ValidationUtils.validateMinLength(values, resource));

        return validatorStatusList;
    }

    /**
     * Returns true if some of the values are empty. Returns false if all the values are filled.
     *
     * @param values list of values that will be checked.
     * @return
     */
    private static boolean isEmptyValues(String[] values) {
        for (String value : ArrayUtils.nullToEmpty(values)) {
            if (value.isEmpty()) {
                return true;
            }
        }
        return false;
    }
}
