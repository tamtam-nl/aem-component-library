package nl.tricode.aem.core.models.allianz.blockexample;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.inject.Inject;


@Model(adaptables = Resource.class)
public class BlockExampleComponent {

    @Inject
    @Optional
    private String title;

    @Inject
    @Optional
    private String turnover;

    @Inject
    @Optional
    private String insuredamount;

    @Inject
    @Optional
    private String premium;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTurnover() {
        return turnover;
    }

    public void setTurnover(String turnover) {
        this.turnover = turnover;
    }

    public String getInsuredamount() {
        return insuredamount;
    }

    public void setInsuredamount(String insuredamount) {
        this.insuredamount = insuredamount;
    }

    public String getPremium() {
        return premium;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }
}
