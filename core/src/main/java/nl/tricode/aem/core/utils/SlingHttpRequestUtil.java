package nl.tricode.aem.core.utils;

import nl.tricode.aem.core.ACLConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestParameter;

import java.util.Map;

/**
 * Util class for slingHttpServletRequest helper methods
 */
public class SlingHttpRequestUtil {

    /**
     * Private constructor. Prevent from creating objects from utility class.
     */
    private SlingHttpRequestUtil() {
    }

    public static Map<String, RequestParameter[]> getRequestParametersIfMultipart(final SlingHttpServletRequest request) {
        if (isMultipartRequest(request)) {
            return request.getRequestParameterMap();
        }
        return null;
    }

    public static boolean isMultipartRequest(final SlingHttpServletRequest request) {
        return request.getContentType() != null &&
                request.getContentType().toLowerCase().contains(ACLConstants.MULTIPART_FORM_DATA_PARAMETER);
    }
}
