package nl.tricode.aem.core.models.form.datepicker;

import nl.tricode.aem.core.ACLConstants;
import nl.tricode.aem.core.models.form.FormFieldModel;
import nl.tricode.aem.core.services.validation.models.ValidationStatusTypes;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.inject.Inject;
import javax.inject.Named;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Sling model datepicker component.
 */
@Model(adaptables = {Resource.class})
public class DatepickerModel extends FormFieldModel {

    @Inject @Optional @Default(values = "dd-MM-yyyy") @Named(ACLConstants.DATE_FORMAT_PROPERTY)
    private String dateFormat;

    @Inject @Optional @Named(ACLConstants.MIN_DATE_PROPERTY)
    private Date minDate;

    @Inject @Optional @Named(ACLConstants.MAX_DATE_PROPERTY)
    private Date maxDate;

    @Inject @Optional @Named(ACLConstants.CHECK_MAX_DATE_PROPERTY)
    private String checkMaxDate;

    @Inject @Optional @Named(ACLConstants.CHECK_MIN_DATE_PROPERTY)
    private String checkMinDate;

    @Inject @Optional @Named(ACLConstants.MAX_DATE_ERROR_MSG_PROPERTY)
    private String maxDateErrorMsg;

    @Inject @Optional @Named(ACLConstants.MIN_DATE_ERROR_MSG_PROPERTY)
    private String minDateErrorMsg;

    public String getDateFormat() {
        return dateFormat;
    }

    public String getMinDate() {
        SimpleDateFormat format1 = new SimpleDateFormat(ACLConstants.MIN_DATE_FORMAT);
        return format1.format(minDate.getTime());
    }

    public String getMaxDate() {
        SimpleDateFormat format1 = new SimpleDateFormat(ACLConstants.MIN_DATE_FORMAT);
        return format1.format(maxDate.getTime());
    }

    public String getCheckMaxDate() {
        return checkMaxDate;
    }

    public String getCheckMinDate() {
        return checkMinDate;
    }

    public String getMaxDateErrorMsg() {
        return maxDateErrorMsg !=null ? maxDateErrorMsg : ValidationStatusTypes.DATE_LATER_THAN_MAX_DATE.getDescription();
    }

    public String getMinDateErrorMsg() {
        return minDateErrorMsg !=null ? minDateErrorMsg : ValidationStatusTypes.DATE_BEFORE_MIN_DATE.getDescription();
    }
}
