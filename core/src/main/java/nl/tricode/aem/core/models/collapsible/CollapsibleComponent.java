package nl.tricode.aem.core.models.collapsible;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;

/**
 * Collapsible component model with single property.
 */
@Model(adaptables = Resource.class)
public class CollapsibleComponent {

    @Inject
    @Default(values = "<Please configure link text>")
    private String linkText;

    @Inject
    @Default(values = "none")
    private String defaultState;


    @Inject
    @Default(values = "pull-left")
    private String iconPosition;



    public String getLinkText() {
        return linkText;
    }

    public String getDefaultState() {
        return defaultState;
    }

    public String getIconPosition() {
        return iconPosition;
    }
}