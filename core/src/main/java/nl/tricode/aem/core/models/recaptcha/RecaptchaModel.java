package nl.tricode.aem.core.models.recaptcha;



import nl.tricode.aem.core.services.recaptcha.RecaptchaServiceConfig;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Sling model for recaptcha component.
 */
@Model(adaptables = Resource.class)
public class RecaptchaModel {
    @Inject
    private RecaptchaServiceConfig recaptchaServiceConfig;

    @Inject @Optional
    private String siteKey;


    @PostConstruct
    protected void init() {
        this.siteKey = recaptchaServiceConfig.getSiteKey();
    }

    public String getSiteKey() {
        return siteKey;
    }

}