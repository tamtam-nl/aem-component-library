package nl.tricode.aem.core.models.button;

import nl.tricode.aem.core.utils.LinkUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;

import javax.inject.Inject;

/**
 * Sling models button component.
 */
@Model(adaptables = {SlingHttpServletRequest.class})
public class ButtonModel {
    @Inject
    @Optional
    @Via("resource")
    private String fontWeight;

    @Inject
    @Optional
    @Via("resource")
    private String text;

    @Inject
    @Optional
    @Via("resource")
    private String title;

    @Inject
    @Optional
    @Via("resource")
    private String link;

    @Inject
    @Optional
    @Via("resource")
    private String align;

    @Inject
    @Optional
    @Via("resource")
    private String size;

    @Inject
    @Optional
    @Via("resource")
    private String buttonStyle;

    @Inject
    @Optional
    @Via("resource")
    private String buttonColor;

    @Inject
    @Optional
    @Via("resource")
    private String fileReference;

    @Inject
    private SlingHttpServletRequest request;

    /**
     * Get the button text
     *
     * @return text
     */
    public String getText() {
        if (StringUtils.isEmpty(text)) {
            return "Please configure a text";
        }
        return text;
    }

    /**
     * Get the button title if present, else return text
     *
     * @return title
     */
    public String getTitle() {
        if (title != null && title.length() > 0) {
            return title;
        } else {
            return getText();
        }
    }

    /**
     * Get the page or url that the button links to
     *
     * @return link
     */
    public String getLink() {
        return LinkUtil.pathBrowserPropertyToLink(request, link);
    }

    /**
     * Get the button size
     *
     * @return size
     */
    public String getSize() {
        return size;
    }

    public String getFontWeight() {
        return fontWeight;
    }

    public String getAlign() {
        return align;
    }

    public String getButtonStyle() {
        return buttonStyle;
    }

    public String getButtonColor() {
        return buttonColor;
    }

    public String getFileReference() {
        return fileReference;
    }

    public boolean hasBgImage() {return StringUtils.isNotEmpty(fileReference);}
}