package nl.tricode.aem.core.services;

import com.day.cq.wcm.api.Page;
import org.apache.sling.api.resource.Resource;

/**
 * Service for checking how many h1 tags exists on a page
 */
@FunctionalInterface
public interface TitleValidatorService {
    /**
     * Checks if there are more than one h1 tags on the page
     *
     * @param currentPage
     * @return Boolean based on the existence of one h1 on the page
     */
    int isValid(Page currentPage, Resource currentResource);
}