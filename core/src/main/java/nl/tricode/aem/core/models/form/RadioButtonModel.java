package nl.tricode.aem.core.models.form;

import nl.tricode.aem.core.ACLConstants;
import nl.tricode.aem.core.models.form.selectable.SelectableBuilder;
import nl.tricode.aem.core.models.form.selectable.SelectableItem;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Model(adaptables = Resource.class)
public class RadioButtonModel extends FormFieldModel {

    @Inject
    @Optional
    @Named(ACLConstants.SELECTABLE_ITEMS_PROPERTY)
    protected String[] items;

    public List<SelectableItem> getRadioButtonItems() {

        return SelectableBuilder.buildSelectableItems(items);
    }
}
