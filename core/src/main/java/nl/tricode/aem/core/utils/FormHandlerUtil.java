package nl.tricode.aem.core.utils;

import nl.tricode.aem.core.ACLConstants;
import nl.tricode.aem.core.services.validation.factory.ValidationFactory;
import nl.tricode.aem.core.services.validation.models.ValidatorStatus;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FormHandlerUtil {

    private static final Logger LOG = LoggerFactory.getLogger(FormHandlerUtil.class);
    private static final String QUERY_TEMPLATE = "SELECT * FROM [nt:base] AS s WHERE s.[sling:resourceType] LIKE 'wcm/foundation/components/parsys'  AND ISDESCENDANTNODE(s, '%s')";
    private static final ValidationFactory fieldValidator = new ValidationFactory();

    /**
     * Send GET request
     *
     * @return responseCode from the opened connection
     */
    public static int sendGet(String target, Map<String, Object> parameters) throws IOException {
        URL url = new URL(target + "?" + urlEncode(parameters));
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        // optional default is GET
        connection.setRequestMethod("GET");
        connection.getInputStream();
        return connection.getResponseCode();
    }

    /**
     * Send POST request
     *
     * @return responseCode from the opened connection
     */
    public static int sendPost(String target, Map<String, Object> parameters) throws IOException {
        URL url = new URL(target);
        byte[] postDataBytes = urlEncode(parameters).getBytes(ACLConstants.UTF8);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        connection.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        connection.setDoOutput(true);
        connection.getOutputStream().write(postDataBytes);
        connection.getInputStream();
        return connection.getResponseCode();

    }

    private static String encodeUTF8(String s) {
        try {
            return URLEncoder.encode(s, ACLConstants.UTF8);
        } catch (UnsupportedEncodingException e) {
            LOG.debug("Could not decode string " + s);
            LOG.error(e.getMessage(), e);
        }
        return StringUtils.EMPTY;
    }

    private static String urlEncode(Map map) {
        StringBuilder sb = new StringBuilder();
        Set<Map.Entry> entrySet = map.entrySet();

        for (Map.Entry entry : entrySet) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(String.format("%s=%s",
                    encodeUTF8(entry.getKey().toString()),
                    encodeUTF8(((String[]) entry.getValue())[0])
            ));
        }
        return sb.toString();
    }

    public static List<ValidatorStatus> validate(final SlingHttpServletRequest request) {

        Resource resource = request.getResource();
        String queryString = String.format(QUERY_TEMPLATE, resource.getPath());
        final NodeIterator nodeIterator = QueryUtil.getResultNodeIteratorForQuery(queryString, resource.getResourceResolver().adaptTo(Session.class));

        return getValidatorStatuses(nodeIterator, request);
    }

    private static List<ValidatorStatus> getValidatorStatuses(final NodeIterator nodeIterator, final SlingHttpServletRequest request) {
        ResourceResolver resourceResolver = request.getResourceResolver();
        final List<ValidatorStatus> allValidatorStatuses = new ArrayList<>();

        nodeIterator.forEachRemaining(node -> {
            try {
                ((Node) node).getNodes().forEachRemaining(formNode -> {
                    Node formFieldNode = (Node) formNode;
                    Resource formFieldResource = getFormFieldResource(resourceResolver, formFieldNode);
                    if (formFieldResource != null && !formFieldResource.isResourceType(ACLConstants.RESOURCE_TYPE_FIELDSET)) {
                        ValueMap formFieldProperties = formFieldResource.getValueMap();
                        Object nameProperty = formFieldProperties.get("name");
                        if (nameProperty != null) {
                            final List<ValidatorStatus> validatorStatuses = fieldValidator.validate(request.getParameterValues(nameProperty.toString()), formFieldResource, SlingHttpRequestUtil.getRequestParametersIfMultipart(request));
                            allValidatorStatuses.addAll(validatorStatuses);
                        }
                    }
                });
            } catch (RepositoryException e) {
                LOG.error("Error in getting the resource", e);
            }
        });
        return allValidatorStatuses;
    }

    private static Resource getFormFieldResource(final ResourceResolver resourceResolver, final Node formFieldNode) {
        try {
            return resourceResolver.getResource(formFieldNode.getPath());
        } catch (RepositoryException e) {
            LOG.error("Error in getting the resource", e);
        }
        return null;
    }

}
