package nl.tricode.aem.core.services;

import org.apache.sling.api.resource.Resource;

/**
 * Service for dealing with inherited properties.
 */
@FunctionalInterface
public interface InheritedPropertiesService {

    /**
     * Returns the inherited property value.
     *
     * @param propertyName The property name that we look for
     * @param resource The resource that we are looking the property value for
     * @param <T> The resource type we expect to get back
     * @return The string value for the resource property or empty if not found
     */
    <T> T getInheritedPropertyValue(final String propertyName, final Resource resource);
}
