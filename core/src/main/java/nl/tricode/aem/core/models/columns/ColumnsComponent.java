package nl.tricode.aem.core.models.columns;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Model for the columns component
 */
@Model(adaptables = Resource.class)
public class ColumnsComponent {

    public static final String DEFAULT_LAYOUT = "12";
    public static final int DEFAULT_NO_COLUMNS = 1;

    /**
     * Column layout set in the dialog
     */
    @Inject
    @Optional
    private String layouts;

    @Inject @Default(values = "no-border")
    private String borderStyle;

    @Inject
    @Default (values = "none")
    private String bgColor;

    public List<String> getLayouts() {
        List<String> columnList = new ArrayList<String>();
        if (layouts != null) {
            layouts = layouts.trim();
            String[] parts = layouts.split("-");
            columnList = Arrays.asList(parts);
        } else {
            columnList.add(DEFAULT_LAYOUT);
        }

        return columnList;
    }

    public int getNoColumns(){
        if (layouts != null) {
            layouts = layouts.trim();
            String[] parts = layouts.split("-");
            return parts.length;
        }else {
            return DEFAULT_NO_COLUMNS;
        }
    }

    public String getBorderStyle() {
        return borderStyle;
    }
    public String getBgColor() {
        return bgColor;
    }

}
