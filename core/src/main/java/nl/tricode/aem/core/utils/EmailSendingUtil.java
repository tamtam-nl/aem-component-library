package nl.tricode.aem.core.utils;

import nl.tricode.aem.core.services.ACLDefaultMailService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.mail.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * Util class for sending email
 */
public class EmailSendingUtil {

    private static final Logger LOG = LoggerFactory.getLogger(EmailSendingUtil.class);

    private EmailSendingUtil() {
    }

    public static void sendEmailWithSubmittedParameters(final SlingHttpServletRequest request, final ACLDefaultMailService mailService) {
        Email email = getFormSubmittedEmail(request, mailService);
        if (email != null) {
            mailService.send(email);
        }
    }

    public static Email getFormSubmittedEmail(final SlingHttpServletRequest request, final ACLDefaultMailService mailService) {
        Resource formResource = request.getResource();
        String emailAddressTo = null;
        String emailSubject = null;
        final ValueMap contactFormValueMap = formResource.adaptTo(ValueMap.class);
        if (contactFormValueMap != null) {
            emailAddressTo = (String) contactFormValueMap.get("emailAddressTo");
            emailSubject = (String) contactFormValueMap.get("emailSubject");
        }
        String fromAddress = mailService.getFromAddress();
        LOG.debug("Parameter values: , emailAddressTo={}, emailSubject={}, fromAddress={}, ", new Object[]{emailAddressTo, emailSubject, fromAddress});
        if (StringUtils.isEmpty(emailAddressTo) || StringUtils.isEmpty(fromAddress)) {
            return null;
        }

        String messageContent = buildMessageBody(request);
        LOG.debug("messageContent: " + messageContent);

        MultiPartEmail email = new MultiPartEmail();
        try {
            email.setFrom(fromAddress);
            email.setSubject(emailSubject);
            email.setMsg(messageContent);
            email.addReplyTo(emailAddressTo);
            email.addTo(emailAddressTo);
            attachDocuments(request, email);
        } catch (EmailException ex) {
            LOG.error("Error while sending email!", ex);
        }
        return email;

    }

    private static String buildMessageBody(final SlingHttpServletRequest request) {
        List<RequestParameter> parameterList = request.getRequestParameterList();
        final StringBuilder messageBody = new StringBuilder("Submitted parameters are the following: ").append(System.lineSeparator()).append(System.lineSeparator());

        parameterList.forEach(parameter -> {
            if (parameter.getContentType() == null) {
                messageBody.append(parameter.getName()).append(" = ");
                messageBody.append(parameter.getString()).append(System.lineSeparator());
            }
        });
        return messageBody.toString();
    }

    private static void attachDocuments(final SlingHttpServletRequest request, final MultiPartEmail email) {
        Map<String, RequestParameter[]> requestParameters = SlingHttpRequestUtil.getRequestParametersIfMultipart(request);
        if (requestParameters != null) {
            for (final Map.Entry<String, RequestParameter[]> pairs : requestParameters.entrySet()) {
                final RequestParameter[] pArr = pairs.getValue();
                for (RequestParameter requestParameter : (RequestParameter[]) ArrayUtils.nullToEmpty(pArr)) {
                    attachOneDocument(requestParameter, email);
                }
            }
        }
    }

    private static void attachOneDocument(final RequestParameter requestParameter, final MultiPartEmail email) {
        String contentType;
        if ((contentType = requestParameter.getContentType()) != null) {
            try {
                final InputStream stream = requestParameter.getInputStream();
                if (stream != null) {
                    byte[] b = new byte[(int) requestParameter.getSize()];
                    stream.read(b, 0, b.length);
                    ByteArrayDataSource byteArrayDataSource = new ByteArrayDataSource(b, contentType);
                    email.attach(byteArrayDataSource, requestParameter.getFileName(), requestParameter.getFileName(), EmailAttachment.ATTACHMENT);
                }
            } catch (IOException e) {
                LOG.error("Exception in input stream!", e);
            } catch (EmailException ex) {
                LOG.error("Error while sending email!", ex);
            }
        }
    }
}
