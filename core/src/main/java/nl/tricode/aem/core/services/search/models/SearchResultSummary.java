package nl.tricode.aem.core.services.search.models;


import nl.tricode.aem.core.services.models.ResultSummary;

/**
 * The summary of a search (statistics, the results and pagination info)
 */
public class SearchResultSummary extends ResultSummary<ResultPage> {
    private String keywords = null;

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

}
