package nl.tricode.aem.core.models.page;

import com.day.cq.commons.Language;

/**
 * Alternate page for specific language
 */
public class AlternatePage {
    private Language language;
    private String languageLocale;
    private String path;
    private boolean isCurrentPage;

    public AlternatePage(Language language, String path) {
        this.language = language;
        this.path = path;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getLanguageLocale() {
        return this.language.getLocale().toString();
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isCurrentPage() {
        return isCurrentPage;
    }

    public void setCurrentPage(boolean isCurrentPage) {
        this.isCurrentPage = isCurrentPage;
    }
}
