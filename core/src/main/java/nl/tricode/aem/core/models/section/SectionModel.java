package nl.tricode.aem.core.models.section;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;

import javax.inject.Inject;

/**
 * Model for Section component.
 */

@Model(adaptables = Resource.class)
public class SectionModel {
    @Inject
    @Default (values = "inherit")
    private String textColor;

    @Inject
    @Default (values = "inherit")
    private String textAlign;

    @Inject
    @Default (values = "none")
    private String bgColor;

    @Inject
    @Default (values = "center")
    private String bgAlign;

    @Inject
    @Default (values = "normal")
    private String bgSize;

    @Inject
    @Default (values = "100")
    private String overlayOpacity;

    @Inject
    @Default (values = "container")
    private String layout;

    @Inject
    @Default (values = "small-gutter")
    private String gutter;

    @Inject
    @Default (values = "no-overlap")
    private String overlap;

    @Inject
    @Optional
    private String fileReference;

//    @Inject
//    @Default (values = "sample-text")
//    private String text;


    public String getTextColor() { return textColor; }

    public String getTextAlign() { return textAlign; }

    public String getBgColor() {
        return bgColor;
    }

    public String getFileReference() {
        return fileReference;
    }

    public String getLayout() { return layout; }

    public String getGutter() { return gutter; }

    public String getOverlap() {
        return overlap;
    }

    public String getBgAlign() { return bgAlign; }

    public String getBgSize() { return bgSize; }

    public String getOverlayOpacity() { return overlayOpacity; }

    //    public String getText() { return text; }
}
