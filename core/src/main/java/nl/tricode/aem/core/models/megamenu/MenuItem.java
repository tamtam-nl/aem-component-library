package nl.tricode.aem.core.models.megamenu;

import java.util.List;

/**
 * Model for the Menu Item. Each item has navigation title and list of sub-pages
 */
public class MenuItem {
    private String title;
    private String path;
    private boolean active;

    private List<MenuItem> subPages;

    public MenuItem(final String title, final String path) {
        this.title = title;
        this.path = path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(final String path) {
        this.path = path;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(final boolean active) {
        this.active = active;
    }

    public List<MenuItem> getSubPages() {
        return subPages;
    }

    public void setSubPages(final List<MenuItem> subPages) {
        this.subPages = subPages;
    }
}
