package nl.tricode.aem.core.models.image;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.Asset;
import com.day.cq.wcm.api.AuthoringUIMode;
import com.day.cq.wcm.resource.details.AssetDetails;
import com.day.jcr.vault.util.MimeTypes;
import nl.tricode.aem.core.utils.LinkUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

/**
 * Sling model for Image component. Uses inherited properties functionality.
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class Image {

    private static final String AUTHOR_IMAGE_CLASS = "cq-dd-image";
    private static final String PLACEHOLDER_SRC = "/etc/designs/default/0.gif";
    private static final String PLACEHOLDER_TOUCH_CLASS = "cq-placeholder file";
    private static final String PLACEHOLDER_CLASSIC_CLASS = "cq-image-placeholder";
    private static final String WEB_RENDITION_SELECTOR = "img";
    private static final String DEFAULT_MIME_TYPE = "image/png";
    // also configured in com.day.cq.wcm.foundation.impl.AdaptiveImageComponentServlet.xml, has to match!
    private static final String[] WIDTHS = {"270", "370", "570", "1140"};
    
    private static final Logger LOG = LoggerFactory.getLogger(Image.class);

    @Self
    SlingHttpServletRequest request;

    @Inject
    @Optional
    @Via("resource")
    private String fileReference;

    @Inject
    @Optional
    @Via("resource")
    private String isDecorative;

    @Inject
    @Optional
    @Via("resource")
    @Named("jcr:title")
    private String title;

    @Inject
    @Optional
    @Via("resource")
    private String width;

    @Inject
    @Optional
    @Via("resource")
    private String height;

    @Inject
    @Optional
    @Via("resource")
    private String align;

    @Inject
    @Optional
    @Via("resource")
    @Named("linkURL")
    private String linkUrl;

    @Inject
    @Optional
    @Via("resource")
    private String alt;

    @Inject
    @Via("resource")
    @Default(booleanValues = true)
    private boolean responsive;

    public String getFileReference() {
        return fileReference;
    }

    public String getIsDecorative() {
        return isDecorative;
    }

    public String getTitle() {
        return title;
    }

    public String getWidth() {
        if (StringUtils.isEmpty(this.width) || Integer.valueOf(this.width) <= 0) {
            return "";
        }
        return width;
    }

    public String getHeight() {
        if (StringUtils.isEmpty(this.height) || Integer.valueOf(this.height) <= 0) {
            return "";
        }
        return height;
    }
    
    public boolean getResponsive() {
        return this.responsive;
    }

    public String getFileName() {
        final String filePath;
        if (!this.getFileReference().isEmpty()) {
            filePath = this.getFileReference();
        } else {
            filePath = request.getRequestPathInfo().getResourcePath();
        }
        final int lastIndex = filePath.lastIndexOf('/');
        return lastIndex > 0 ? filePath.substring(lastIndex + 1) : filePath;
    }

    public String getLinkUrl() {
        return LinkUtil.pathBrowserPropertyToLink(request, linkUrl);
    }

    public String getAlign() {
        return align;
    }

    public String getCssClass() {
        String cssClass;
        cssClass = AUTHOR_IMAGE_CLASS;
        if (fileReference == null || fileReference.isEmpty()) {
            fileReference = PLACEHOLDER_SRC;
            if (AuthoringUIMode.TOUCH.equals(AuthoringUIMode.fromRequest(request))) {
                cssClass = cssClass + " " + PLACEHOLDER_TOUCH_CLASS;
            } else if (AuthoringUIMode.CLASSIC.equals(AuthoringUIMode.fromRequest(request))) {
                cssClass = cssClass + " " + PLACEHOLDER_CLASSIC_CLASS;
            }
        }
        cssClass = cssClass + " " + align;
        return cssClass;
    }

    public String getAlt() {
        if (!Boolean.getBoolean(isDecorative)) {
            return this.alt;
        }
        return "";
    }

    public String getSrc() {
        if(StringUtils.isNotEmpty(fileReference)&&fileReference.endsWith("svg")){
            return fileReference;
        } else {
            return request.getRequestPathInfo().getResourcePath() + "." + WEB_RENDITION_SELECTOR + "." + getExtension() + getCachingSuffix();
        }
    }

    /**
     * Build up the srcset string. See https://css-tricks.com/responsive-images-youre-just-changing-resolutions-use-srcset/
     * @return String containing srcset attribute value
     */
    public String getSrcSet() {
        final String resPath = request.getRequestPathInfo().getResourcePath();
        final String extension = getExtension();
        final String suffix = getCachingSuffix();
        
        final StringBuilder builder = new StringBuilder();
        for (int i=0; i<WIDTHS.length; i++) {
            final String width = WIDTHS[i];
            // check if the format is bigger then the original, in that case don't add it
            if (isWiderAsOriginal(Integer.valueOf(width))) {
                continue;
            }
            
            if (i > 0) {
                builder.append(", ");
            }
            builder.append(resPath);
            builder.append("." + WEB_RENDITION_SELECTOR);
            builder.append("." + width);
            // For now, always use the medium quality, which should be ok in 99% of the cases.
            // In the future we might add a "force high quality" option to the image component if needed.
            builder.append(".MEDIUM");
            builder.append("." + extension);
            builder.append(suffix);
            builder.append(" " + width + "w");
        }
        return builder.toString();
    }

    public String getExtension() {
        final Asset asset;
        String extension = MimeTypes.getExtension(DEFAULT_MIME_TYPE);
        final Resource res = request.getResourceResolver().getResource(fileReference);
        if (res != null && (asset = res.adaptTo(Asset.class)) != null) {
            extension = MimeTypes.getExtension(asset.getMimeType());
        }
        return extension;
    }

    private String getCachingSuffix() {
        String suffix = "";
        try {
            long lastMod = getComponentLastModified();
            long fileLastMod = getFileLastModified();

            if (fileLastMod > lastMod) {
                lastMod = fileLastMod;
            }
            if (lastMod != 0) {
                suffix = "/" + lastMod;
                suffix = suffix + "." + this.getExtension();
            }
        } catch (RepositoryException re) {
            LOG.error("An error occurred while reading node properties", re);
        }
        return suffix;
    }

    public String getResourceType() {
        return request.getResource().getResourceType();
    }

    private boolean isWiderAsOriginal(final int width) {
        final AssetDetails assetDetails = new AssetDetails(getResource(getFileReference()));
        try {
            return width > assetDetails.getWidth();
        } catch (RepositoryException e) {
            LOG.error("Could not get width of DAM asset", e);
        }
        return false;
    }

    private final Resource getResource(final String resourcePath) {
        return request.getResourceResolver().getResource(resourcePath);
    }

    private final long getComponentLastModified() throws RepositoryException {
        long lastMod = 0;
        final String resourcePath = request.getRequestPathInfo().getResourcePath();
        final Resource nodeResource = getResource(resourcePath);
        final Node node = nodeResource.adaptTo(Node.class);
        if (node != null) {
            if (node.hasProperty(JcrConstants.JCR_LASTMODIFIED)) {
                lastMod = node.getProperty(JcrConstants.JCR_LASTMODIFIED).getLong();
            } else if (node.hasProperty(JcrConstants.JCR_CREATED)) {
                lastMod = node.getProperty(JcrConstants.JCR_CREATED).getLong();
            }
        } else {
            final ValueMap values = nodeResource.adaptTo(ValueMap.class);
            if (values != null) {
                Long value = values.get(JcrConstants.JCR_LASTMODIFIED, Long.class);
                if (value == null) {
                    value = values.get(JcrConstants.JCR_CREATED, Long.class);
                }
                if (value != null) {
                    lastMod = value;
                }
            }
        }
        return lastMod;
    }

    private final long getFileLastModified() {
        long fileLastMod = 0;
        if (this.getFileReference().length() > 0) {
            try {
                final Resource refNodeResource = getResource(this.getFileReference());
                final Node refNode = refNodeResource.adaptTo(Node.class);
                if (refNode.getNode(JcrConstants.JCR_CONTENT).hasProperty(JcrConstants.JCR_LASTMODIFIED)) {
                    fileLastMod = refNode.getNode(JcrConstants.JCR_CONTENT).getProperty(JcrConstants.JCR_LASTMODIFIED).getLong();
                } else if (refNode.hasProperty(JcrConstants.JCR_CREATED)) {
                    fileLastMod = refNode.getProperty(JcrConstants.JCR_CREATED).getLong();
                }
            } catch (RepositoryException e) {
                LOG.error("An error occurred while reading resource", e);
            }
        }
        return fileLastMod;
    }

}
