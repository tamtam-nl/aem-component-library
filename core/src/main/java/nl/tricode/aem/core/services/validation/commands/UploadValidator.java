package nl.tricode.aem.core.services.validation.commands;

import com.google.common.collect.Lists;
import nl.tricode.aem.core.ACLConstants;
import nl.tricode.aem.core.services.validation.models.ValidationStatusTypes;
import nl.tricode.aem.core.services.validation.models.ValidatorStatus;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Validation for the upload component
 */
public class UploadValidator extends FieldValidator {

    private static final String ALL_FILE_TYPES_SEPARATOR = "/*";
    private static final String FILE_TYPE_SEPARATOR = "/";
    private static final Logger LOG = LoggerFactory.getLogger(UploadValidator.class);

    @Override
    public List<ValidatorStatus> validate(final String[] values, final Resource resource, final Map<String, RequestParameter[]> requestParameters) {
        List<ValidatorStatus> validatorStatusList = new ArrayList<>();

        validatorStatusList.add(validateRequired(values, resource));

        if (requestParameters != null) {
            validatorStatusList.addAll(validateRequestParameters(createValidators(resource), requestParameters.values()));
        }

        return validatorStatusList;
    }

    private static List<ValidatorStatus> validateRequestParameters(final List<Validator> validators, Collection<RequestParameter[]> requestParameters) {
        final List<ValidatorStatus> statuses = Lists.newArrayList();

        for (final RequestParameter[] parameters : requestParameters) {
            for (RequestParameter parameter : parameters) {
                for (Validator validator : validators) {
                    statuses.add(validator.validate(parameter));
                }
            }
        }
        return statuses;
    }

    private List<Validator> createValidators(final Resource resource) {
        return Lists.newArrayList(
                new FileTypeValidator(resource),
                new FileSizeValidator(resource)
        );
    }

    @FunctionalInterface
    interface Validator {
        ValidatorStatus validate(final RequestParameter requestParameter);
    }

    class FileTypeValidator implements Validator {
        private final List<String> availableTypes = Lists.newArrayList();
        private final Resource resource;

        FileTypeValidator(final Resource resource) {
            this.resource = resource;
            final String[] fileTypes = (String[]) resource.getValueMap().get(ACLConstants.FILE_TYPES_PARAMETER);
            if (fileTypes != null) {
                availableTypes.addAll(Lists.newArrayList(fileTypes));
            }
        }

        @Override
        public ValidatorStatus validate(RequestParameter requestParameter) {
            String mimeType = requestParameter.getContentType();
            if (mimeType != null && !availableTypes.contains(mimeType) && !isAudioVideoOrImage(availableTypes, mimeType)) {
                return new ValidatorStatus(ValidationStatusTypes.FILE_TYPE_NOT_SUPPORTED.getStatusCode(), ValidationStatusTypes.FILE_TYPE_NOT_SUPPORTED.getDescription(), resource);
            }

            return new ValidatorStatus(ValidationStatusTypes.VALIDATION_OK.getStatusCode(), ValidationStatusTypes.VALIDATION_OK.getDescription(), resource);
        }
    }

    class FileSizeValidator implements Validator {
        private final Resource resource;
        private final int maxLength;

        FileSizeValidator(final Resource resource) {
            this.resource = resource;

            Object maxFileSizeProperty = resource.getValueMap().get(ACLConstants.MAX_FILE_SIZE_PARAMETER);
            maxLength = Integer.parseInt(maxFileSizeProperty.toString());
        }

        @Override
        public ValidatorStatus validate(final RequestParameter requestParameter) {
            try {
                final InputStream stream = requestParameter.getInputStream();
                if (stream != null && stream.available() / ACLConstants.BYTES_IN_KILOBYTE > maxLength) {
                    return new ValidatorStatus(ValidationStatusTypes.FILE_SIZE_BIGGER_THAN_MAX_SIZE.getStatusCode(),
                            ValidationStatusTypes.FILE_SIZE_BIGGER_THAN_MAX_SIZE.getDescription(),
                            resource);
                }
            } catch (IOException e) {
                LOG.error("Input stream not available: ", e);
            }

            return new ValidatorStatus(ValidationStatusTypes.VALIDATION_OK.getStatusCode(), ValidationStatusTypes.VALIDATION_OK.getDescription(), resource);
        }
    }

    private static boolean isAudioVideoOrImage(final List<String> availableTypes, final String contentType) {
        String contentTypePrefix = contentType.substring(0, contentType.indexOf(FILE_TYPE_SEPARATOR));
        if (availableTypes.isEmpty()) {
            return true;
        }
        for (String availableType : availableTypes) {
            if (availableType.contains(ALL_FILE_TYPES_SEPARATOR)) {
                String availableTypePrefix = availableType.substring(0, availableType.indexOf(ALL_FILE_TYPES_SEPARATOR));
                if (availableTypePrefix.equals(contentTypePrefix)) {
                    return true;
                }
            }
        }
        return false;
    }

}
