package nl.tricode.aem.core.models.text;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;

import javax.inject.Inject;


@Model(adaptables = SlingHttpServletRequest.class)
public class TextModel {

    @Inject
    @Optional
    @Via("resource")
    private String listItemStyle;

    @Inject
    @Default(values = "inherit")
    @Via("resource")
    private String textColor;

    @Inject
    @Default(values = "inherit")
    @Via("resource")
    private String textAlign;

    public String getListItemStyle() {
        return listItemStyle;
    }

    public String getTextAlign() {
        return textAlign;
    }

    public String getTextColor() {
        return textColor;
    }
}
