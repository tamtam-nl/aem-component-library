package nl.tricode.aem.core.services;

import org.apache.commons.mail.Email;

/**
 * Service for configuring Email Service properties
 */
public interface ACLDefaultMailService {

    void send(Email email);

    String getFromAddress();
}
