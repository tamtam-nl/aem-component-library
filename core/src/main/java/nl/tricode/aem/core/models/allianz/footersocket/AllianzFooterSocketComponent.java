package nl.tricode.aem.core.models.allianz.footersocket;

import nl.tricode.aem.core.services.InheritedPropertiesService;
import nl.tricode.aem.core.utils.granite.MultifieldOptionBuilder;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Model(adaptables = Resource.class)
public class AllianzFooterSocketComponent {

    private static final Logger LOGGER = LoggerFactory.getLogger(AllianzFooterSocketComponent.class);

    @Inject
    @Default(booleanValues = false)
    private boolean disableInheritance;

    @Inject
    @Default(values = {})
    private String[] socketLinks;

    @Inject
    private InheritedPropertiesService inheritedPropertiesService;

    @Self
    private Resource resource;

    public boolean isDisableInheritance() {
        return disableInheritance;
    }

    public List<SocketLinksOption> getSocketLinks() {
        if (disableInheritance) {
            return getSocketLinksOptions(socketLinks);
        } else {
            Object socketLinks = inheritedPropertiesService.getInheritedPropertyValue("socketLinks", resource);
            return getSocketLinksOptions(socketLinks);
        }
    }

    private List<SocketLinksOption> getSocketLinksOptions(final Object socketLinks) {
        final MultifieldOptionBuilder<SocketLinksOption> multifieldOptionBuilder = new MultifieldOptionBuilder<>();

        try {
            if (socketLinks != null) {
                if (socketLinks instanceof String) {
                    return multifieldOptionBuilder.buildOption((String) socketLinks, SocketLinksOption.class);
                } else if (socketLinks instanceof String[]) {
                    return multifieldOptionBuilder.buildOptions((String[]) socketLinks, SocketLinksOption.class);
                }
            }
        } catch (final IOException e) {
            LOGGER.error("Unable to populate list of socket links {}", socketLinks, e);
        }
        return Collections.EMPTY_LIST;
    }

    public boolean isConfigured() {
        return !getSocketLinks().isEmpty();
    }
}
