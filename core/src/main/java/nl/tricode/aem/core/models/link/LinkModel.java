package nl.tricode.aem.core.models.link;


import nl.tricode.aem.core.utils.LinkUtil;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;

import javax.inject.Inject;

/**
 * Model for the link component
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class LinkModel {
    /**
     * Text of the link set in the dialog
     */
    @Inject
    @Optional
    @Via("resource")
    private String text;

    /**
     * Title of the link set in the dialog
     */
    @Inject
    @Optional
    @Via("resource")
    private String title;

    /**
     * href attribute which will be placed in the link. It is set in the dialog
     */
    @Inject
    @Optional
    @Via("resource")
    private String link;

    /**
     * Size of the title of the link which is set in the dialog
     */
    @Inject
    @Optional
    @Via("resource")
    private String size;

    /**
     * Open link in a new page?
     */
    @Inject
    @Optional
    @Via("resource")
    private boolean newPage;

    /**
     * Alignment of the link which is set in the dialog
     */
    @Inject
    @Optional
    @Via("resource")
    private String align;

    @Inject
    private SlingHttpServletRequest request;

    public boolean getNewPage() {
        return newPage;
    }

    public String getText() {
        return text;
    }

    public String getTitle() {
        if (title != null && title.length() > 0) {
            return title;
        } else {
            return text;
        }
    }

    public String getLink() {
        return LinkUtil.pathBrowserPropertyToLink(request, link);
    }

    public String getSize() {
        return size;
    }

    public String getAlign() {
        return align;
    }
}