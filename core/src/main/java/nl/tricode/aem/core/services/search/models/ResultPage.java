package nl.tricode.aem.core.services.search.models;

/**
 * Search result page. This represents one entry in the search results list
 */
public class ResultPage {
    private String title;
    private String path;
    private String excerpt;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }
}
