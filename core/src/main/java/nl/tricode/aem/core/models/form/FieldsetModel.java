package nl.tricode.aem.core.models.form;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.inject.Inject;

/**
 * Model for fieldset component
 */
@Model(adaptables = {Resource.class})
public class FieldsetModel {

    @Inject
    @Optional
    private String title;

    @Inject
    @Optional
    private String hideTitle;

    public String getTitle() {
        return title;
    }

    public String getHideTitle() {
        return hideTitle;
    }
}
