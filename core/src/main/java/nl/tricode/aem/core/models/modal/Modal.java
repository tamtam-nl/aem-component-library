package nl.tricode.aem.core.models.modal;


import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;

import javax.inject.Inject;
import java.util.UUID;

/**
 * Sling model for Modal (Lightbox) component.
 */

@Model(adaptables = SlingHttpServletRequest.class)
public class Modal {

    @Inject
    @Optional
    @Via("resource")
    private String buttonText;

    @Inject
    @Optional
    @Via("resource")
    private String modalHeader;

    @Inject
    @Optional
    @Via("resource")
    private String modalClose;

    public String getButtonText() {
        return buttonText;
    }

    public String getModalHeader() {
        return modalHeader;
    }

    public String getModalClose() {
        return modalClose;
    }

    public String getId() {
        return UUID.randomUUID().toString();
    }
}



