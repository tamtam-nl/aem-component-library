package nl.tricode.aem.core.services.implementations;

import com.day.cq.wcm.api.Page;
import nl.tricode.aem.core.models.megamenu.MenuItem;
import nl.tricode.aem.core.utils.LinkUtil;
import nl.tricode.aem.core.utils.PageUtil;
import nl.tricode.aem.core.services.MenuItemsService;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.LinkedList;
import java.util.List;

/**
 * Implementation of {@link nl.tricode.aem.core.services.MenuItemsService}
 */
@Component(immediate = true, metatype = false, label = "ACL - Menu Items Service")
@Service(MenuItemsService.class)
public class MenuItemServiceImpl implements MenuItemsService {

    private static final int THIRD_LEVEL_NAVIGATION = 3;
    private static final Logger LOG = LoggerFactory.getLogger(MenuItemServiceImpl.class);

    @Override
    public List<MenuItem> getMenuItems(final Page rootPage, final SlingHttpServletRequest request, final int pageLevel) {
        final List<MenuItem> menuItems = new LinkedList<>();
        if (rootPage == null) {
            LOG.error("No menu items found for rootPage: {}", rootPage);
            return menuItems;
        }
        rootPage.listChildren().forEachRemaining(page -> {
            if (!isHiddenInNavigation(page)) {
                MenuItem menuItem = new MenuItem(PageUtil.getNavigationTitleWithFallback(page),
                        LinkUtil.pathBrowserPropertyToLink(request, page.getPath()));
                final List<MenuItem> listOfSubNavigationItems = new LinkedList<>();
                if (pageLevel == THIRD_LEVEL_NAVIGATION) {
                    page.listChildren().forEachRemaining(subPage -> {
                        if (!isHiddenInNavigation(subPage)) {
                            MenuItem subMenuItem = new MenuItem(PageUtil.getNavigationTitleWithFallback(subPage),LinkUtil.pathBrowserPropertyToLink(request, subPage.getPath()));
                            populateSubNavigationItems(request, subPage, subMenuItem);
                            listOfSubNavigationItems.add(subMenuItem);
                            setActivePageStatus(request, subMenuItem);
                        }
                    });
                }
                menuItem.setSubPages(listOfSubNavigationItems);
                setActivePageStatus(request, menuItem);
                menuItems.add(menuItem);
            }
        });
        return menuItems;
    }

    private static void setActivePageStatus(final SlingHttpServletRequest request, final MenuItem menuItem) {
        menuItem.setActive(menuItem.getPath().equals(request.getPathInfo().split("jcr:content")[0]));
    }

    private static void populateSubNavigationItems(final SlingHttpServletRequest request, final Page page, final MenuItem navigationItem) {
        final List<MenuItem> listOfSubNavigationItems = new LinkedList<>();
        page.listChildren().forEachRemaining(subPage -> {
            if (!isHiddenInNavigation(subPage)) {
                listOfSubNavigationItems.add(new MenuItem(PageUtil.getNavigationTitleWithFallback(subPage),
                        LinkUtil.pathBrowserPropertyToLink(request, subPage.getPath())));
            }
        });
        navigationItem.setSubPages(listOfSubNavigationItems);
    }

    private static boolean isHiddenInNavigation(final Page page) {
        return "true".equals(page.getProperties().get("hideInNav"));
    }
}
