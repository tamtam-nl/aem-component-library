package nl.tricode.aem.core.models.form;

import nl.tricode.aem.core.ACLConstants;
import nl.tricode.aem.core.services.validation.commands.textfield.textvalidators.factory.RegexValidationFactory;
import nl.tricode.aem.core.services.validation.models.ValidationStatusTypes;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Model for text component
 */
@Model(adaptables = Resource.class)
public class TextModel extends FormFieldModel {

    private final RegexValidationFactory regexValidationFactory = new RegexValidationFactory();

    @Self
    Resource resource;

    @Inject
    @Optional
    private String placeholder;

    @Inject
    @Optional
    @Default(values = ACLConstants.TEXT_PROPERTY)
    private String type;

    @Inject
    @Optional
    private String prefix;

    @Inject
    @Optional
    private String suffix;

    @Inject
    @Optional
    private String checkMinLength;

    @Inject
    @Optional
    private String checkMaxLength;

    @Inject
    @Optional
    private String checkMinValue;
    @Inject
    @Optional
    private String checkMaxValue;

    @Inject
    @Optional
    private String checkAdvanced;

    @Inject
    @Optional
    @Named(ACLConstants.MIN_LENGTH_PROPERTY)
    private String minLength;

    @Inject
    @Optional
    @Named(ACLConstants.MAX_LENGTH_PROPERTY)
    private String maxLength;

    @Inject
    @Optional
    private String minValue;

    @Inject
    @Optional
    private String maxValue;

    @Inject
    @Optional
    private String minLengthMsg;

    @Inject
    @Optional
    private String maxLengthMsg;

    @Inject
    @Optional
    private String minValueMsg;

    @Inject
    @Optional
    private String maxValueMsg;

    @Inject
    @Optional
    private String advancedValidation;

    @Inject
    @Optional
    private String advancedValidationMsg;

    @Inject
    @Default(values = "")
    private String noWhitespace;

    public String getPlaceholder() {
        return placeholder;
    }

    public String getType() {
        return type;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public String getCheckMinLength() {
        return checkMinLength;
    }

    public String getCheckMaxLength() {
        return checkMaxLength;
    }

    public String getCheckMinValue() {
        return checkMinValue;
    }

    public String getCheckMaxValue() {
        return checkMaxValue;
    }

    public String getCheckAdvanced() {
        return checkAdvanced;
    }

    public String getMinValueMsg() {
        return ACLConstants.NUMBER_PROPERTY.equals(type) && Boolean.parseBoolean(this.checkMinValue) ? (StringUtils.isNotEmpty(this.minValueMsg) ? this.minValueMsg : String.format(ValidationStatusTypes.FIELD_VALUE_LOWER_THAN_MIN_VALUE.getDescription(), this.minValue)) : null;
    }

    public String getMaxValueMsg() {
        return ACLConstants.NUMBER_PROPERTY.equals(type) && Boolean.parseBoolean(this.checkMaxValue) ? (StringUtils.isNotEmpty(this.maxValueMsg) ? this.maxValueMsg : String.format(ValidationStatusTypes.FIELD_VALUE_HIGHER_THAN_MAX_VALUE.getDescription(), this.maxValue)) : null;
    }

    public String getMinLengthMsg() {
        return ACLConstants.TEXT_PROPERTY.equals(type) && Boolean.parseBoolean(this.checkMinLength) ? (StringUtils.isNotEmpty(this.minLengthMsg) ? this.minLengthMsg : String.format(ValidationStatusTypes.FIELD_SHORTER_THAN_MIN_LENGTH.getDescription(), this.minLength)) : null;
    }

    public String getMaxLengthMsg() {
        return ACLConstants.TEXT_PROPERTY.equals(type) && Boolean.parseBoolean(this.checkMaxLength) ? (StringUtils.isNotEmpty(this.maxLengthMsg) ? this.maxLengthMsg : String.format(ValidationStatusTypes.FIELD_LONGER_THAN_MAX_LENGTH.getDescription(), this.minLength)) : null;
    }

    public String getAdvancedValidationMsg() {
        return (ACLConstants.TEXT_PROPERTY.equals(type) && StringUtils.isNotEmpty(this.advancedValidationMsg)) ? this.advancedValidationMsg : regexValidationFactory.getFalseValidatorStatus(resource).getDescription();
    }


    public String getNoWhitespace() {
        return noWhitespace;
    }

    public String getAdvancedValidation() {
        return advancedValidation;
    }

    public String getMinValue() {
        return ACLConstants.NUMBER_PROPERTY.equals(type) && Boolean.parseBoolean(this.checkMinValue) ? minValue : null;
    }

    public String getMaxValue() {
        return ACLConstants.NUMBER_PROPERTY.equals(type) && Boolean.parseBoolean(this.checkMaxValue) ? maxValue : null;
    }

    public String getMinLength() {
        return ACLConstants.TEXT_PROPERTY.equals(type) && Boolean.parseBoolean(this.checkMinLength) ? minLength : null;
    }

    public String getMaxLength() {
        return ACLConstants.TEXT_PROPERTY.equals(type) && Boolean.parseBoolean(this.checkMaxLength) ? maxLength : null;
    }

    public String getRegex() {
        return regexValidationFactory.getRegex(resource);
    }
}