package nl.tricode.aem.core.models.searchresults;

import nl.tricode.aem.core.services.models.PaginationItem;
import nl.tricode.aem.core.services.search.SearchService;
import nl.tricode.aem.core.services.search.models.ResultPage;
import nl.tricode.aem.core.services.search.models.SearchResultSummary;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

/**
 * Sling models button component.
 */
@Model(adaptables = {SlingHttpServletRequest.class})
public class SearchResultModel {

    private static final Logger LOG = LoggerFactory.getLogger(SearchResultModel.class);

    public static final String PARAM_QUERY = "q";

    public static final long DEFAULT_RESULTS_PER_PAGE = 10;
    public static final String SEARCH_IN = "searchIn";


    @Inject
    private SearchService searchService;

    private SearchResultSummary searchResult;

    @Inject
    private SlingHttpServletRequest request;

    @Inject
    @Optional
    @Via("resource")
    private String resultsPerPage;

    @PostConstruct
    protected void init() {
        final String keywords = request.getParameter(PARAM_QUERY);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Starting search on keywords '{}'", keywords);
        }

        String searchInPath = request.getSession().getAttribute(SEARCH_IN).toString();

        searchResult = searchService.search(request, keywords, getSelector(), getResultsPerPage(), searchInPath);
    }

    /**
     * Get the search results
     *
     * @return list with result or null on error
     */
    public List<ResultPage> getResult() {
        return searchResult.getHits();
    }

    public long getNrOfPages() {
        return searchResult.getNumberOfPages();
    }

    public long getTotalMatches() {
        return searchResult.getResultCount();
    }

    public long getPage() {
        return searchResult.getStartPage();
    }

    public String getKeywords() {
        return searchResult.getKeywords();
    }

    public List<PaginationItem> getPaginationItems() {
        return searchResult.getPaginationItems();
    }

    private long getResultsPerPage() {
        if (StringUtils.isEmpty(resultsPerPage)) {
            return DEFAULT_RESULTS_PER_PAGE;
        }
        return Long.valueOf(resultsPerPage);
    }

    private long getSelector() {
        if (request.getRequestPathInfo().getSelectors().length != 0) {
            return Long.valueOf(request.getRequestPathInfo().getSelectors()[0]);
        }
        return 1;
    }

}