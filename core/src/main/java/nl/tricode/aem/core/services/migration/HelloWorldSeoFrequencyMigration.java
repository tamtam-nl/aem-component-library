package nl.tricode.aem.core.services.migration;

import org.apache.felix.scr.annotations.Component;
import org.osgi.framework.Version;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

/**
 * HelloWorld migration script that changes the frequency for seo. Is not enabled by default because the script is here
 * just to show how the migration scripts are to be used.
 * For any migration script that is needed please enable the component.
 */
@Component(enabled = false)
public class HelloWorldSeoFrequencyMigration extends AbstractMigration {

    /**
     * FREQUENCY.
     */
    private static final String FREQUENCY = "frequency";

    /**
     * Content path.
     */
    public static final String CONTENT_PATH = "/content";

    /**
     * Number of migrated nodes.
     */
    private int numOfNodesModified;

    /**
     * Array of old resource types.
     */
    private static final String[] resourceTypes = new String[]{
            "site-platform/components/structure/page"
    };

    @Override
    void doMigration() {
        migrateNodes(getNodesWithResourceTypesInPath(resourceTypes, CONTENT_PATH));
        getLogger().info("Total nodes migrated : {}", numOfNodesModified);
    }

    @Override
    void migrateNode(Node node) throws RepositoryException {
        if (node != null && node.hasProperty(FREQUENCY)) {
            node.setProperty(FREQUENCY, "never");
            updateNodeVersion(node);
            updatePageModified(node);
            ++numOfNodesModified;
        }
    }

    @Override
    Version getLatestVersion() {
        return new Version("1.0.0");
    }
}