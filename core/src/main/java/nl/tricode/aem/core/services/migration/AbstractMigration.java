package nl.tricode.aem.core.services.migration;

import com.day.cq.search.PredicateConverter;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.WCMException;
import nl.tricode.aem.core.utils.ResourceUtil;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.jcr.resource.JcrResourceConstants;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.framework.Version;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Component(
        enabled = true,
        immediate = true,
        componentAbstract = true,
        name = "ACL Content Migration - Base Class")
@Service(value = AbstractMigration.class)
public abstract class AbstractMigration {

    protected static final String PROPERTY_VERSION = "aclVersion";

    @Reference
    protected SlingRepository slingRepository;

    @Reference
    protected SlingSettingsService slingSettingsService;

    @Reference
    protected ResourceResolverFactory resourceResolverFactory;

    @Reference
    protected QueryBuilder queryBuilder;

    protected boolean isTestMode = false;

    protected Session session;

    private ComponentContext componentContext;

    /**
     * Must be defined by the extending class; returns the Version when the last
     * changes were made that nodes need to migrate to.
     *
     * @return The Version
     */
    abstract Version getLatestVersion();

    /**
     * It uses subclass object LOG. Returns a Logger object so that
     * the log entries show the inherited classname.
     *
     * @return A Logger object
     */
    protected Logger getLogger() {
        return LoggerFactory.getLogger(getClass().getName());
    }

    /**
     * Must be defined by the extending class; this is the main method that will
     * do the migration.
     */
    abstract void doMigration();

    /**
     * Must be defined by the extending class; it's the method that migrates a
     * single node.
     *
     * @param node The node to be migrated
     * @throws javax.jcr.RepositoryException If an error occurred in accessing/changing the node
     */
    abstract void migrateNode(Node node) throws RepositoryException;

    /**
     * Called by OSGi when the component is activated
     *
     * @param componentContext The context of this component in OSGi
     */
    @Activate
    protected void activate(final ComponentContext componentContext) {

        ResourceResolver resourceResolver = ResourceUtil.getResourceResolver(resourceResolverFactory);

        session = resourceResolver.adaptTo(Session.class);

        doMigration();

        if (session != null) {
            if (isTestMode) {
                getLogger().info("Test => not saving session");
            } else {
                try {
                    session.save();
                } catch (final Exception exception) {
                    getLogger().error("Unable to save session after migration: {}", exception.getMessage());
                }
            }

            session.logout();
            session = null;
        }

        resourceResolver.close();
    }

    /**
     * Called by OSGi when the component is deactivated
     *
     * @param componentContext The context of this component in OSGi
     */
    @Deactivate
    protected void deactivate(final ComponentContext componentContext) {
    }

    /**
     * Sets the test mode
     *
     * @param isTestMode True or false
     */
    public void setTestMode(final boolean isTestMode) {
        this.isTestMode = isTestMode;
    }

    /**
     * Get nodes with property
     *
     * @param property The property that should exist
     * @param type     The type of the node
     * @param paths    Array with paths
     * @return Node iterator
     */
    protected Iterator<Node> getNodesWithProperty(final String property, final String type, final String[] paths) {
        final Map<String, String> predicateMap = new HashMap<>();

        predicateMap.put("type", type);

        predicateMap.put("1_group.p.or", "true");
        for (int i = 0; i < paths.length; i++) {
            predicateMap.put("1_group." + (i + 1) + "_path", paths[i]);
        }

        predicateMap.put("2_group.p.or", "true");
        predicateMap.put("2_group.property", property);
        predicateMap.put("2_group.property.operation", "exists");

        final PredicateGroup predicateGroup = PredicateConverter.createPredicates(predicateMap);
        getLogger().debug("Query:\n{}", predicateGroup);
        final Query query = queryBuilder.createQuery(predicateGroup, session);
        query.setHitsPerPage(0);
        final SearchResult result = query.getResult();
        getLogger().debug("Search returned {} results in {}", result.getTotalMatches(), result.getExecutionTime());
        return result.getNodes();
    }

    /**
     * Returns an Iterator of all nodes with the specified sling:resourceType
     * below /content
     *
     * @param resourceType The value for sling:resourceType
     * @return The Iterator
     */
    protected Iterator<Node> getNodesWithResourceType(final String resourceType) {
        return getNodesWithResourceTypesInPaths(new String[]{resourceType}, new String[]{"/content"});
    }

    /**
     * Returns an Iterator of all nodes with the specified sling:resourceType
     * below the specified path
     *
     * @param resourceType The value for sling:resourceType
     * @param path         The path to search in
     * @return The Iterator
     */
    protected Iterator<Node> getNodesWithResourceTypeInPath(final String resourceType, final String path) {
        return getNodesWithResourceTypesInPaths(new String[]{resourceType}, new String[]{path});
    }

    /**
     * Returns an Iterator of all nodes with the specified sling:resourceTypes
     * below the specified path
     *
     * @param resourceTypes The value for sling:resourceTypes
     * @param path          The path to search in
     * @return The Iterator
     */
    protected Iterator<Node> getNodesWithResourceTypesInPath(final String[] resourceTypes, final String path) {
        return getNodesWithResourceTypesInPaths(resourceTypes, new String[]{path});
    }

    /**
     * Returns an Iterator of all nodes with the specified sling:resourceType
     * below the specified paths
     *
     * @param resourceType The values for sling:resourceType
     * @param paths        The paths to search in
     * @return The Iterator
     */
    protected Iterator<Node> getNodesWithResourceTypeInPaths(final String resourceType, final String[] paths) {
        return getNodesWithResourceTypesInPaths(new String[]{resourceType}, paths);
    }

    /**
     * Returns an Iterator of all nodes with the specified sling:resourceTypes
     * below the specified paths
     *
     * @param resourceTypes The values for sling:resourceType
     * @param paths         The paths to search in
     * @return The Iterator
     */
    protected Iterator<Node> getNodesWithResourceTypesInPaths(final String[] resourceTypes, final String[] paths) {
        final Map<String, String> predicateMap = new HashMap<>();

        predicateMap.put("type", "nt:unstructured");

        predicateMap.put("1_group.p.or", "true");
        for (int i = 0; i < paths.length; i++) {
            predicateMap.put("1_group." + (i + 1) + "_path", paths[i]);
        }

        predicateMap.put("2_group.p.or", "true");
        for (int i = 0; i < resourceTypes.length; i++) {
            predicateMap.put("2_group." + (i + 1) + "_property", JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY);
            predicateMap.put("2_group." + (i + 1) + "_property.value", resourceTypes[i]);
        }

        final PredicateGroup predicateGroup = PredicateConverter.createPredicates(predicateMap);
        getLogger().debug("Query:\n{}", predicateGroup);
        final Query query = queryBuilder.createQuery(predicateGroup, session);
        query.setHitsPerPage(0);
        final SearchResult result = query.getResult();
        getLogger().debug("Search returned {} results in {}", result.getTotalMatches(), result.getExecutionTime());
        return result.getNodes();
    }

    /**
     * Migrates the nodes returned by the Iterator; This method will call
     * migrateNode(node) to handle the actual migration
     *
     * @param nodeIterator The Iterator that provides the nodes to be migrated
     */
    protected void migrateNodes(final Iterator<Node> nodeIterator) {
        while (nodeIterator.hasNext()) {
            final Node node = nodeIterator.next();
            try {
                if (!ignoreNode(node)) {
                    migrateNode(node);
                }
            } catch (final Exception exception) {
                try {
                    getLogger().error("Error migrating node {}: {}", node.getPath(), exception.getMessage());
                } catch (final Exception exception2) {
                    getLogger().error("Error migrating node: {}", exception.getMessage());
                }
            }
        }
    }

    /**
     * Update the lgiVersion property of the node to prevent it from being
     * migrated again.
     *
     * @param node The node to update
     * @throws javax.jcr.RepositoryException If an error occurred in accessing/changing the node
     */
    protected void updateNodeVersion(final Node node) throws RepositoryException {
        getLogger().debug("Updating node {} to version {}", node.getPath(), getLatestVersion().toString());
        node.setProperty(PROPERTY_VERSION, getLatestVersion().toString());
    }

    /**
     * Mark the containing page as modified; update cq:Modified and
     * cq:ModifiedBy properties
     *
     * @param node The node inside the page
     */
    protected void updatePageModified(final Node node) {
        try {
            final Node pageNode = getPageNode(node);
            if (pageNode != null) {
                final ResourceResolver resourceResolver = ResourceUtil.getResourceResolver(resourceResolverFactory);
                final PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
                if(pageManager != null) {
                    pageManager.touch(pageNode, /* shallow */true, Calendar.getInstance(), /* clearReplication */true);
                }
                resourceResolver.close();
                getLogger().debug("Touched page {}", pageNode.getPath());
            }
        } catch (final RepositoryException repositoryException) {
            // ignore; only occurs in logging command
        } catch (final WCMException wcmException) {
            getLogger().error("WCM Exception: {}", wcmException.getMessage());
        }
    }

    /**
     * Returns the page (as node) that contains the provided node
     *
     * @param node The node inside the page
     * @return Node that references the page
     */
    protected Node getPageNode(final Node node) {
        Node pageNode = node;
        try {
            while (pageNode != null) {
                if (pageNode.getPrimaryNodeType().isNodeType(NameConstants.NT_PAGE)) {
                    return pageNode;
                }
                pageNode = pageNode.getParent();
            }
            getLogger().warn("Unable to find page for {}", node != null ? node.getPath() : "");
        } catch (final RepositoryException repositoryException) {
            getLogger().error("Unable to find page from node: {}", repositoryException.getMessage());
        }
        return null;
    }

    /**
     * Returns True if the node should not be migrated. This can be because the
     * node is in a path we don't process, because the node is locked or because
     * the node is of a newer (or same) version.
     *
     * @param node The Node to test
     * @return True or False
     */
    protected boolean ignoreNode(final Node node) {
        try {
            if (node.isLocked()) {
                getLogger().warn("Skipping locked node: {}", node.getPath());
                return true;
            }
            if (getNodeVersion(node).compareTo(getLatestVersion()) >= 0) {
                getLogger().debug("Node version {} is newer than {}", getNodeVersion(node).toString(),
                        getLatestVersion().toString());
                return true;
            }
        } catch (final RepositoryException repositoryException) {
            getLogger().error("Error accessing node: {}", repositoryException.getMessage());
            return true;
        }

        return false;
    }

    /**
     * Returns the lgiVersion of the node, 0.0.0 if not set.
     *
     * @param node The node to get lgiVersion from
     * @return The Version
     */
    protected Version getNodeVersion(final Node node) {
        try {
            if (node.hasProperty(PROPERTY_VERSION)) {
                final String versionString = node.getProperty(PROPERTY_VERSION).getString();
                return Version.parseVersion(versionString);
            }
        } catch (final RepositoryException repositoryException) {
            getLogger().error(repositoryException.getMessage(), repositoryException);
        }
        return Version.emptyVersion;
    }

    /**
     * Returns the component context to be used in other migration services extending this class.
     * @return
     *       componentContext.
     */
    public ComponentContext getComponentContext()
    {
        return componentContext;
    }
}
