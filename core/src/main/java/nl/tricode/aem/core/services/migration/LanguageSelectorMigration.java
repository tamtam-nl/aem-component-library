package nl.tricode.aem.core.services.migration;

import org.apache.felix.scr.annotations.Component;
import org.osgi.framework.Version;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

/**
 * Migration script to add language selector in the header of the base template and the search results template
 */
@Component(enabled = true)
public class LanguageSelectorMigration extends AbstractMigration {

    /**
     * Content path.
     */
    public static final String CONTENT_PATH = "/content";

    /**
     * Number of migrated nodes.
     */
    private int numOfNodesModified;

    /**
     * Array of old resource types.
     */
    private static final String[] resourceTypes = new String[]{
            "acl/components/structure/pages/base-template-page",
            "acl/components/structure/pages/searchresult"
    };

    @Override
    void doMigration() {
        migrateNodes(getNodesWithResourceTypesInPath(resourceTypes, CONTENT_PATH));
        getLogger().info("Total nodes migrated : {}", numOfNodesModified);
    }

    @Override
    void migrateNode(final Node node) throws RepositoryException {
        if (node != null) {
            final Node headerNode = node.getNode("header");
            if (headerNode != null) {
                headerNode.addNode("languageselector");
                headerNode.getNode("languageselector").setProperty("sling:resourceType", "acl/components/structure/language-selector");
                updateNodeVersion(headerNode);
                updatePageModified(node);
                ++numOfNodesModified;
            }
        }
    }

    @Override
    Version getLatestVersion() {
        return new Version("1.0.0");
    }
}