package nl.tricode.aem.core.utils;

import com.day.cq.wcm.api.designer.Designer;
import com.day.cq.wcm.api.designer.Style;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class for working with designer
 */
public class DesignerUtil {

    private static final Logger LOG = LoggerFactory.getLogger(DesignerUtil.class);

    private DesignerUtil() {}

    public static Style getContentStyle(final Designer designer, final String path, final ResourceResolver resourceResolver) {
        if (path != null) {
            Resource contentStyleResource = resourceResolver.getResource(path);
            if(contentStyleResource != null) {
                return designer.getStyle(contentStyleResource);
            }
        }
        return null;
    }
}
