package nl.tricode.aem.core.models.form.selectable;

/**
 * Radio button item class.
 */
public class SelectableItem {

    private String text;
    private String value;

    public SelectableItem(String text, String value) {
        this.text = text;
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public String getValue() {
        return value;
    }
}
