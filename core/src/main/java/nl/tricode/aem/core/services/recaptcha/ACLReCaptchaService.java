package nl.tricode.aem.core.services.recaptcha;

import org.apache.sling.api.SlingHttpServletRequest;

import java.io.IOException;

/**
 * Service for dealing with inherited properties.
 */
@FunctionalInterface
public interface ACLReCaptchaService {
    /**
     * Check if the user's recaptcha response is valid
     *
     */
    ReCaptchaApiResponse check(SlingHttpServletRequest slingRequest, String resourcePath,
                               String challenge, String remoteIp) throws IOException;
}
