package nl.tricode.aem.core.utils;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Utility class for working with resources
 */
public final class ResourceUtil {

    private static final Logger LOG = LoggerFactory.getLogger(ResourceUtil.class);

    /**
     * Empty constructor
     */
    private ResourceUtil() {

    }

    /**
     * Find the page that contains the resource
     *
     * @param resource - The resource
     * @return A Page object
     */
    public static Page getResourcePage(final Resource resource) {
        final PageManager pageManager = resource.getResourceResolver().adaptTo(PageManager.class);
        if (pageManager != null) {
            return pageManager.getContainingPage(resource);
        }
        LOG.error("Cannot adapt resource {} to page manager", resource);
        return null;
    }

    /**
     * Provides the resource resolver from the adminResourceResolver sub-service.
     *
     * @return ResourceResolver
     */
    public static ResourceResolver getResourceResolver(final ResourceResolverFactory resourceFactory) {
        final Map<String, Object> param = new HashMap<>();
        param.put(ResourceResolverFactory.SUBSERVICE, "adminResourceResolver");
        ResourceResolver resourceResolver = null;
        try {
            resourceResolver = resourceFactory.getServiceResourceResolver(param);
        } catch (final LoginException e) {
            LOG.error("Unable to get service resource resolver", e);
        }
        return resourceResolver;
    }
}
