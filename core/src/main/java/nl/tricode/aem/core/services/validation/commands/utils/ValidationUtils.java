package nl.tricode.aem.core.services.validation.commands.utils;

import nl.tricode.aem.core.ACLConstants;
import nl.tricode.aem.core.models.form.selectable.SelectableBuilder;
import nl.tricode.aem.core.models.form.selectable.SelectableItem;
import nl.tricode.aem.core.services.validation.models.ValidationStatusTypes;
import nl.tricode.aem.core.services.validation.models.ValidatorStatus;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * Util class for validation methods
 */
public class ValidationUtils {

    private static final Logger LOG = LoggerFactory.getLogger(ValidationUtils.class);

    private ValidationUtils() {
    }

    public static ValidatorStatus validateMaxLength(final String[] values, final Resource resource) {
        return validateLength(values, resource, ACLConstants.MAX_LENGTH_PROPERTY);
    }

    public static ValidatorStatus validateMinLength(final String[] values, final Resource resource) {
        return validateLength(values, resource, ACLConstants.MIN_LENGTH_PROPERTY);
    }

    private static ValidatorStatus validateLength(final String[] values, final Resource resource, final String lengthParameter) {
        ValueMap resourceValueMap = resource.getValueMap();
        Object lengthProperty = resourceValueMap.get(lengthParameter);
        Object maxLengthChecked = resourceValueMap.get(ACLConstants.CHECK_MAX_LENGTH_PROPERTY);
        Object minLengthChecked = resourceValueMap.get(ACLConstants.CHECK_MIN_LENGTH_PROPERTY);

        if (lengthProperty != null && ArrayUtils.isNotEmpty(values)) {
            long length = Long.parseLong(lengthProperty.toString());
            if (maxLengthChecked != null && lengthParameter.equals(ACLConstants.MAX_LENGTH_PROPERTY) && values[0].length() > length) {
                return new ValidatorStatus(ValidationStatusTypes.FIELD_LONGER_THAN_MAX_LENGTH.getStatusCode(), String.format(ValidationStatusTypes.FIELD_LONGER_THAN_MAX_LENGTH.getDescription(), lengthProperty.toString()), resource);
            }
            if (minLengthChecked != null && lengthParameter.equals(ACLConstants.MIN_LENGTH_PROPERTY) && values[0].length() < length) {
                return new ValidatorStatus(ValidationStatusTypes.FIELD_SHORTER_THAN_MIN_LENGTH.getStatusCode(), String.format(ValidationStatusTypes.FIELD_SHORTER_THAN_MIN_LENGTH.getDescription(), lengthProperty.toString()), resource);
            }
        }
        return new ValidatorStatus(ValidationStatusTypes.VALIDATION_OK.getStatusCode(), ValidationStatusTypes.VALIDATION_OK.getDescription(), resource);
    }

    /**
     * Method that validates if the submitted value is contained in the available options.
     * If the user tries to submit a value that is not in the provided values this method will return status for unknown value.
     *
     * @param values    = submitted values
     * @param resource  = the field resource
     * @param parameter = the parameter field that contains all the available options
     * @return ValidatorStatus object that contains Validation Ok or Error code.
     */
    public static ValidatorStatus validateRightValuesSubmitted(final String[] values, final Resource resource, final String parameter) {
        if (values != null) {
            String[] groupItems = (String[]) resource.getValueMap().get(parameter);
            String isEmptyOptionSelected = (String) resource.getValueMap().get(ACLConstants.SHOW_EMPTY_OPTION_PROPERTY);
            List<String> groupItemsList = getAllValues(groupItems);
            if (!isValueContained(groupItemsList, values, isEmptyOptionSelected)) {
                return new ValidatorStatus(ValidationStatusTypes.SUBMITTED_VALUE_UNKNOWN.getStatusCode(), ValidationStatusTypes.SUBMITTED_VALUE_UNKNOWN.getDescription(), resource);
            }
        }
        //return status OK
        return new ValidatorStatus(ValidationStatusTypes.VALIDATION_OK.getStatusCode(), ValidationStatusTypes.VALIDATION_OK.getDescription(), resource);
    }

    public static ValidatorStatus validateMinValuesSelectedOnCheckBox(final String[] values, final Resource resource) {
        String minSelectionsAllowed = (String) resource.getValueMap().get(ACLConstants.MIN_LENGTH_PROPERTY);
        if (minSelectionsAllowed != null && values != null) {
            if (values.length < Integer.parseInt(minSelectionsAllowed)) {
                return new ValidatorStatus(ValidationStatusTypes.MIN_NUMBER_OF_SELECTIONS_MISSED.getStatusCode(), String.format(ValidationStatusTypes.MIN_NUMBER_OF_SELECTIONS_MISSED.getDescription(), minSelectionsAllowed), resource);
            }
        }
        //return status OK
        return new ValidatorStatus(ValidationStatusTypes.VALIDATION_OK.getStatusCode(), ValidationStatusTypes.VALIDATION_OK.getDescription(), resource);
    }

    private static boolean isValueContained(final List<String> groupItemsList, final String[] values, String isEmptyOptionSelected) {
        if (values != null && groupItemsList.containsAll(Arrays.asList(values))) {
            return true;
        } else if (Boolean.parseBoolean(isEmptyOptionSelected) && ArrayUtils.isNotEmpty(values) && values[0].equals("on")) {
            return true;
        }
        return false;
    }

    private static List<String> getAllValues(final String[] groupItems) {
        List<SelectableItem> groupItemsList = SelectableBuilder.buildSelectableItems(groupItems);
        return groupItemsList.stream().map(SelectableItem::getValue).collect(Collectors.toList());
    }

    public static ValidatorStatus validateMaxNumberValue(final String[] values, final Resource resource) {
        return validateValue(values, resource, ACLConstants.MAX_VALUE_PROPERTY);
    }

    public static ValidatorStatus validateMinNumberValue(final String[] values, final Resource resource) {
        return validateValue(values, resource, ACLConstants.MIN_VALUE_PROPERTY);
    }

    private static ValidatorStatus validateValue(final String[] values, final Resource resource, final String maxOrMinValueParameter) {

        ValueMap resourceValueMap = resource.getValueMap();
        Object maxOrMinValueProperty = resourceValueMap.get(maxOrMinValueParameter);
        Object maxValueChecked = resourceValueMap.get(ACLConstants.CHECK_MAX_VALUE_PROPERTY);
        Object minValueChecked = resourceValueMap.get(ACLConstants.CHECK_MIN_VALUE_PROPERTY);
        if (maxOrMinValueProperty != null && ArrayUtils.isNotEmpty(values)) {
            try {
                long maxOrMinValue = Long.parseLong(maxOrMinValueProperty.toString());
                long value = Long.parseLong(values[0]);
                if (maxValueChecked != null && maxOrMinValueParameter.equals(ACLConstants.MAX_VALUE_PROPERTY) && value > maxOrMinValue) {
                    return new ValidatorStatus(ValidationStatusTypes.FIELD_VALUE_HIGHER_THAN_MAX_VALUE.getStatusCode(), String.format(ValidationStatusTypes.FIELD_VALUE_HIGHER_THAN_MAX_VALUE.getDescription(), maxOrMinValueProperty.toString()), resource);
                }
                if (minValueChecked != null && maxOrMinValueParameter.equals(ACLConstants.MIN_VALUE_PROPERTY) && value < maxOrMinValue) {
                    return new ValidatorStatus(ValidationStatusTypes.FIELD_VALUE_LOWER_THAN_MIN_VALUE.getStatusCode(), String.format(ValidationStatusTypes.FIELD_VALUE_LOWER_THAN_MIN_VALUE.getDescription(), maxOrMinValueProperty.toString()), resource);
                }
            } catch (NumberFormatException e) {
                LOG.error("The object is not a number ", e);
            }
        }
        return new ValidatorStatus(ValidationStatusTypes.VALIDATION_OK.getStatusCode(), ValidationStatusTypes.VALIDATION_OK.getDescription(), resource);
    }

    public static ValidatorStatus validateRegex(final String[] values, final Resource resource, final String patternString, final ValidatorStatus validatorStatus) {
        if (!patternString.isEmpty()) {
            final Pattern pattern = Pattern.compile(patternString);
            if (!pattern.matcher(values[0]).matches()) {
                return validatorStatus;
            }
        }
        return new ValidatorStatus(ValidationStatusTypes.VALIDATION_OK.getStatusCode(), ValidationStatusTypes.VALIDATION_OK.getDescription(), resource);
    }
}
