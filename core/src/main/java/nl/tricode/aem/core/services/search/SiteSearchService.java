package nl.tricode.aem.core.services.search;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import nl.tricode.aem.core.services.models.PaginationItem;
import nl.tricode.aem.core.services.search.models.ResultPage;
import nl.tricode.aem.core.services.search.models.SearchResultSummary;
import nl.tricode.aem.core.utils.LinkUtil;
import nl.tricode.aem.core.utils.ResourceUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of {@link nl.tricode.aem.core.services.search.SearchService}
 */
@Component(immediate = true, label = "Search service", description = "Search service for full text search on websites")
@Service(value = SearchService.class)
public class SiteSearchService implements SearchService {

    private static final Logger LOG = LoggerFactory.getLogger(SiteSearchService.class);

    @Reference
    private QueryBuilder queryBuilder;

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Override
    public SearchResultSummary search(final SlingHttpServletRequest request,
                                      final String keywords,
                                      final long startPage,
                                      final long resultsPerPage,
                                      final String searchInPath) {
        if (StringUtils.isEmpty(keywords)) {
            LOG.error("No keywords to search for");
            return new SearchResultSummary();
        }

        final Map params = createQueryParams(keywords, searchInPath);

        ResourceResolver resourceResolver = null;
        Session session = null;
        try {
            resourceResolver = ResourceUtil.getResourceResolver(resourceResolverFactory);
            session = resourceResolver.adaptTo(Session.class);

            final SearchResult searchResult = executeSearchResultsQuery(startPage, resultsPerPage, params, session);

            return buildSummarySearchResult(request, keywords, startPage, resultsPerPage, searchResult);
        } catch (final RepositoryException e) {
            LOG.error("Error reading search results", e);
        } finally {
            if (session != null && session.isLive()) {
                session.logout();
            }
            if (resourceResolver != null && resourceResolver.isLive()) {
                resourceResolver.close();
            }
        }

        return new SearchResultSummary();
    }

    private SearchResultSummary buildSummarySearchResult(final SlingHttpServletRequest request, final String keywords,
                                                         final long startPage, final long resultsPerPage,
                                                         final SearchResult searchResult) throws RepositoryException {
        final SearchResultSummary summary = buildResultList(request, searchResult);
        summary.setStartPage(startPage);
        summary.setNumberOfPages(calculateNumberOfPages(summary.getResultCount(), resultsPerPage));
        summary.setKeywords(keywords);
        addPaginationInfo(summary, ResourceUtil.getResourcePage(request.getResource()));
        return summary;
    }

    private SearchResult executeSearchResultsQuery(final long startPage, final long resultsPerPage, final Map params,
                                                   final Session session) {
        final Query query = queryBuilder.createQuery(PredicateGroup.create(params), session);
        query.setStart((startPage - 1) * resultsPerPage);
        query.setHitsPerPage(resultsPerPage);
        return query.getResult();
    }

    private void addPaginationInfo(final SearchResultSummary summary, final Page currentPage) {
        for (int i = 1; i <= summary.getNumberOfPages(); i++) {
            final PaginationItem item = buildPaginationItem(summary, currentPage, i);
            summary.getPaginationItems().add(item);
        }
    }

    private PaginationItem buildPaginationItem(final SearchResultSummary summary, final Page currentPage, final int i) {
        final PaginationItem item = new PaginationItem();
        item.setTitle(String.valueOf(i));
        item.setLink(buildLink(currentPage, summary.getKeywords(), i));
        item.setActive(summary.getStartPage() == i);
        return item;
    }

    private String buildLink(final Page currentPage, final String keywords, final int i) {
        final StringBuilder sb = new StringBuilder();
        if (currentPage != null) {
            sb.append(currentPage.getPath());
            sb.append(".").append(i).append(".html?q=").append(keywords);
        }
        return sb.toString();
    }

    private Map createQueryParams(final String query, final String searchInPath) {

        final Map<String, String> params = new HashMap<>();
        params.put("path", searchInPath);
        params.put("type", NameConstants.NT_PAGE);
        params.put("fulltext", query);
        params.put("orderby", "@jcr:score");
        // only show pages that are available for indexing
        params.put("1_property", "jcr:content/availableForIndexing");
        params.put("1_property.value", "on");
        return params;
    }

    private SearchResultSummary buildResultList(final SlingHttpServletRequest request, final SearchResult searchResult) throws RepositoryException {
        final SearchResultSummary summary = new SearchResultSummary();
        summary.setResultCount(searchResult.getTotalMatches());
        for (final Hit hit : searchResult.getHits()) {
            final ResultPage resultObject = buildResultPage(request, hit);
            summary.getHits().add(resultObject);
        }
        return summary;
    }

    private ResultPage buildResultPage(final SlingHttpServletRequest request, final Hit hit) throws RepositoryException {
        final ResultPage resultObject = new ResultPage();
        resultObject.setTitle(hit.getTitle());
        resultObject.setPath(LinkUtil.pathBrowserPropertyToLink(request, hit.getPath()));
        resultObject.setExcerpt(hit.getExcerpt());
        return resultObject;
    }

    private long calculateNumberOfPages(final double resultCount, final double resultsPerPage) {
        return (long) Math.ceil(resultCount / resultsPerPage);
    }
}
