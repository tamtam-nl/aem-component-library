package nl.tricode.aem.core.models.anchor;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;

/**
 * Anchor component model with single property.
 */
@Model(adaptables = Resource.class)
public class AnchorComponent {

    @Inject @Default(values = "")
    private String anchorId;

    public String getAnchorId() {
        return anchorId;
    }
}
