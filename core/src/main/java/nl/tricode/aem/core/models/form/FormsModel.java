package nl.tricode.aem.core.models.form;

import nl.tricode.aem.core.services.validation.models.ValidatorStatus;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.inject.Inject;

@Model(adaptables = SlingHttpServletRequest.class)
public class FormsModel {

    @Self
    private SlingHttpServletRequest request;

    @Inject
    @Optional
    @Via("resource")
    protected String target;

    @Inject
    @Optional
    @Via("resource")
    protected String requiredMessage;

    @Inject
    @Optional
    @Via("resource")
    @Default(values = "GET")
    private String method;

    @Inject
    @Optional
    @Via("resource")
    private String emailAddressTo;

    @Inject
    @Optional
    @Via("resource")
    private String emailSubject;

    @Inject
    @Default (values = "default")
    @Via("resource")
    private String formWidth;
    
    @Inject
    @Optional
    @Via("resource")
    @Default(values = "Submit")
    private String buttonText;

    @Inject
    @Optional
    @Via("resource")
    private String horizontalLabels;

    @Inject
    @Optional
    @Via("resource")
    private String buttonAlign;
    
    @Inject
    @Optional
    @Via("resource")
    @Default(values = "btn-default")
    private String buttonStyle;

    @Inject
    @Optional
    @Via("resource")
    @Default(values = "size-md")
    private String buttonSize;
    
    public String getTarget() {
        return target;
    }

    public String getMethod() {
        return method;
    }

    public String getEmailAddressTo() {
        return emailAddressTo;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public ValidatorStatus getValidationMessage() {
        if (request != null && request.getSession() != null) {
            Object responseValidatorStatus = request.getSession().getAttribute("responseValidatorStatus");
            if (responseValidatorStatus != null) {
                return (ValidatorStatus)responseValidatorStatus;
            }
        }
        return null;
    }

    public String getButtonText() {
        return buttonText;
    }

    public String getButtonAlign() {
        return buttonAlign;
    }

    public String getButtonStyle() {
        return buttonStyle;
    }

    public String getButtonSize() {
        return buttonSize;
    }
    public String getFormWidth() { return formWidth; }

    public String getHorizontalLabels() { return horizontalLabels; }

    public String getRequiredMessage() {
        return requiredMessage;
    }
}
