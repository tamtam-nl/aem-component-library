package nl.tricode.aem.core.services.validation.commands.textfield.textvalidators;

import nl.tricode.aem.core.ACLConstants;
import nl.tricode.aem.core.services.validation.models.ValidationStatusTypes;
import nl.tricode.aem.core.services.validation.models.ValidatorStatus;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;

/**
 */
public class TextRegexValidator implements RegexValidator {

    private ValidatorStatus validatorStatus;
    private String regex;

    @Override
    public String getRegex(final Resource resource) {
        buildRegex(resource);
        return regex;
    }

    @Override
    public ValidatorStatus getFalseValidatorStatus(final Resource resource) {
        buildRegex(resource);
        return validatorStatus;
    }

    private void buildRegex(final Resource resource) {
        if (regex == null || validatorStatus == null) {
            ValueMap resourceValueMap = resource.getValueMap();
            Object advancedValidation = resourceValueMap.get("advancedValidation");
            Object checkAdvanced = resourceValueMap.get("checkAdvanced");
            String noWhitespace = (String) resourceValueMap.get("noWhitespace");
            validatorStatus = new ValidatorStatus(ValidationStatusTypes.TEXT_FIELD_NOT_VALID.getStatusCode(), ValidationStatusTypes.TEXT_FIELD_NOT_VALID.getDescription(), resource);
            StringBuilder regexBuilder = new StringBuilder();

            if (checkAdvanced != null && Boolean.parseBoolean(checkAdvanced.toString())) {
                regexBuilder.append("^[a-zA-Z");
                validatorStatus = new ValidatorStatus(ValidationStatusTypes.FIELD_NOT_ALPHA.getStatusCode(), ValidationStatusTypes.FIELD_NOT_ALPHA.getDescription(), resource);
                if (ACLConstants.ALPHANUMERIC_PROPERTY.equals(advancedValidation)) {
                    regexBuilder.append("0-9");
                    validatorStatus = new ValidatorStatus(ValidationStatusTypes.FIELD_NOT_ALPHANUMERIC.getStatusCode(), ValidationStatusTypes.FIELD_NOT_ALPHANUMERIC.getDescription(), resource);
                }
                if (StringUtils.isEmpty(noWhitespace)) {
                    regexBuilder.append(" ");
                    //the validation message is same as above, only no white space info added
                    validatorStatus = new ValidatorStatus(ValidationStatusTypes.NO_WHITESPACE.getStatusCode(), validatorStatus.getDescription() + ValidationStatusTypes.NO_WHITESPACE, validatorStatus.getResource());
                }
                regexBuilder.append("]+$");
            }
            regex = regexBuilder.toString();
        }
    }
}
