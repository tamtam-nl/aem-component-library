package nl.tricode.aem.core.services.validation.commands;

import nl.tricode.aem.core.ACLConstants;
import nl.tricode.aem.core.services.validation.commands.utils.ValidationUtils;
import nl.tricode.aem.core.services.validation.models.ValidatorStatus;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.Resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Validation for the radio button group
 */
public class RadioButtonValidator extends FieldValidator {

    @Override
    public List<ValidatorStatus> validate(final String[] values, final Resource resource, final Map<String, RequestParameter[]> requestParameters) {
        List<ValidatorStatus> validatorStatusList = new ArrayList<>();

        validatorStatusList.add(validateRequired(values, resource));
        validatorStatusList.add(ValidationUtils.validateRightValuesSubmitted(values, resource, ACLConstants.SELECTABLE_ITEMS_PROPERTY));

        return validatorStatusList;
    }


}
