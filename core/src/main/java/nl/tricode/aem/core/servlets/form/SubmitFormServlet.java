package nl.tricode.aem.core.servlets.form;

import nl.tricode.aem.core.ACLConstants;
import nl.tricode.aem.core.services.implementations.ACLDefaultMailServiceImpl;
import nl.tricode.aem.core.services.validation.models.ValidatorStatus;
import nl.tricode.aem.core.utils.EmailSendingUtil;
import nl.tricode.aem.core.utils.FormHandlerUtil;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@SlingServlet(
        label = "Form Submit Servlet",
        methods = {"GET", "POST"},
        resourceTypes = "/apps/acl/components/content/forms/form",
        selectors = "submit"
)
public class SubmitFormServlet extends SlingAllMethodsServlet {

    private static final Logger LOG = LoggerFactory.getLogger(SubmitFormServlet.class);

    @Reference
    private transient ACLDefaultMailServiceImpl mailService;

    @Override
    public void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        sendRequest(request, response);
    }

    @Override
    public void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        sendRequest(request, response);
    }

    private void sendRequest(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        String target = getProperty(request, ACLConstants.TARGET);
        String errorPage = getProperty(request, ACLConstants.ERROR_PAGE) + ACLConstants.EXTENSION_PAGE;
        String successPage = getProperty(request, ACLConstants.SUCCESS_PAGE) + ACLConstants.EXTENSION_PAGE;
        Map parameters = request.getParameterMap();
        int responseCode = 200;
        List<ValidatorStatus> listOfValidatorStatuses = FormHandlerUtil.validate(request);
        try {

            for (ValidatorStatus validatorStatus : listOfValidatorStatuses) {
                if (validatorStatus.getStatusCode() != 200) {
                    response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
                    request.getSession().setAttribute("responseValidatorStatus", validatorStatus);
                    response.sendRedirect(getRequestBaseUrl(request));
                    return;
                }
            }

            EmailSendingUtil.sendEmailWithSubmittedParameters(request, mailService);

            if (request.getMethod().equals(HttpConstants.METHOD_GET)) {
                responseCode = FormHandlerUtil.sendGet(target, parameters);
            } else if (request.getMethod().equals(HttpConstants.METHOD_POST)) {
                responseCode = FormHandlerUtil.sendPost(target, parameters);
            }
            if (responseCode == 200) {
                response.sendRedirect(successPage);
            } else {
                response.sendRedirect(errorPage);
            }
            response.setStatus(HttpServletResponse.SC_OK);
            response.flushBuffer();
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }

    }

    private static String getProperty(SlingHttpServletRequest request, String property) {
        String target = "";
        Resource component = request.getResource();
        if (component.getValueMap().get(property) != null) {
            target = component.getValueMap().get(property).toString();
        }
        return target;
    }

    private static String getRequestBaseUrl(final SlingHttpServletRequest request) {
        String requestUri = request.getRequestURI();
        if (requestUri != null) {
            requestUri = requestUri.substring(0, requestUri.indexOf("/" + ACLConstants.JCR_CONTENT_URL_PROPERTY));
            requestUri += ACLConstants.HTML_EXTENSION;
        }
        return requestUri;
    }
}