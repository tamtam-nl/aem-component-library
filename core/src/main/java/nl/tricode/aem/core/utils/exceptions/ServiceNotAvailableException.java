package nl.tricode.aem.core.utils.exceptions;

/**
 * Exception that indicates that a specific service, which is needed to get the specific data, is not available.
 *
 * This is a RuntimeException (unchecked exception), so the user is not forced to handle this exception. Normally
 * the developer can not help that a service of an external API is not available, so we do not want to bother the
 * developer with a checked exception.
 */
public class ServiceNotAvailableException extends RuntimeException
{
    private static final String DEFAULT_MESSAGE = "Unable to use the service '%s'. Cause: %s";

    /**
     * Constructor based on class and message
     * @param service
     * @param cause
     */
    public ServiceNotAvailableException(final Class service, final String cause)
    {
        super(createMessage(service, cause));
    }

    /**
     * Constructor pased on class, message and the original thowable
     * @param service
     * @param cause
     * @param throwable
     */
    public ServiceNotAvailableException(final Class service, final String cause, final Throwable throwable)
    {
        super(createMessage(service, cause), throwable);
    }

    private static String createMessage(final Class service, final String cause)
    {
        return String.format(
                DEFAULT_MESSAGE,
                service.getName(),
                cause);
    }
}
