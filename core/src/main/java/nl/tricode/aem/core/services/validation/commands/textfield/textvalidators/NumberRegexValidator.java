package nl.tricode.aem.core.services.validation.commands.textfield.textvalidators;

import nl.tricode.aem.core.services.validation.ValidationConstants;
import nl.tricode.aem.core.services.validation.models.ValidationStatusTypes;
import nl.tricode.aem.core.services.validation.models.ValidatorStatus;
import org.apache.sling.api.resource.Resource;

/**
 *
 */
public class NumberRegexValidator implements RegexValidator {

    @Override
    public String getRegex(final Resource resource) {
        return ValidationConstants.NUMBER_REGEX;
    }

    @Override
    public ValidatorStatus getFalseValidatorStatus(final Resource resource) {
        return new ValidatorStatus(ValidationStatusTypes.FIELD_NOT_NUMBER.getStatusCode(), ValidationStatusTypes.FIELD_NOT_NUMBER.getDescription(), resource);
    }
}
