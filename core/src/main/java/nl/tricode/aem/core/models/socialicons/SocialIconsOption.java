package nl.tricode.aem.core.models.socialicons;

/**
 * Class to be used to populate the list of social icons
 */
public class SocialIconsOption {

    private String title;
    private String link;
    private String logoType;

    public SocialIconsOption() {
    }

    public SocialIconsOption(String title, String link, String logoType) {
        this.title = title;
        this.link = link;
        this.logoType = logoType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLogoType() {
        return logoType;
    }

    public void setLogoType(String logoType) {
        this.logoType = logoType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SocialIconsOption)) {
            return false;
        }

        SocialIconsOption that = (SocialIconsOption) o;

        if (link != null ? !link.equals(that.link) : that.link != null) {
            return false;
        }
        if (logoType != null ? !logoType.equals(that.logoType) : that.logoType != null) {
            return false;
        }
        if (title != null ? !title.equals(that.title) : that.title != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (link != null ? link.hashCode() : 0);
        result = 31 * result + (logoType != null ? logoType.hashCode() : 0);
        return result;
    }
}
