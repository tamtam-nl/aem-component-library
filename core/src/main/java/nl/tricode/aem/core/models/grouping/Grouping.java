package nl.tricode.aem.core.models.grouping;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;

import javax.inject.Inject;

/**
 * Sling models grouping component.
 */
@Model(adaptables = {SlingHttpServletRequest.class})
public class Grouping {

    /**
     * Background color selected from the background color dropdown
     */
    @Inject
    @Optional
    @Via("resource")
    private String backgroundColor;

    /**
     * Background color selected from the background color dropdown
     */
    @Inject
    @Optional
    @Via("resource")
    private String spacing;
    /**
     * Value from the cut edge checkbox. If checked the value is true
     */
    @Inject
    @Optional
    @Via("resource")
    private String cutEdge;



    @Inject
    private SlingHttpServletRequest request;

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public String getSpacing() {
        return spacing;
    }

    public String getCutEdge() {
        return cutEdge;
    }
}