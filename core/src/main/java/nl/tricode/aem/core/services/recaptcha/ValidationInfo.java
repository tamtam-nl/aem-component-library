package nl.tricode.aem.core.services.recaptcha;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import com.day.cq.wcm.foundation.forms.FormsHelper;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ValidationInfo implements Serializable{

    private static final Logger LOG = LoggerFactory.getLogger(ValidationInfo.class);
    /** Constants for formfield properties */
    public static final String PROPERTY_REQUIRED = "required";
    public static final String PROPERTY_REQUIRED_MESSAGE = "requiredMessage";

    public static final String PROPERTY_CONSTRAINT_TYPE = "constraintType";
    public static final String PROPERTY_CONSTRAINT_MESSAGE = "constraintMessage";

    /** Constant for global validation messages. */
    private static final String GLOBAL = "";

    /** The map containing all error messages. */
    private final Map<String, String[]> messages;

    /** The map containing all values */
    private final Map<String, String[]> values;

    /** The resource bundle. */
    private final transient ResourceBundle resBundle;

    /** The request attribute caching the validation info. */
    private static final String REQ_ATTR = ValidationInfo.class.getName();

    /**
     * Construct a new validation info. The validation info stores the form id,
     * all content request parameters and their values together with possible
     * validation messages.
     *
     * @param request
     *            The current request.
     */
    public ValidationInfo(final HttpServletRequest request) {
        SlingHttpServletRequest slingRequest = (SlingHttpServletRequest)request;
        this.values = new HashMap<>();
        final Iterator<String> nameIter = FormsHelper.getContentRequestParameterNames(slingRequest);
        while (nameIter.hasNext()) {
            final String name = nameIter.next();
            final RequestParameter reqParam = slingRequest.getRequestParameter(name);
            if (reqParam != null && reqParam.isFormField()) {
                final String[] parameterValues = slingRequest.getParameterValues(name);
                if (parameterValues != null && parameterValues.length > 0) {
                    this.values.put(name, parameterValues);
                }
            }
        }
        this.messages = new HashMap<>();
        Locale locale = FormsHelper.getLocale(slingRequest);
        this.resBundle = slingRequest.getResourceBundle(locale);

        slingRequest.getSession().setAttribute(REQ_ATTR, this);
    }

    /**
     * Return whether a field has multiple values.
     *
     * @param field
     *            The name of the field.
     * @return <code>true</code> when the field has multiple values
     */
    public boolean isMultiValue(String field) {
        String[] fieldValues = getValues(field);
        return fieldValues != null && fieldValues.length > 1;
    }

    /**
     * Return all values for a field.
     *
     * @param field
     *            The name of the field.
     * @return The array of values or <code>null</code>
     */
    public String[] getValues(String field) {
        return this.values.get(field);
    }

    /**
     * Return a single value for a field.
     *
     * @param field
     *            The name of the field.
     * @return A single value or <code>null</code> if there are no values
     * @throws IllegalStateException
     *             when the field has multiple values
     */
    public String getValue(String field) {
        String[] fieldValues = getValues(field);
        if (fieldValues != null) {
            if (fieldValues.length == 0) {
                return null;
            } else if (fieldValues.length == 1) {
                return fieldValues[0];
            } else {
                throw new IllegalStateException("Field has multiple values");
            }
        } else {
            return null;
        }
    }

    /**
     * Add the error message for the field.
     *
     * @param field
     *            The name of the field or <code>null</code>
     * @param msg
     *            The error message.
     */
    public void addErrorMessage(String field, String msg) {
        if (this.resBundle == null) {
            throw new IllegalStateException(
                    "Validation info is not mutable. The ResourceBundle of the request is not defined for the request locale.");
        }
        String fieldName;
        if (field == null) {
            fieldName = GLOBAL;
        }else {
            fieldName = field;
        }
        // localize msg
        String errorMsg;
        errorMsg = this.resBundle.getString(msg);

        String[] msgs = this.messages.get(fieldName);
        if (msgs == null) {
            this.messages.put(fieldName, new String[] { errorMsg });
        } else {
            String[] newMsgs = new String[msgs.length + 1];
            System.arraycopy(msgs, 0, newMsgs, 0, msgs.length);
            newMsgs[msgs.length] = errorMsg;
            this.messages.put(fieldName, newMsgs);
        }
    }

    /**
     * Return true if an error was reported
     *
     * @return <code>true</code> or <code>false</code>
     */
    public boolean hasErrorMessages() {
        return !this.messages.isEmpty();
    }

    /**
     * Return all error messages for this parameter.
     *
     * @param field
     *            Parameter name or <code>null</code> to get global errors.
     * @return <code>null</code> if there are no messages
     */
    public String[] getErrorMessages(String field) {
        String fieldName;
        if (field == null) {
            fieldName = GLOBAL;
        }else {
            fieldName = field;
        }
        return this.messages.get(fieldName);
    }

    /**
     * Return all error messages for this parameter.
     *
     * @param field
     *            Parameter name or <code>null</code> to get global errors.
     * @return <code>null</code> if there are no messages
     * @since 5.3
     */
    public String[] getErrorMessages(String field, final int valueIndex) {
        String fieldName;
        if (field == null) {
            fieldName = GLOBAL;
        }else {
            fieldName = field;
        }
        return this.messages.get(fieldName + ':' + valueIndex);
    }

    /**
     * Return the validation information for a request
     *
     * @param request
     *            The current request.
     * @return The validation info for this request or <code>null</null>
     */
    public static ValidationInfo getValidationInfo(final HttpServletRequest request) {
        ValidationInfo validationInfo = null;

        try {
            validationInfo = (ValidationInfo) request.getSession().getAttribute(REQ_ATTR);
        } catch (final ClassCastException classCastException) {
            LOG.warn(
                    String.format(
                            "Class cast exception. Cause: %s",
                            classCastException.getMessage()
                    ),
                    classCastException);

            return null;
        }

        if (validationInfo == null)
            validationInfo = new ValidationInfo(request);

        return validationInfo;
    }

    public void clear() {
        this.values.clear();
        this.messages.clear();
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder(ValidationInfo.class.getSimpleName() + "{");
        StringBuilder m = new StringBuilder();
        for (Map.Entry<String, String[]> entry : messages.entrySet()) {
            if (m.length() > 0) {
                m.append(",");
            }
            m.append(entry.getKey()).append("=");
            String[] entryValues = entry.getValue();
            if (entryValues != null) {
                m.append("[" + entryValues[0]);
                for (int i = 1; i < entryValues.length; i++) {
                    m.append(",");
                    m.append(entryValues[i]);
                }
                m.append("]");
            } else {
                m.append("null");
            }
        }
        b.append("messages={").append(m).append("}");
        return b.append("}").toString();
    }
}
