package nl.tricode.aem.core.services.migration;

import org.apache.felix.scr.annotations.Component;
import org.osgi.framework.Version;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

/**
 * Migration script to change the social share component with a social icon component in the header of the base template
 * and the search results template
 */
@Component(enabled = true)
public class SocialShareSocialIconsMigration extends AbstractMigration {

    /**
     * Content path.
     */
    public static final String CONTENT_PATH = "/content";

    /**
     * Number of migrated nodes.
     */
    private int numOfNodesModified;

    /**
     * Array of old resource types.
     */
    private static final String[] resourceTypes = new String[]{
            "acl/components/structure/pages/base-template-page",
            "acl/components/structure/pages/searchresult"
    };

    @Override
    void doMigration() {
        migrateNodes(getNodesWithResourceTypesInPath(resourceTypes, CONTENT_PATH));
        getLogger().info("Total nodes migrated : {}", numOfNodesModified);
    }

    @Override
    void migrateNode(final Node node) throws RepositoryException {
        if (node != null) {
            final Node headerNode = node.getNode("header");
            if(headerNode != null && headerNode.hasNode("socialshare")){
                headerNode.getNode("socialshare").remove();
                headerNode.addNode("socialicons");
                headerNode.getNode("socialicons").setProperty("sling:resourceType", "acl/components/structure/socialicons");
                updateNodeVersion(headerNode);
                updatePageModified(node);
                ++numOfNodesModified;
            }
        }
    }

    @Override
    Version getLatestVersion() {
        return new Version("1.0.0");
    }
}