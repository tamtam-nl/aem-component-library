package nl.tricode.aem.core.utils;

import nl.tricode.aem.core.utils.exceptions.ServiceNotAvailableException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Utility class containing methods for AEM service related actions such as retrieving an instance of the SlingSettingsService.
 * This class is not final due to a test case, according to NFa.
 */
public class OSGIServiceUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(OSGIServiceUtil.class);

    // Private constructor to prevent instantiating
    private OSGIServiceUtil() {
    }

    /**
     * Get list of services based on the given interface
     *
     * @param osgiServiceInterface the interface
     * @return list of services, can be empty but not null
     */
    public static <T> List<T> getOSGIServices(final Class<T> osgiServiceInterface) {
        return getOSGIServices(osgiServiceInterface, null);
    }

    /**
     * Get list of services based on the given interface and matching the given filter.
     * For the filter syntax see https://osgi.org/javadoc/r4v43/core/org/osgi/framework/BundleContext.html#getAllServiceReferences(java.lang.String,%20java.lang.String)
     *
     * @param osgiServiceInterface the interface
     * @param filter               the filter to apply
     * @return list of services, can be empty but not null
     */
    public static <T> List<T> getOSGIServices(final Class<T> osgiServiceInterface, final String filter) {
        final BundleContext bundleContext = getBundleContext(osgiServiceInterface);
        try {
            if (bundleContext != null) {
                final ServiceReference[] references = bundleContext.getAllServiceReferences(osgiServiceInterface.getName(), filter);
                if (references != null) {
                    return populateServices(osgiServiceInterface, bundleContext, references);
                }
            }
        } catch (InvalidSyntaxException e) {
            throw new ServiceNotAvailableException(osgiServiceInterface, "Invalid syntax, cause:" + e);
        }

        return new ArrayList<>();
    }

    private static <T> List<T> populateServices(Class<T> osgiServiceInterface, BundleContext bundleContext, ServiceReference[] references) {
        final List<T> services = new ArrayList<>();
        for (ServiceReference reference : references) {
            final Object service = bundleContext.getService(reference);
            if (service != null && osgiServiceInterface.isInstance(service)) {
                services.add((T) service);
            }
        }
        return services;
    }

    /**
     * Get service instance by class name
     *
     * @param osgiServiceClass the interface to search for
     * @return the service reference or null
     */
    public static <T> T getOSGIService(final Class<T> osgiServiceClass) {
        final BundleContext bundleContext = getBundleContext(osgiServiceClass);
        if (bundleContext == null) {
            LOGGER.warn("Unable to get an instance of the class. Cause: the mandatory parameter 'osgiServiceClass' is NULL.");
            return null;
        }

        final ServiceReference serviceReference = bundleContext.getServiceReference(osgiServiceClass.getName());
        if (serviceReference == null) {
            LOGGER.warn(String.format(
                    "Unable to get an instance of the '%s'. Cause: a NULL reference was returned for the ServiceReference class for %s.",
                    osgiServiceClass.getName(),
                    SlingSettingsService.class.getName()));

            return null;
        }

        final Object service = bundleContext.getService(serviceReference);
        try {
            return osgiServiceClass.cast(service);
        } catch (final ClassCastException classCastException) {
            LOGGER.warn(
                    String.format(
                            "Unable to get an instance of the SlingSettingsService. Cause: %s",
                            classCastException.getMessage()
                    ),
                    classCastException);

            return null;
        }
    }

    private static <T> BundleContext getBundleContext(final Class<T> osgiServiceClass) {
        if (osgiServiceClass == null) {
            LOGGER.warn("Unable to get an instance of the class. Cause: the mandatory parameter 'osgiServiceClass' is NULL.");
            return null;
        }

        final Bundle bundle = FrameworkUtil.getBundle(Resource.class);
        if (bundle == null) {
            LOGGER.warn(String.format("Unable to get an instance of the '%s'. Cause: a NULL reference was returned for the Bundle class.", osgiServiceClass.getName()));

            return null;
        }

        final BundleContext bundleContext = bundle.getBundleContext();
        if (bundleContext == null) {
            LOGGER.warn(String.format("Unable to get an instance of the '%s'. Cause: a NULL reference was returned for the BundleContext class.", osgiServiceClass.getName()));

            return null;
        }
        return bundleContext;
    }
}
