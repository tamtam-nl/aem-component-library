package nl.tricode.aem.core.models.page;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.commons.WCMUtils;
import nl.tricode.aem.core.utils.LinkUtil;
import nl.tricode.aem.core.utils.PathUtil;
import nl.tricode.aem.core.utils.ResourceUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Head model for page component
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class Head {

    @Inject
    @Optional
    @Via("resource")
    private String canonicalLink;

    @Inject
    @Optional
    @Via("resource")
    @Named("jcr:title")
    private String title;

    @Inject
    @Optional
    @Via("resource")
    @Named("jcr:description")
    private String description;

    @Inject
    @Optional
    @Via("resource")
    private String enableRss;

    @Inject
    @Optional
    @Via("resource")
    private String rssRootPage;

    @Inject
    Page currentPage;

    @Inject
    Design currentDesign;

    @Self
    private SlingHttpServletRequest request;

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }

    public String getCanonicalLink() {
        if (StringUtils.isEmpty(canonicalLink)) {
            return LinkUtil.pathBrowserPropertyToAbsoluteLink(request, ResourceUtil.getResourcePage(request.getResource()).getPath());
        }
        // externalize it if needed. it might be a path only
        return LinkUtil.pathBrowserPropertyToAbsoluteLink(request, canonicalLink);
    }

    public String getKeywords(){
        return WCMUtils.getKeywords(currentPage);
    }
    public String getUrl(){
        return PathUtil.getUrl(request);
    }
    public String getImageUrl(){
        return PathUtil.getUrl(request).replace(".html","")+".thumb.png";
    }

    public Resource getFavIcon(){
        return request.getResourceResolver().getResource(currentDesign.getPath() + "/favicon.ico");
    }

    public String getEnableRss() {
        return enableRss;
    }

    public String getRssRootPage() {
        return rssRootPage;
    }
}
