package nl.tricode.aem.core.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

/**
 * Utility class for quering the repository.
 */
public class QueryUtil {

    private static final Logger LOG = LoggerFactory.getLogger(QueryUtil.class);

    private QueryUtil() {}

    /**
     * Builds a query from the passed query string and the passed session
     *
     * @param queryString
     * @param session
     * @return the query
     */
    private static Query buildQuery(final String queryString, final Session session) {

        Query query = null;
        try {
            final QueryManager queryManager = session.getWorkspace().getQueryManager();
            query = queryManager.createQuery(queryString, Query.JCR_SQL2);
        } catch (final RepositoryException e) {
            LOG.error("Failed to create query: {}", queryString, e);
        }

        return query;
    }

    /**
     * Returns node iterator result for SQL2 query
     *
     * @param queryString
     * @param session
     * @return
     */
    public static NodeIterator getResultNodeIteratorForQuery(final String queryString, final Session session) {
        final Query query = buildQuery(queryString, session);

        if (query != null) {
            try {
                final QueryResult result = query.execute();
                return result.getNodes();
            } catch (final RepositoryException e) {
                LOG.error("Failed to execute query", e);
            }
        }

        return null;
    }
}
