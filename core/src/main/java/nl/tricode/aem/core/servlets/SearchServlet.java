package nl.tricode.aem.core.servlets;

import com.day.cq.search.suggest.Suggester;
import com.day.cq.search.suggest.SuggestionIndexManager;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.jcr.api.SlingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Session;

@SlingServlet(
        label = "Search Servlet",
        metatype = true,
        methods = {"GET"},
        paths = {"/apps/acl/SearchServlet"},
        name = " nl.tricode.aem.core.servlets.SearchServlet"
)
public class SearchServlet extends SlingAllMethodsServlet {

    private static final Logger LOG = LoggerFactory.getLogger(SearchServlet.class);

    @Reference
    protected transient SlingRepository repository;
    @Reference
    private transient SuggestionIndexManager suggestionIndexManager;
    @Reference
    private transient Suggester suggester;

    @Override
    public void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        LOG.error("SearchServlet : ");
        response.setContentType("application/json");

        try {
            String term = request.getParameter("searchQuery");
            LOG.error("Data from ajax call " + term);

            JSONArray resultsArray = new JSONArray();

            Session session = request.getResourceResolver().adaptTo(Session.class);
            if (session != null) {
                String[] list = suggester.getSuggestions(session, "oak-cq:Page", term, true);
                for (String entry : list) {
                    LOG.error("entry= " + entry);
                    JSONObject sugegstion = new JSONObject();
                    sugegstion.put("item", entry);
                    resultsArray.put(sugegstion);
                }
                response.getWriter().write(resultsArray.toString());
                String redirectPage = request.getParameter(":redirect");
                LOG.error("redirectPage:" + redirectPage);
                session.logout();
            }
        } catch (Exception e) {
            LOG.error("Error:" + e);
        }
    }
}