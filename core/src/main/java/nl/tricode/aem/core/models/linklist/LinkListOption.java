package nl.tricode.aem.core.models.linklist;

/**
 * Class to be used to populate the list of links
 */
public class LinkListOption {

    private String text;
    private String title;
    private String link;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
