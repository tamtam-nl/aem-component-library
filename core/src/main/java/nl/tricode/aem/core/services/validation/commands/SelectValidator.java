package nl.tricode.aem.core.services.validation.commands;

import nl.tricode.aem.core.ACLConstants;
import nl.tricode.aem.core.services.validation.commands.utils.ValidationUtils;
import nl.tricode.aem.core.services.validation.models.ValidatorStatus;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.Resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Validator for select (dropdown) component
 */
public class SelectValidator  extends FieldValidator {

    @Override
    public List<ValidatorStatus> validate(String[] values, Resource resource, Map<String, RequestParameter[]> requestParameters) {
        List<ValidatorStatus> validatorStatusList = new ArrayList<>();

        validatorStatusList.add(validateRequired(values, resource));
        validatorStatusList.add(ValidationUtils.validateRightValuesSubmitted(values, resource, ACLConstants.SELECTABLE_ITEMS_PROPERTY));

        return validatorStatusList;
    }
}
