package nl.tricode.aem.core.models.divider;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;

import javax.inject.Inject;

/**
 * Model for Divider component
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class DividerModel {

    /**
     * Value for the height of the divider
     */
    @Inject
    @Optional
    @Via("resource")
    private String height;

    /**
     * Value for the style of the divider
     */
    @Inject
    @Optional
    @Via("resource")
    private String style;

    public String getHeight() {
        return height;
    }

    public String getStyle() {
        return style;
    }
}
