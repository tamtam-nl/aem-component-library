package nl.tricode.aem.core.servlets.image;

import com.day.cq.wcm.commons.AbstractImageServlet;
import com.day.cq.wcm.foundation.AdaptiveImageHelper;
import com.day.cq.wcm.foundation.Image;
import com.day.image.Layer;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.osgi.OsgiUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Iterator;
import java.util.List;

/**
 * Adaptive image servlet. This is a copy of the code found in the repository at /libs/foundation/src/impl/src/com/day/cq/wcm/foundation/impl/AdaptiveImageComponentServlet.java
 * Because its in the repo, we cannot extend the existing class 
 */
@Component(metatype = true, label = "Adaptive Image Component Servlet",
        description = "Render adaptive images in a variety of qualities (suplements ootb component)")
@Service
@Properties(value = {
        @Property(name = "sling.servlet.resourceTypes", value = "acl/components/content/image", propertyPrivate = true),
        @Property(name = "sling.servlet.selectors", value = "img", propertyPrivate = true),
        @Property(name = "sling.servlet.extensions", value = {
                "jpg",
                "jpeg",
                "png",
                "gif"
        }, propertyPrivate = true)
})
public class AclAdaptiveImageServlet extends AbstractImageServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(AclAdaptiveImageServlet.class);

    // Selector to indicate that we should not scale the image at all, only adjust the quality.
    private static final String FULL_SIZE_SELECTOR = "full";

    @Property(value = {
            "270", // 2 col
            "370", // 3 col
            "570", // 6 col
            "1140" // 12 col
    },
            label = "Supported Widths",
            description = "List of widths this component is permitted to generate.")
    private static final String PROPERTY_SUPPORTED_WIDTHS = "adapt.supported.widths";
    private List<String> supportedWidths;

    protected void activate(ComponentContext componentContext) {
        Dictionary<String, Object> properties = componentContext.getProperties();

        supportedWidths = new ArrayList<>();
        String[] supportedWidthsArray = OsgiUtil.toStringArray(properties.get(PROPERTY_SUPPORTED_WIDTHS));
        if (supportedWidthsArray != null && supportedWidthsArray.length > 0) {
            for (String width : supportedWidthsArray) {
                supportedWidths.add(width);
            }
        }
    }

    @Override
    protected Layer createLayer(ImageContext imageContext) throws RepositoryException, IOException {
        SlingHttpServletRequest request = imageContext.request;
        String [] selectors = request.getRequestPathInfo().getSelectors();
        // We expect exactly 3 selectors OR only 1
        if (selectors.length != 3 && selectors.length != 1) {
            LOGGER.error("Unsupported number of selectors.");
            return null;
        }

        // selectors: [1] width, [2] quality
        String widthSelector = FULL_SIZE_SELECTOR;
        if (selectors.length == 3) {
            widthSelector = selectors[1];
        }
        // Ensure this is one of our supported widths
        if (!isDimensionSupported(widthSelector)) {
            LOGGER.error("Unsupported width requested: {}.", widthSelector);
            return null;
        }

        Image image = new Image(imageContext.resource);
        // If this image does not have a valid file reference OR image child, return an empty layer
        if (!image.hasContent()) {
            LOGGER.error("The image associated with this page does not have a valid file reference; drawing a placeholder.");
            // This should never occur - in author mode we render a placeholder in absence of an image.
            return null;
        }

        AdaptiveImageHelper adaptiveHelper = new AdaptiveImageHelper();

        if (FULL_SIZE_SELECTOR.equals(widthSelector)) {
            // No scaling is necessary in this case
            return adaptiveHelper.applyStyleDataToImage(image, imageContext.style);
        }
        // else
        return adaptiveHelper.scaleThisImage(image, Integer.parseInt(widthSelector), 0, imageContext.style);
    }

    /**
     * Query if this servlet has been configured to render images of the given width.
     * This method could be overridden to always return true in the case where any dimension
     * combination is permitted.
     * @param widthStr     Width of the image to render, or "full"
     * @return          true if the given dimensions are supported, false otherwise
     */
    protected boolean isDimensionSupported(String widthStr) {
        Iterator<String> iterator = getSupportedWidthsIterator();
        if (FULL_SIZE_SELECTOR.equals(widthStr)) {
            return true;
        }
        int width = Integer.parseInt(widthStr);
        while (iterator.hasNext()) {
            if (width == (Integer.parseInt(iterator.next()))) {
                return true;
            }
        }

        return false;
    }

    /**
     * An iterator to the collection of widths this servlet is configured to render.
     * @return  Iterator
     */
    protected Iterator<String> getSupportedWidthsIterator() {
        return supportedWidths.iterator();
    }

    @Override
    protected void writeLayer(SlingHttpServletRequest request, SlingHttpServletResponse response, ImageContext context, Layer layer) throws IOException, RepositoryException {
        double quality;
        // If the quality selector exists, use it
        String [] selectors = request.getRequestPathInfo().getSelectors();
        if (selectors.length == 3) {
            String imageQualitySelector = selectors[2];
            quality = getRequestedImageQuality(imageQualitySelector);
        } else {
            // If the quality selector does not exist, fall back to the default
            quality = getImageQuality();
        }

        writeLayer(request, response, context, layer, quality);
    }

    private double getRequestedImageQuality(String imageQualitySelector) {
        // If imageQualitySelector is not a valid Quality, fall back to teh default
        AdaptiveImageHelper.Quality newQuality = AdaptiveImageHelper.getQualityFromString(imageQualitySelector);
        if (newQuality != null ) {
            return newQuality.getQualityValue();
        }
        // Fall back to the defaut
        return getImageQuality();
    }

    @Override
    protected String getImageType() {
        return "image/jpeg";
    }
}