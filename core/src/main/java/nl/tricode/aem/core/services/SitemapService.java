package nl.tricode.aem.core.services;

import com.day.cq.wcm.api.Page;
import nl.tricode.aem.core.models.sitemap.SitemapElement;
import org.apache.sling.api.SlingHttpServletRequest;

import java.util.List;

/**
 * Service for finding elements to be set in the sitemap
 */
@FunctionalInterface
public interface SitemapService {

    /**
     * Lists sitemap elements. The elements are populated from current page parent.
     *
     * @param request
     * @param currentPage
     * @return list sitemap element
     */
    List<SitemapElement> getListOfSitemapElements(SlingHttpServletRequest request, Page currentPage);
}
