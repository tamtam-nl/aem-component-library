package nl.tricode.aem.core.models.genericdatasource;

import com.adobe.granite.ui.components.ds.DataSource;
import com.adobe.granite.ui.components.ds.EmptyDataSource;
import com.adobe.granite.ui.components.ds.SimpleDataSource;
import com.adobe.granite.ui.components.ds.ValueMapResource;
import com.day.cq.wcm.api.designer.Designer;
import com.day.cq.wcm.api.designer.Style;
import nl.tricode.aem.core.utils.DesignerUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceMetadata;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.wrappers.ValueMapDecorator;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;

@Model(adaptables = SlingHttpServletRequest.class)
public class GenericDataSource {

    @Self
    private SlingHttpServletRequest request;

    @Inject
    private Designer designer;

    protected void loadCustomDataSource(String designNodeName) {
        ResourceResolver resourceResolver = request.getResourceResolver();
        final Style contentStyle = DesignerUtil.getContentStyle(designer, request.getRequestPathInfo().getSuffix(), resourceResolver);
        String designOptions = "";
        if (contentStyle != null && !contentStyle.isEmpty()) {
            designOptions = contentStyle.get(designNodeName, String.class);
            final ArrayList<Resource> resourceList = getOptions(resourceResolver, designOptions);
            //put the resource list in the request for the datasource mechanism to pick it up
            request.setAttribute(DataSource.class.getName(), new SimpleDataSource(resourceList.iterator()));
        }
        if (StringUtils.isEmpty(designOptions)) {
            request.setAttribute(DataSource.class.getName(), EmptyDataSource.instance());
        }
    }

    private static ArrayList<Resource> getOptions(final ResourceResolver resourceResolver, final String designOptions) {
        final ArrayList<Resource> resourceList = new ArrayList<>();
        final String[] options = designOptions.split("\n");

        for (final String option : options) {
            final String[] tokenizedOption = option.split("\t");
            final HashMap map = new HashMap();
            map.put("value", tokenizedOption[0].trim());
            map.put("text", tokenizedOption[1]);
            final ValueMapResource valMapResource = new ValueMapResource(resourceResolver, new ResourceMetadata(), "",
                    new ValueMapDecorator(map));
            resourceList.add(valMapResource);
        }
        return resourceList;
    }


}
