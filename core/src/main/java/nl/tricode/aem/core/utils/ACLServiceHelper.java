package nl.tricode.aem.core.utils;

import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;

public class ACLServiceHelper {

    private ACLServiceHelper(){}

    @SuppressWarnings("unchecked")
    public static <T> T getService(Class<T> source) {
        final BundleContext bundleContext = FrameworkUtil.getBundle(ACLServiceHelper.class).getBundleContext();
        return (T) bundleContext.getService(bundleContext.getServiceReference(source.getName()));
    }
}
