'use strict';

$(document).ready(function() {
    $('.btn-open-modal').each(function( index ) {
        acl['modal' + index] = new acl.Modal( this );
    });
});


window.acl = window.acl || {};

acl.Modal = function( element ) {
    this.$openButton        = $(element);
    this.$modal             = $( this.$openButton.data('target') );
    this.$modalDialog       = this.$modal.find('.modal-dialog');
    this.$dismissButtons    = this.$modal.find('[data-dismiss="modal"]');
    this.$body              = $('body');

    this.$openButton.click( this.openModal.bind(this) );
};

acl.Modal.prototype.openModal = function() {
    this.setScrollbar();
    var $backdrop = $("<div class='modal-backdrop'></div>");
    this.$body.addClass('modal-open').append($backdrop);

    this.$modal.show();

    $backdrop[0].offsetWidth // force reflow

    this.$modal.addClass('fadeIn');
    $backdrop.addClass('fadeIn');

    var modalObject = this;
    this.$modal.on("click", function(e) {
        if( e.target === e.currentTarget ) {
            modalObject.closeModal();
        }
    });

    this.$dismissButtons.on("click", this.closeModal.bind(this) );
};

acl.Modal.prototype.closeModal = function() {
    this.$dismissButtons.off();
    this.$modal.off();

    $(".modal-backdrop").on('webkitTransitionEnd otransitionend msTransitionEnd transitionend', function() {
        $(this).remove();
        $(this).off();
    }).removeClass('fadeIn');

    var modalObject = this;
    this.$modalDialog.on('webkitTransitionEnd otransitionend msTransitionEnd transitionend', function() {
        modalObject.$modal.hide();
        $(this).off();
    });
    this.$modal.removeClass('fadeIn');

    this.resetScrollbar();
    this.$body.removeClass('modal-open');
};

acl.Modal.prototype.setScrollbar = function () {
    this.checkScrollbar();
    if (this.bodyIsOverflowing) {
        var bodyPad = parseInt((this.$body.css('padding-right') || 0), 10);
        this.originalBodyPad = document.body.style.paddingRight || '';
        this.$body.css('padding-right', bodyPad + this.scrollbarWidth)
    }
};

acl.Modal.prototype.checkScrollbar = function () {
    var fullWindowWidth = window.innerWidth;
    if (!fullWindowWidth) { // workaround for missing window.innerWidth in IE8
        var documentElementRect = document.documentElement.getBoundingClientRect();
        fullWindowWidth = documentElementRect.right - Math.abs(documentElementRect.left);
    }
    this.bodyIsOverflowing = document.body.clientWidth < fullWindowWidth;
    this.scrollbarWidth = this.measureScrollbar();
};

/* Create a little div with scrollbar offscreen to measure the scrollbar width (thanks to David Walsh). */
acl.Modal.prototype.measureScrollbar = function () {
    var scrollDiv = document.createElement('div');
    scrollDiv.className = 'modal-scrollbar-measure';
    this.$body.append(scrollDiv);
    var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
    this.$body[0].removeChild(scrollDiv);
    return scrollbarWidth;
};

acl.Modal.prototype.resetScrollbar = function () {
    this.$body.css('padding-right', this.originalBodyPad);
};