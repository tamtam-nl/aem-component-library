$(".collapsible-trigger").click(function () {
    $(this).parent().find('.pull-right').toggleClass('open');
    $(this).parent().find('.pull-left').toggleClass('open');
    $(this).parent().next().slideToggle(300);
});