function Datepicker(id, target, format, minDate, maxDate) {
    this.$id = $('#' + id); // element to attach widget to
    this.$monthObj = this.$id.find('#month');
    this.$prev = this.$id.find('#bn_prev');
    this.$next = this.$id.find('#bn_next');
    this.$grid = this.$id.find('#cal');
    this.$target = $('#' + target); // div or text box that will receive the selected date string and focus
    this.monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'];
    this.dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    this.format = format;
    if (minDate instanceof Date) {
        this.minDate = minDate;
    } else {
        this.minDate = new Date(minDate);
        this.minDate.setHours(0, 0, 0);
    }
    if (maxDate instanceof Date) {
        this.maxDate = maxDate;
    } else {
        this.maxDate = new Date(maxDate);
        this.maxDate.setHours(0, 0, 0);
    }
    this.dateObj = new Date();
    this.curYear = this.dateObj.getFullYear();
    this.year = this.curYear;
    this.curMonth = this.dateObj.getMonth();
    this.month = this.curMonth;
    this.currentDate = true;
    this.date = this.dateObj.getDate();
    this.visible = false;
    this.keys = {
        tab: 9,
        enter: 13,
        esc: 27,
        space: 32,
        pageup: 33,
        pagedown: 34,
        end: 35,
        home: 36,
        left: 37,
        up: 38,
        right: 39,
        down: 40
    };
    // display the current month
    this.$monthObj.html(this.monthNames[this.month] + ' ' + this.year);
    // populate the calendar grid
    this.popGrid();
    // update the table's activedescdendant to point to the current day
    this.$grid.attr('aria-activedescendant', this.$grid.find('.today').attr('id'));
    this.bindHandlers();
}

// popGrid() is a member function to populate the Datepicker grid with calendar days
// representing the current month
//
// @return N/A
//
Datepicker.prototype.popGrid = function () {
    var numDays = this.calcNumDays(this.year, this.month);
    var startWeekday = this.calcStartWeekday(this.year, this.month);
    var weekday;
    var curDay;
    var rowCount = 1;
    var $tbody = this.$grid.find('tbody');
    var gridCells = '\t<tr id="row1">\n';
    // clear the grid
    $tbody.empty();
    $('#msg').empty();
    // Insert the leading empty cells
    for (weekday = 0; weekday < startWeekday; weekday++) {
        gridCells += '\t\t<td class="empty">&nbsp;</td>\n';
    }
    // insert the days of the month.
    for (curDay = 1; curDay <= numDays; curDay++) {
        var displayedDate = new Date(this.year + "-" + (this.month + 1) + "-" + curDay);
        var disabled = displayedDate < this.minDate || displayedDate > this.maxDate;
        var today = curDay == this.date && this.currentDate == true;
        gridCells += '\t\t<td id="day' + curDay + '" class="';
        if (today) {
            gridCells += 'today ';
        }
        if (disabled) {
            gridCells += 'disabled';
        }
        gridCells += '" headers="row' + rowCount + ' ' + this.dayNames[weekday] + '" role="gridcell" aria-selected="false">' + curDay + '</td>';
        if (weekday == 6 && curDay < numDays) {
            // This was the last day of the week, close it out
            // and begin a new one
            gridCells += '\t</tr>\n\t<tr id="row' + rowCount + '">\n';
            rowCount++;
            weekday = 0;
        }
        else {
            weekday++;
        }
    }

    // Insert any trailing empty cells
    for (weekday; weekday < 7; weekday++) {
        gridCells += '\t\t<td class="empty">&nbsp;</td>\n';
    }
    gridCells += '\t</tr>';
    $tbody.append(gridCells);
}

// calcNumDays() is a member function to calculate the number of days in a given month
//
// @return (integer) number of days
//
Datepicker.prototype.calcNumDays = function (year, month) {
    return 32 - new Date(year, month, 32).getDate();
}

// calcstartWeekday() is a member function to calculate the day of the week the first day of a
// month lands on
//
// @return (integer) number representing the day of the week (0=Sunday....6=Saturday)
//
Datepicker.prototype.calcStartWeekday = function (year, month) {
    return  new Date(year, month, 1).getDay();
}


// showPrevMonth() is a member function to show the previous month
// @param (offset int) offset may be used to specify an offset for setting
//                      focus on a day the specified number of days from
//                      the end of the month.
// @return N/A
//
Datepicker.prototype.showPrevMonth = function (offset) {
    // show the previous month
    if (this.month == 0) {
        this.month = 11;
        this.year--;
    }
    else {
        this.month--;
    }
    if (this.month != this.curMonth || this.year != this.curYear) {
        this.currentDate = false;
    }
    else {
        this.currentDate = true;
    }
    // populate the calendar grid
    this.popGrid();
    this.$monthObj.html(this.monthNames[this.month] + ' ' + this.year);
    // if offset was specified, set focus on the last day - specified offset
    if (offset != null) {
        var numDays = this.calcNumDays(this.year, this.month);
        var day = 'day' + (numDays - offset);
        this.$grid.attr('aria-activedescendant', day);
        $('#' + day).addClass('focus').attr('aria-selected', 'true');
    }

}

// showNextMonth() is a member function to show the next month
//
// @param (offset int) offset may be used to specify an offset for setting
//                      focus on a day the specified number of days from
//                      the beginning of the month.
// @return N/A
//
Datepicker.prototype.showNextMonth = function (offset) {
    // show the next month
    if (this.month == 11) {
        this.month = 0;
        this.year++;
    }
    else {
        this.month++;
    }
    if (this.month != this.curMonth || this.year != this.curYear) {
        this.currentDate = false;
    }
    else {
        this.currentDate = true;
    }
    // populate the calendar grid
    this.popGrid();
    this.$monthObj.html(this.monthNames[this.month] + ' ' + this.year);
    // if offset was specified, set focus on the first day + specified offset
    if (offset != null) {
        var day = 'day' + offset;
        this.$grid.attr('aria-activedescendant', day);
        $('#' + day).addClass('focus').attr('aria-selected', 'true');
    }
}

// showPrevYear() is a member function to show the previous year
//
// @return N/A
//
Datepicker.prototype.showPrevYear = function () {
    // decrement the year
    this.year--;
    if (this.month != this.curMonth || this.year != this.curYear) {
        this.currentDate = false;
    }
    else {
        this.currentDate = true;
    }
    // populate the calendar grid
    this.popGrid();
    this.$monthObj.html(this.monthNames[this.month] + ' ' + this.year);
}

// showNextYear() is a member function to show the next year
//
// @return N/A
//
Datepicker.prototype.showNextYear = function () {
    // increment the year
    this.year++;
    if (this.month != this.curMonth || this.year != this.curYear) {
        this.currentDate = false;
    }
    else {
        this.currentDate = true;
    }
    // populate the calendar grid
    this.popGrid();
    this.$monthObj.html(this.monthNames[this.month] + ' ' + this.year);
}

// bindHandlers() is a member function to bind event handlers for the widget
//
// @return N/A
//
Datepicker.prototype.bindHandlers = function () {
    var thisObj = this;
    ////////////////////// bind button handlers //////////////////////////////////
    this.$target.off('keydown').keydown(function (e) {
        return thisObj.handleTargetKeyDown(e);
    });
    this.$prev.off('click').click(function (e) {
        return thisObj.handlePrevClick(e);
    });
    this.$next.off('click').click(function (e) {
        return thisObj.handleNextClick(e);
    });
    this.$prev.off('keydown').keydown(function (e) {
        return thisObj.handlePrevKeyDown(e);
    });
    this.$next.off('keydown').keydown(function (e) {
        return thisObj.handleNextKeyDown(e);
    });
    ///////////// bind grid handlers //////////////
    this.$grid.off('keydown').keydown(function (e) {
        return thisObj.handleGridKeyDown(e);
    });
    this.$grid.off('keypress').keypress(function (e) {
        return thisObj.handleGridKeyPress(e);
    });
    this.$grid.off('focus').focus(function (e) {
        return thisObj.handleGridFocus(e);
    });
    this.$grid.off('blur').blur(function (e) {
        return thisObj.handleGridBlur(e);
    });
    this.$grid.off('click').delegate('td', 'click', function (e) {
        return thisObj.handleGridClick(this, e);
    });
}

// handlePrevClick() is a member function to process click events for the prev month button
// @input (e obj) e is the event object associated with the event
//
// @return (boolean) false if consuming event, true if propagating
//
Datepicker.prototype.handlePrevClick = function (e) {
    var active = this.$grid.attr('aria-activedescendant');
    if (e.ctrlKey) {
        this.showPrevYear();
    }
    else {
        this.showPrevMonth();
    }
    if (this.currentDate == false) {
        this.$grid.attr('aria-activedescendant', 'day1');
    }
    else {
        this.$grid.attr('aria-activedescendant', active);
    }
    e.stopPropagation();
    return false;
}

// handleNextClick() is a member function to process click events for the next month button
// @input (e obj) e is the event object associated with the event
//
// @return (boolean) false if consuming event, true if propagating
//
Datepicker.prototype.handleNextClick = function (e) {
    var active = this.$grid.attr('aria-activedescendant');
    if (e.ctrlKey) {
        this.showNextYear();
    }
    else {
        this.showNextMonth();
    }
    if (this.currentDate == false) {
        this.$grid.attr('aria-activedescendant', 'day1');
    }
    else {
        this.$grid.attr('aria-activedescendant', active);
    }
    e.stopPropagation();
    return false;
}

// handlePrevKeyDown() is a member function to process keydown events for the prev month button
// @input (e obj) e is the event object associated with the event
//
// @return (boolean) false if consuming event, true if propagating
//
Datepicker.prototype.handlePrevKeyDown = function (e) {
    if (e.altKey) {
        return true;
    }
    switch (e.keyCode) {
        case this.keys.tab:
        {
            return true;
        }
        case this.keys.esc:
        {
            return this.dismiss(e);
        }
        case this.keys.enter:
        case this.keys.space:
        {
            if (e.shiftKey) {
                return true;
            }

            if (e.ctrlKey) {
                this.showPrevYear();
            }
            else {
                this.showPrevMonth();
            }

            e.stopPropagation();
            return false;
        }
    }
    return true;
}
// handleTargetKeyDown() is a member function to process keydown events for the target
// @input (e obj) e is the event object associated with the event
//
// @return (boolean) false if consuming event, true if propagating
//
Datepicker.prototype.handleTargetKeyDown = function (e) {
    switch (e.keyCode) {
        case this.keys.tab:
        {
            if (e.shiftKey && this.visible) {
                return this.dismiss(e);
            }
        }
    }
    return true;
}

// dismiss() is a member function to dismiss the datepicker
// @input (e) e is the event object associated with the event
//
// @return (boolean) false if consuming event, true if propagating
//
Datepicker.prototype.dismiss = function(e) {
    this.hideDlg();
    e.stopPropagation();
    return false;
}

// handleNextKeyDown() is a member function to process keydown events for the next month button
// @input (e obj) e is the event object associated with the event
//
// @return (boolean) false if consuming event, true if propagating
//
Datepicker.prototype.handleNextKeyDown = function (e) {
    if (e.altKey) {
        return true;
    }
    switch (e.keyCode) {
        case this.keys.esc:
        {
            return this.dismiss(e);
        }
        case this.keys.enter:
        case this.keys.space:
        {
            if (e.ctrlKey) {
                this.showNextYear();
            }
            else {
                this.showNextMonth();
            }
            e.stopPropagation();
            return false;
        }
    }
    return true;
}

// handleGridKeyDown() is a member function to process keydown events for the Datepicker grid
// @input (e obj) e is the event object associated with the event
//
// @return (boolean) false if consuming event, true if propagating
//
Datepicker.prototype.handleGridKeyDown = function (e) {
    var $rows = this.$grid.find('tbody tr');
    var $curDay = $('#' + this.$grid.attr('aria-activedescendant'));
    var $days = this.$grid.find('td').not('.empty');
    var $curRow = $curDay.parent();
    if (e.altKey) {
        return true;
    }
    switch (e.keyCode) {
        case this.keys.tab:
        {
            break;
        }
        case this.keys.enter:
        case this.keys.space:
        {
            if (e.ctrlKey) {
                return true;
            }
            if (!$curDay.is('.disabled')) {
                // update the target box
                this.updateTarget($curDay);
            }
            // fall through
        }
        case this.keys.esc:
        {
            return this.dismiss(e);
        }
        case this.keys.left:
        {
            if (e.ctrlKey || e.shiftKey) {
                return true;
            }
            var dayIndex = $days.index($curDay) - 1;
            var $prevDay = null;
            if (dayIndex >= 0) {
                $prevDay = $days.eq(dayIndex);
                $curDay.removeClass('focus').attr('aria-selected', 'false');
                $prevDay.addClass('focus').attr('aria-selected', 'true');
                this.$grid.attr('aria-activedescendant', $prevDay.attr('id'));
            }
            else {
                this.showPrevMonth(0);
            }
            e.stopPropagation();
            return false;
        }
        case this.keys.right:
        {
            if (e.ctrlKey || e.shiftKey) {
                return true;
            }
            var dayIndex = $days.index($curDay) + 1;
            var $nextDay = null;
            if (dayIndex < $days.length) {
                $nextDay = $days.eq(dayIndex);
                $curDay.removeClass('focus').attr('aria-selected', 'false');
                $nextDay.addClass('focus').attr('aria-selected', 'true');
                this.$grid.attr('aria-activedescendant', $nextDay.attr('id'));
            }
            else {
                // move to the next month
                this.showNextMonth(1);
            }
            e.stopPropagation();
            return false;
        }
        case this.keys.up:
        {
            if (e.ctrlKey || e.shiftKey) {
                return true;
            }
            var dayIndex = $days.index($curDay) - 7;
            var $prevDay = null;
            if (dayIndex >= 0) {
                $prevDay = $days.eq(dayIndex);
                $curDay.removeClass('focus').attr('aria-selected', 'false');
                $prevDay.addClass('focus').attr('aria-selected', 'true');
                this.$grid.attr('aria-activedescendant', $prevDay.attr('id'));
            }
            else {
                // move to appropriate day in previous month
                dayIndex = 6 - $days.index($curDay);
                this.showPrevMonth(dayIndex);
            }
            e.stopPropagation();
            return false;
        }
        case this.keys.down:
        {
            if (e.ctrlKey || e.shiftKey) {
                return true;
            }
            var dayIndex = $days.index($curDay) + 7;
            var $prevDay = null;
            if (dayIndex < $days.length) {
                $prevDay = $days.eq(dayIndex);
                $curDay.removeClass('focus').attr('aria-selected', 'false');
                $prevDay.addClass('focus').attr('aria-selected', 'true');
                this.$grid.attr('aria-activedescendant', $prevDay.attr('id'));
            }
            else {
                // move to appropriate day in next month
                dayIndex = 8 - ($days.length - $days.index($curDay));
                this.showNextMonth(dayIndex);
            }
            e.stopPropagation();
            return false;
        }
        case this.keys.pageup:
        {
            var active = this.$grid.attr('aria-activedescendant');
            if (e.shiftKey) {
                return true;
            }
            if (e.ctrlKey) {
                this.showPrevYear();
            }
            else {
                this.showPrevMonth();
            }
            if ($('#' + active).attr('id') == undefined) {
                var lastDay = 'day' + this.calcNumDays(this.year, this.month);
                $('#' + lastDay).addClass('focus').attr('aria-selected', 'true');
            }
            else {
                $('#' + active).addClass('focus').attr('aria-selected', 'true');
            }
            e.stopPropagation();
            return false;
        }
        case this.keys.pagedown:
        {
            var active = this.$grid.attr('aria-activedescendant');
            if (e.shiftKey) {
                return true;
            }
            if (e.ctrlKey) {
                this.showNextYear();
            }
            else {
                this.showNextMonth();
            }
            if ($('#' + active).attr('id') == undefined) {
                var lastDay = 'day' + this.calcNumDays(this.year, this.month);
                $('#' + lastDay).addClass('focus').attr('aria-selected', 'true');
            }
            else {
                $('#' + active).addClass('focus').attr('aria-selected', 'true');
            }
            e.stopPropagation();
            return false;
        }
        case this.keys.home:
        {
            if (e.ctrlKey || e.shiftKey) {
                return true;
            }
            $curDay.removeClass('focus').attr('aria-selected', 'false');
            $('#day1').addClass('focus').attr('aria-selected', 'true');
            this.$grid.attr('aria-activedescendant', 'day1');
            e.stopPropagation();
            return false;
        }
        case this.keys.end:
        {
            if (e.ctrlKey || e.shiftKey) {
                return true;
            }
            var lastDay = 'day' + this.calcNumDays(this.year, this.month);
            $curDay.removeClass('focus').attr('aria-selected', 'false');
            $('#' + lastDay).addClass('focus').attr('aria-selected', 'true');
            this.$grid.attr('aria-activedescendant', lastDay);
            e.stopPropagation();
            return false;
        }
    }
    return true;
}

// handleGridKeyPress() is a member function to consume keypress events for browsers that
// use keypress to scroll the screen and manipulate tabs
// @input (e obj) e is the event object associated with the event
//
// @return (boolean) false if consuming event, true if propagating
//
Datepicker.prototype.handleGridKeyPress = function (e) {
    if (e.altKey) {
        return true;
    }
    switch (e.keyCode) {
        case this.keys.tab:
        case this.keys.enter:
        case this.keys.space:
        case this.keys.esc:
        case this.keys.left:
        case this.keys.right:
        case this.keys.up:
        case this.keys.down:
        case this.keys.pageup:
        case this.keys.pagedown:
        case this.keys.home:
        case this.keys.end:
        {
            e.stopPropagation();
            return false;
        }
    }
    return true;
}

// handleGridClick() is a member function to process mouse click events for the Datepicker grid
// @input (id obj) e is the id of the object triggering the event
// @input (e obj) e is the event object associated with the event
//
// @return (boolean) false if consuming event, true if propagating
//
Datepicker.prototype.handleGridClick = function (id, e) {
    var $cell = $(id);
    if ($cell.is('.empty') || $cell.is('.disabled')) {
        return true;
    }
    this.$grid.find('.focus').removeClass('focus').attr('aria-selected', 'false');
    $cell.addClass('focus').attr('aria-selected', 'true');
    this.$grid.attr('aria-activedescendant', $cell.attr('id'));
    var $curDay = $('#' + this.$grid.attr('aria-activedescendant'));
    // update the target box
    this.updateTarget($curDay);
    e.stopPropagation();
    return false;
}

Datepicker.prototype.updateTarget = function (selectedDay) {
    var day = $(selectedDay).html();
    var month = this.month + 1;
    var year = this.year;

    var separator = undefined;
    for (var i = 0, len = this.format.length; i < len; i++) {
        var character = this.format.charAt(i);
        if (character != 'D' && character != 'M' && character != 'Y') {
            separator = character;
            break;
        }
    }
    var seq = this.format.split(separator);

    var value = "";
    for (var i = 0, len = seq.length; i < len; i++) {
        var dmy = seq[i];
        if (dmy.indexOf('D') > -1) {
            if (day < 10 && dmy.length == 2) {
                value += "0";
            }
            value = value + day;
        } else if (dmy.indexOf('M') > -1) {
            if (month < 10 && dmy.length == 2) {
                value += "0";
            }
            value = value + month;
        } else if (dmy.indexOf('Y') > -1) {
            if (dmy.length == 2) {
                year = Number(year.toString().substr(2));
            }
            value += year;
        }
        if (i < len - 1) {
            value += separator;
        }
    }
    this.$target.val(value).trigger('blur');
    this.hideDlg();
}

// handleGridFocus() is a member function to process focus events for the Datepicker grid
// @input (e obj) e is the event object associated with the event
//
// @return (boolean) true
//
Datepicker.prototype.handleGridFocus = function (e) {
    var active = this.$grid.attr('aria-activedescendant');
    if ($('#' + active).attr('id') == undefined) {
        var lastDay = 'day' + this.calcNumDays(this.year, this.month);
        $('#' + lastDay).addClass('focus').attr('aria-selected', 'true');
    }
    else {
        $('#' + active).addClass('focus').attr('aria-selected', 'true');
    }
    return true;
}

// handleGridBlur() is a member function to process blur events for the Datepicker grid
// @input (e obj) e is the event object associated with the event
//
// @return (boolean) true
//
Datepicker.prototype.handleGridBlur = function (e) {
    $('#' + this.$grid.attr('aria-activedescendant')).removeClass('focus').attr('aria-selected', 'false');
    datePicker.hideDlg();
    return true;
}

// showDlg() is a member function to show the Datepicker and give it focus.
//
// @return N/A
//
Datepicker.prototype.showDlg = function () {
    $(document).bind('click', function (e) {
        datePicker.hideDlg();
        e.stopPropagation;
        return false;
    });
    this.visible = true;
    this.$id.show();
}

// hideDlg() is a member function to hide the Datepicker and remove focus. This function is only called if
// the Datepicker is used in modal dialog mode.
//
// @return N/A
//
Datepicker.prototype.hideDlg = function () {
    $(document).unbind('click');
    this.visible = false;
    this.$id.hide();
}

var datePicker;

$('.calendar').each(function () {
    $(this).on({
        'focus': function (e) {
            var $input = $(this);
            var id = $input.attr('id');
            var target = $input.data('target');
            var dateformat = $input.data('format');
            var mindate = $input.data('mindate');
            var maxdate = $input.data('maxdate');
            datePicker = new Datepicker('calendar-' + id, id, dateformat, mindate, maxdate);
            datePicker.showDlg();
            e.stopPropagation();
            return false;
        },
        'click': function (e) {
            e.stopPropagation();
            return false;
        }
    });
});