(function ($) {
    $(document).ready(function() {
        $(".select-container").each(function(){
            initializeSelect(this);
        });
    });

    function initializeSelect(selectContainer) {
        var $selectContainer = $(selectContainer),
            $optionsContainer = $selectContainer.find('.options-container'),
            $selectPlaceholder = $selectContainer.find('.select-placeholder');

        // If JS is enabled, the fallback select can be removed and the custom one can be shown.
        $selectContainer.find('select').remove();
        $selectPlaceholder.show();

        // Show the correct value if it is preset.
        displaySelectedOption();

        $selectContainer.on({
            'keydown': function (e) {
                if (e.which == 13 || e.which == 32) {   //Space or enter.
                    toggleSelect($selectContainer);
                } else if ( (e.keyCode == 27 || e.keyCode == 9) && $(this).hasClass('select-open')) {   //Tab or escape.
                    e.preventDefault();
                    closeSelect();
                }
            },
            'mouseup.select-click': function(e){
                // Puts focus on the checked option so the keyboard can be used when the select is opened via a mouse click.
                $selectContainer.find("input:checked").trigger("focus");
                toggleSelect();
            }
        });

        $selectContainer
            .on('focus', 'input', function() {
                $selectContainer.addClass('select-focused')
            })

            .on('focusout', 'input', function() {
                $selectContainer.removeClass('select-focused')
            })

            .find('input').on("change", function () {
                displaySelectedOption();
            });

        function toggleSelect() {
            if (!$selectContainer.hasClass('select-open')) {

                // Opening select, display options and add handlers.
                $selectContainer.addClass('select-open');

                // Display selected option on top.
                displaySelectedOption();

                // Close the select if you click anywhere else.
                $(document).one('mousedown.select-click', function(e){
                    if(!$(e.target).closest($selectContainer).length){
                        closeSelect();
                    }
                });

            }  else {
                closeSelect();
            }
        }

        function displaySelectedOption() {
            var selectedLabelText = $optionsContainer.find('input:radio:checked').next().text();
            $selectPlaceholder.text(selectedLabelText);
        }

        function closeSelect() {
            $selectContainer.removeClass('select-open');
        }
    }
})
(jQuery);