(function ($) {
    "use strict";

    var pipe = function(fn1, fn2) {
        return function(x) {
            return fn2(fn1(x));
        };
    };

    var debounce = function (fn, timeout){
        var t;
        return function(){
            clearTimeout(t);
            t = setTimeout(fn, timeout)
        };
    };

    var objectReduce = function(obj, fn, acc){
        for (var key in obj){
            if (!obj.hasOwnProperty(key)) { continue; }
            acc = fn(acc, key, obj[key]);
        }
        return acc;
    };

    var id = function(x){
        return x;
    };

    var validationRules = {
        required : function(){
            return function(x){
                return x.length > 0;
            };
        },
        min : function(min){
            return function(x){
                return Number(x) >= Number(min);
            };
        },
        max : function(max){
            return function(x){
                return Number(x) <= Number(max);
            };
        },
        minlength : function(min){
            return function(x){
                return x.length >= Number(min);
            };
        },
        dateformat : function(){
            return function(x){
                // moment.js returns "Invalid date" string if it has failed to parse the date.
                return x !== "Invalid date";
            }
        },
        mindate : function(min){
            return function(x){
                return Date.parse(x) >= Date.parse(min);
            };
        },
        maxdate : function(max){
            return function(x){
                return Date.parse(x) <= Date.parse(max);
            };
        },
        maxsize : function(max){
            return function(x){
                return x.map(function(file){
                    return file.size <= Number(max) * 1024;
                }).reduce(function(acc, valid){
                    return acc && valid;
                }, true);
            };
        },
        regex: function(str){
            var regex = new RegExp(str);
            return function(x){
                return regex.test(x);
            };
        }
    };

    var validate = function(validatorFn, failureMsg){
        return function(validator){
            var t = validator.test(validatorFn, failureMsg);
            return t;
        };
    };

    var Validator = function(value){
        if(!(this instanceof Validator)){
            return new Validator(value);
        }
        this.isValid = true;
        this.test = function(validationFn, errorMessage) {
            return validationFn(value) ? new Validator(value) : new ValidationFailure(errorMessage);
        }
    };

    var ValidationFailure = function(failureMessage){
        if(!(this instanceof ValidationFailure)){
            return new ValidationFailure(failureMessage);
        }
        this.message = failureMessage;
        this.isValid = false;
        this.test= function(){
            return this;
        };
    };

    var getConstraint = function($el, key){
        var value = $el.attr(key),
            message = $el.data(key+'Msg');

        return {
            value : value,
            message : message
        };
    };

    var getChoiceGroupInputValue = function ($el) {
        var value = [];
        $el.find('input').each(function() {
            var $i = $(this),
                v = this.getAttribute("value");
            if($i.is(':checked') && v) {
                value.push(v);
            }
        });
        return value;
    };

    var getFileUploadInputValue = function($el){
        return Array.prototype.slice.call($el[0].files);
    };

    var getInputValue = function ($el) {
        if($el.is('div.cgroup')) {
            return getChoiceGroupInputValue($el);
        } else if($el.is('.calendar')) {
            var value = $el.val(),
                format = $el.data('format');
            //todo change the following line to use date_fns library when the parse option is there. Expected soon.
            return moment(value, format).format('YYYY-MM-DD');
        } else if( $el.is('[type="file"]')) {
            return getFileUploadInputValue($el);
        } else {
            return $el.val();
        }
    };

    function getValidations($el) {
        return objectReduce(validationRules, function(acc, key, validator) {
            var constraint = getConstraint($el, key);
            if (constraint.value !== undefined ) {
                if(constraint.message === undefined) {
                    throw new Error("You must provide a validation message for validation constraint : " + key + ". Add data-" + key + "-msg attribute to input.");
                }
                return pipe(acc, validate( validator(constraint.value), constraint.message) );
            }
            return acc;
        }, id);
    }

    var validateField = function($input) {
        var $errorMsg = $input.parents('.form-group').find('.error-msg'),
            validations = getValidations($input);

        return function() {
            var value = getInputValue($input);
            var val = new Validator(value);
            var v = validations(val);

            if (v.isValid) {
                $input.removeClass('error');
                $errorMsg.text('').hide();
                $input.off('input.error');
            } else {
                $input.addClass('error');
                $errorMsg.text(v.message).show();
                $input.on('input.error', debounce(function() {
                   $input.trigger('validate');
                }, 250 ));
            }
            return v.isValid;
        };
    };

    $('.form form').each(function() {
        var $form = $(this),
            $submit = $form.find('button[type="submit"]'),
            $inputs = $form.find('input:not([type="checkbox"]):not([type="radio"]), textarea, .cgroup'),
            $optionInputs = $form.find('.checkbox .cgroup, .radiobutton .cgroup, .select .cgroup');

        $optionInputs.each(function(){
            var $inputGroup = $(this),
                $options = $inputGroup.find('input');

            $options.on('focus', function(){
                $(this).addClass('focus');
            });
            $options.on('blur', function(){
                $(this).removeClass('focus');
                setTimeout(function(){
                    if(!$options.filter(function(){ return $(this).hasClass('focus'); }).length){
                        $inputGroup.trigger('validate');
                    }
                }, 0);
            });
        });

        $inputs.filter('[type="file"]').on('change', function(){
            $(this).trigger('validate');
        });

        var inputs = $inputs.toArray().map(function(input){
            var $input = $(input);
            var validator = validateField($input);
            $input.on('blur validate', validator);
            return validator;
        });

        $form.on('submit', function(e){

            var isValid = inputs.reduce(function(acc, validator){
                var valid = validator();
                return acc && valid;
            }, true);
            if(!isValid){
                e.preventDefault();
                return false;
            }
        });
    });

})(jQuery);