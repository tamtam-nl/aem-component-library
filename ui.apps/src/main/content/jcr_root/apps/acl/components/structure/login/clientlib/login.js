$(function() {
    $(".user-information").hide();
    $(".login-component").hide();

    var aemSsoCookie = getCookie('aemsso');
    if (aemSsoCookie) {
        sendRequestToGetUserInformation();
    } else {
        $(".login-component").show();
    }
});

$("#loginButton").click(function () {
    sendRequestToGetCookie();
});

$("#logoutButton").click(function () {
    var xsrfCookie = getCookie('XSRF-TOKEN');
    var logoutUrl = $("#endpointUrl").val() + "/api/logout";

    $.ajax({
        url : logoutUrl,
        type: "POST",
        headers: {
            'X-XSRF-TOKEN': xsrfCookie
        },
        xhrFields: {
            withCredentials: true
        }
    });

    document.cookie = "aemsso=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
    $(".user-information").hide();
    $(".login-component").show();
});



function sendRequestToGetCookie() {
    var cookieUrl = $("#endpointUrl").val();

    $.ajax({
        url : cookieUrl,
        type: "GET",
        success: function(res, status, xhr) {
            sendRequestForAuthentication();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            sendRequestForAuthentication();
        },
        xhrFields: {
            withCredentials: true
        }
    });
}

function sendRequestForAuthentication() {
    var username = $("input[name='j_username']").val();
    var password = $("input[name='j_password']").val();
    var xsrfCookie = getCookie('XSRF-TOKEN');
    var authenticationUrl = $("#endpointUrl").val() + "/api/authentication";

    $.ajax({
        url : authenticationUrl,
        type: "POST",
        data : {'j_username': username, 'j_password': password, 'remember-me': true, 'submit': 'Login'},
        headers: {
            'X-XSRF-TOKEN': xsrfCookie
        },
        success: function(data, textStatus, jqXHR)
        {
            sendRequestToGetUserInformation();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            sendRequestToGetUserInformation();
        },
        xhrFields: {
            withCredentials: true
        }
    });

    $("input[name='j_username']").val("");
    $("input[name='j_password']").val("");
}

function sendRequestToGetUserInformation() {
    var userInformationUrl = $("#endpointUrl").val() + "/api/account";

    $.ajax({
        url : userInformationUrl,
        type: "GET",
        success: function(data)
        {
            $(".user-information .firstName").html(data.firstName);
            $(".user-information .lastName").html(data.lastName);
            $(".login-component").hide();
            $(".user-information").show();
            document.cookie = 'aemsso=true';
        },
        error: function ()
        {
            window.alert("Invalid credentials");
        },
        xhrFields: {
            withCredentials: true
        }
    });
}

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}
