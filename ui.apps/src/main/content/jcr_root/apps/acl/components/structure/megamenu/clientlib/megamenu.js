$(function() {
    var $megaMenu       = $(".megamenu"),
        $checkboxes     = $megaMenu.find('input[type="checkbox"]'),
        $allLevels      = $checkboxes.not('#mobile-show'),
        $lvl2Levels     = $allLevels.not('.lvl1-item > :checkbox');

    // Close the menu if you click anywhere else.
    $(document).click( function(event) {
        if( !$(event.target).closest($megaMenu).length ) {
            closeMenu();
        }
    });

    // Close the menu if you press 'escape'.
    $(document).on( "keydown",  function(event) {
        if( event.keyCode === 27 ) {
            closeMenu();
        }
    });

    function closeMenu() {
        $checkboxes.prop("checked", false);
    }

    // Close other levels when you open a lvl1.
    $(document).on( "change", ".lvl1-item > :checkbox", function() {
        if (this.checked) {
            $allLevels.not(this).prop("checked", false);
        }
    });

    // Close other lvl2's when you open one.
    $(document).on( "change", ".lvl2-item > :checkbox", function() {
        if (this.checked) {
            $lvl2Levels.not(this).prop("checked", false);
        }
    });
});