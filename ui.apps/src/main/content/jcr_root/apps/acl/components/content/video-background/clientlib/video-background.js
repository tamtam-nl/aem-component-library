'use strict';

var tag = document.createElement('script');
tag.src = 'https://www.youtube.com/player_api';
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

/* This function needs to be in the global scope because YouTube's API will access it. */
function onYouTubePlayerAPIReady() {
    var videoId = $('#tv').attr('video-id');
    if (videoId) {
        acl.videoBackground.tv = new YT.Player('tv',
            {
                videoId: videoId,
                events:
                {
                    'onReady': acl.videoBackground.onPlayerReady,
                    'onStateChange': acl.videoBackground.onPlayerStateChange
                },
                playerVars: acl.videoBackground.playerDefaults
            });
    }
}


window.acl = window.acl || {};
acl.videoBackground = acl.videoBackground || {};
acl.videoBackground.tv = undefined;
acl.videoBackground.playerDefaults = {autoplay: 0, autohide: 1, modestbranding: 0, rel: 0, showinfo: 0, controls: 0, disablekb: 1, enablejsapi: 0, iv_load_policy: 3};

acl.videoBackground.onPlayerReady = function(event){
    var mute = $('#tv').attr('mute');
    if (mute) {
        acl.videoBackground.tv.mute();
    }
    event.target.playVideo();
};

acl.videoBackground.onPlayerStateChange = function(event) {
    if (event.data == YT.PlayerState.ENDED) {
        acl.videoBackground.tv.playVideo();
    }
};

acl.videoBackground.vidRescale = function() {
    var w = $(window).width() + 200,
        h = $(window).height() + 200,
        $tv = $('.tv'),
        $screen = $tv.find('.screen');

    if (w/h > 16/9){
        if (acl.videoBackground.tv) {
            acl.videoBackground.tv.setSize(w, w/16*9);
        } else {
            $screen.attr('width', w);
            $screen.attr('height', w/16*9);
        }
        $screen.css({'left': '0px'});
    } else {
        if (acl.videoBackground.tv) {
            acl.videoBackground.tv.setSize(h/9*16, h);
        } else {
            $screen.attr('width', h/9*16);
            $screen.attr('height', h);
        }
        $screen.css({'left': -( $screen.outerWidth()-w)/2} );
    }

    var opacity = $tv.attr('video-opacity') / 100;
    if(!opacity) {
        opacity = 0.8;
    }
    $tv.css('opacity', opacity);
};

$(window).on('load resize', function() {
    acl.videoBackground.vidRescale();
});