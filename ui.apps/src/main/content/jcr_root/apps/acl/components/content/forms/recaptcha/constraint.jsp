<%@ page import="nl.tricode.aem.core.services.recaptcha.ValidationInfo" %>
<%@ page import="org.apache.commons.lang.ArrayUtils" %>
<%@include file="/libs/foundation/global.jsp"%>
<%
    final ValidationInfo info = ValidationInfo.getValidationInfo(slingRequest);
    if (info != null) {
        final String[] errors = info.getErrorMessages("g-recaptcha-field");
        if (!ArrayUtils.isEmpty(errors)) {
            for (final String error : errors) {
                %><span><fmt:message key="<%= "error.recaptcha." + error %>"/></span><%
            }
        }
        info.clear();
    }
%>
