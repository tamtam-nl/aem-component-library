(function (document, $, ns) {
    "use strict";

    $(document).on("click", ".cq-dialog-submit", function (e) {
        if ($(this).parents(".cq-dialog-header").find(".coral-Heading").text() === "Video") {
            e.stopPropagation();
            e.preventDefault();

            var $form = $(this).closest("form.foundation-form"),
                urladd = $form.find("[name='./videourl']").val(),
                message, clazz = "coral-Button ",
                patterns = {
                    urladd: /(\byoutube\b)|(\bvimeo\b)/
                };

            if (urladd != "" && !patterns.urladd.test(urladd) && (urladd != null)) {
                ns.ui.helpers.prompt({
                    title: Granite.I18n.get("Invalid Input"),
                    message: "The component suports only vimeo and youtube",
                    actions: [
                        {
                            id: "CANCEL",
                            text: "CANCEL",
                            className: "coral-Button"
                        }
                    ],
                    callback: function (actionId) {
                        if (actionId === "CANCEL") {
                        }
                    }
                });

            } else {
                $form.submit();
                location.reload();
            }
        }
    });
})(document, Granite.$, Granite.author);
