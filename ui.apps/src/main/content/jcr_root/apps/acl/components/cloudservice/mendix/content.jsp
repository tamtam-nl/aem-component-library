<%--
 ADOBE CONFIDENTIAL
 __________________
  Copyright 2012 Adobe Systems Incorporated
  All Rights Reserved.
 NOTICE:  All information contained herein is, and remains
 the property of Adobe Systems Incorporated and its suppliers,
 if any.  The intellectual and technical concepts contained
 herein are proprietary to Adobe Systems Incorporated and its
 suppliers and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material
 is strictly forbidden unless prior written permission is obtained
 from Adobe Systems Incorporated.
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@page session="false" %>

<%
    String name=properties.get("name","");
    String proxyPath=properties.get("proxyPath","");

%>
<%@page session="false" contentType="text/html" pageEncoding="utf-8" %>
<div class="mendix-conf-content">
    <img src="/apps/acl/components/cloudservice/mendix/mendix.png"
         style="width: 100px; height: 100px; float: left;">

    <ul class="mendix-content" style="float: left;">

        <li>
            <div class="li-bullet"><strong>Application Name: </strong><%=name%></div>
        </li>
        <li>
            <div class="li-bullet"><strong>Proxy Path: </strong><%=proxyPath%></div>
        </li>
    </ul>
</div>
