<%@ page import="com.adobe.granite.ui.components.Config" %>
<%@ page import="org.slf4j.Logger" %>
<%@ page import="org.slf4j.LoggerFactory" %>
<%@ page import="com.adobe.granite.ui.components.Value" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@include file="/libs/granite/ui/global.jsp" %>

<%--include ootb multifield--%>
<sling:include resourceType="/libs/granite/ui/components/foundation/form/multifield"/>

<%!
    private final Logger mLog = LoggerFactory.getLogger(this.getClass());
%>

<%
    Config mCfg = cmp.getConfig();

    Resource mField = mCfg.getChild("field");

    if (mField == null) {
        mLog.warn("Field node doesn't exist");
        return;
    }

    ValueMap mVM = mField.adaptTo(ValueMap.class);

    String mName = mVM.get("name", "");

    if ("".equals(mName)) {
        mLog.warn("name property doesn't exist on field node");
        return;
    }

    Value mValue = ((ComponentHelper) cmp).getValue();

    //get the values added in multifield
    String[] mItems = mValue.get(mName, String[].class);
%>