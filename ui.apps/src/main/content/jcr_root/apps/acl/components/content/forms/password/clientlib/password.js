(function($){
    $(document).on('cut copy paste', function(e){

        if($(e.target).is('input[type="password"]')){
            e.preventDefault();
            return false;
        }
    })
})(jQuery);