(function($) {

    var $searchForm = $('.search-form'),
        $searchInput = $('#searchQuery'),
        $suggestions = $searchInput.parent().siblings('.suggestions'),
        maxRows = $suggestions.data('maxRows') || 6;
        borderWidth = 2;

    var debounce = function (fn, timeout){
        var t;
        return function(){
            clearTimeout(t);
            t = setTimeout(fn, timeout)
        };
    };

    var keyNavigation = function(data, $results){
        var i;

        var getListItem = function(i){
            return $results.eq(i%$results.length);
        };

        var focusKey = function($key){
            var childPosition = $key.position().top,
                parentScroll = $suggestions.scrollTop(),
                childHeight = $key.height(),
                parentHeight = $suggestions.height();

            if(childPosition < 0){
                $suggestions.scrollTop(parentScroll + childPosition);
            } else if (childPosition - parentHeight + childHeight > 0){
                $suggestions.scrollTop(parentScroll + childPosition - parentHeight + childHeight);
            }
            $key.addClass('active').siblings().removeClass('active')
        }

        var navigateKeys = function(mod){
            i = typeof i === 'number' ? i + mod : mod > 0 ? 0 : mod;
            focusKey(getListItem(i));
        };

        return function(e){
            switch(e.which){
                case 38:
                    e.preventDefault();
                    navigateKeys(-1);
                    break;
                case 40:
                    e.preventDefault();
                    navigateKeys(1);
                    break;
                case 13:
                    e.preventDefault();
                    selectListItem.call(getListItem(i));
                    break;
                case 27:
                    $searchForm.removeClass('active');
            }
        }
    };

    var selectListItem = function(){
        if($(this).text()) {
            $searchInput.val($(this).text()).off('keydown.dropdown-list');
        }
        $searchForm.removeClass('active');
        $searchForm.submit();
    };

    var createItem = function(x){
        return $('<li>').text(x.item).on('click', selectListItem);
    };

    var items = function(coll, x){
        return coll.add(createItem(x));
    };

    var renderAutocomplete = function(data){
        var $results = data.reduce(items, $([]));
        if($results.length){
            $suggestions.html($results);
            $searchInput.on('keydown.dropdown-list', keyNavigation(data, $results));
            $searchForm.addClass('active');
            $suggestions.css({
                'max-height' : $results.first().height() * maxRows + borderWidth
            });
        } else {
            $searchForm.removeClass('active');
        }
    };

    var getSuggestions = function (){
        $.ajax({
            url : "/apps/acl/SearchServlet",
            type : "GET",
            data : {
                'searchQuery' : $searchInput.val()
            },
            dataType : "json",
            success : renderAutocomplete
        });
    };

    $searchInput.on('input', debounce(getSuggestions, 200));

})(jQuery);