(function (document, $, ns) {
    "use strict";

    function isURL(str) {
        var pattern = new RegExp('^(https?:\\/\\/)' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return pattern.test(str);
    }

    function checkFormLinkValidity(target, message) {
        var valid = true,
            targetPath = target.first().val();
        if (!targetPath) {
            valid = false;
            target.addClass("is-invalid");
            message = "Redirection link is required.Please fill it out!";
        } else if (targetPath && !isURL(targetPath)) {
            valid = false;
            target.addClass("is-invalid");
            message += targetPath + "</br>";
        }
        return {valid: valid, message: message};
    }

    $(document).on("click", ".cq-dialog-submit", function (e) {
        if ($(this).parents(".cq-dialog-header").find(".coral-Heading").text() === "Form Properties") {
            e.stopPropagation();
            e.preventDefault();

            var $form = $(this).closest("form.foundation-form");
            var target = $form.find("input[name='./target']");
            var message = "Please use valid URL insted of:</br>"

            var __ret = checkFormLinkValidity(target, message);

            if (!__ret.valid) {
                ns.ui.helpers.prompt({
                    title: Granite.I18n.get("Invalid Input"),
                    message: __ret.message,
                    actions: [
                        {
                            id: "CANCEL",
                            text: "CANCEL",
                            className: "coral-Button"
                        }
                    ],
                    callback: function (actionId) {
                        if (actionId === "CANCEL") {
                        }
                    }
                });

            } else {
                $form.submit();
            }
        }
    });
})(document, Granite.$, Granite.author);