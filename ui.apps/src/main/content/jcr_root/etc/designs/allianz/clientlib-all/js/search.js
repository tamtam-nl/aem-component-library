(function($) {
    var $searchForm = $('.search-form'),
        $searchInput = $('#searchQuery'),
        $searchSubmit = $('.search-form .search-submit');

    $searchSubmit.click(function(e) {
        e.preventDefault();
        $searchInput.toggleClass('extended');
        $searchSubmit.parent().toggleClass('extended');

        if($searchInput.hasClass("extended")) {
            $searchInput.trigger('focus');
        } else {
            $searchInput.trigger('focusout');
            $searchInput.val("");
        }
    });

    $searchInput.keypress(function(e) {
       if (e.which == 13) {
           e.preventDefault();
           $searchForm.submit();
       }
    });
})(jQuery);
