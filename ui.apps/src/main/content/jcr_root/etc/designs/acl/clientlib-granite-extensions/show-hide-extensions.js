(function(document, $) {
    "use strict";

    var getInput = function (el){
        if (el.is('.coral-Select')){
            return el.children('select');
        } else {
            return el;
        }
    };
//

    var getInputValue = function(el){
        var type = el.attr('type'),
            radiocheck = type === 'checkbox' || type === 'radio';

        if (radiocheck){
            var checked = el.prop('checked');
            return checked ? el.val() : '';
        } else if (el.is('.coral-Select')){
            return el.children('select').val();
        }
        return el.val();
    };

    var objectReduce = function(obj, fn, acc){
        for (var key in obj){
            if (!obj.hasOwnProperty(key)) { continue };
            acc = fn(acc, key, obj[key]);
        }
        return acc;
    };

    var Data = function(){
        var data = {};
        return function(obj, key, value){
            if(obj === undefined){
                return data;
            }
            if(data[obj] === undefined){
                data[obj] = {};
                return false;
            }
            if(key === undefined){
                return data[obj];
            }
            if(value === undefined){
                return data[obj][key]
            }
            data[obj][key] = value;
        }
    };

    var defaults = Data();
    var state = Data();

    $(document).on("foundation-contentloaded", function(e) {
        var controls = $('.cq-dialog-smart-showhide');

        (function init() {

            controls.each(function () {
                var control = $(this),
                    targets = $('[data-smart-showhide-id="' + control.data('cqDialogSmartShowhideTarget') + '"]'),
                    key = getInput(control).attr('name')
                ;

                targets.each(function () {
                    var target = $(this),
                        id = target.attr('data-smart-showhide-id') + target.data('showOnValue');

                    if (!defaults(id)) {
                        target.data('showOnValue').split(" ").forEach(function (x) {
                            var pair = x.split(':'),
                                key = pair[0],
                                value = pair[1]
                                ;
                            defaults(id, key, value);
                            state(id);
                            state(id, key, null);
                        });
                    }
                });

                var controlHandler = function (e) {
                    var control = $(this),
                        targets = $('[data-smart-showhide-id="' + control.data('cqDialogSmartShowhideTarget') + '"]');

                    targets.each(function () {
                        var target = $(this),
                            id = target.attr('data-smart-showhide-id') + target.data('showOnValue');

                        var value = getInputValue(control);
                        state(id, key, value);
                    });

                    targets.each(function () {
                        var target = $(this),
                            id = target.attr('data-smart-showhide-id') + target.data('showOnValue');

                        var newStateIsVisible = objectReduce(defaults(id), function (acc, key, value) {
                            return acc && state(id, key) === value;
                        }, true);

                        var currentStateIsVisible = !target.hasClass('hide');

                        if (newStateIsVisible !== currentStateIsVisible) {
                            target.toggleClass('hide');
                        }
                    });
                };

                if(control.is('.coral-Select')){
                    control.find('.coral-SelectList').on('click', controlHandler.bind(control));
                } else {
                    control.on('change', controlHandler);
                }
                controlHandler.call(control);
            });
        })();
    });

    // when dialog gets injected
    $(document).on("foundation-contentloaded", function(e) {
        // if there is already an inital value make sure the according target element becomes visible
        $(".cq-dialog-checkbox-showhide").each( function() {
            showHide($(this));
        });
    });

    $(document).on("change", ".cq-dialog-checkbox-showhide", function(e) {
        showHide($(this));
    });

    function showHide(el){

        // get the selector to find the target elements. its stored as data-.. attribute
        var target = el.data("cqDialogCheckboxShowhideTarget");

        // get name of set attribute
        var key = el.attr("name");

        // is checkbox checked?
        var checked = el.prop('checked');

        // get the selected value
        // if checkbox is not checked, we set the value to empty string
        var value = checked ? el.val() : '';

        // make sure all unselected target elements are hidden.
        $(target).not(".hide").addClass("hide");

        // unhide the target element that contains the selected value as data-showhidetargetvalue attribute
        $(target).filter("[data-showhidetargetvalue='" + value + "']").removeClass("hide");

    }

})(document,Granite.$);